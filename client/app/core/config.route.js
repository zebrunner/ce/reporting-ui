// TODO: refactor redirections: create global "$rootScope.$on('$stateChangeError'..." handler inside a run() function
(function () {
    'use strict';

    angular.module('app')
        .config(function($stateProvider, $urlRouterProvider) {
            'ngInject';

            $stateProvider
                .state('home', {
                    redirectTo: transition => transition.router.stateService.target('dashboard.list', {}, { location: 'replace', reload: true, inherit: false })
                })
                .state('dashboard', {
                    url: '/dashboards',
                    abstract: true,
                    template: '<ui-view />',
                    data: {
                        requireLogin: true,
                    }
                })
                .state('dashboard.list', {
                    url: '',
                    component: 'dashboardsListComponent',
                    data: {
                        title: 'Dashboards',
                        requireLogin: true,
                        classes: 'p-dashboard',
                    },
                    resolve: {
                        dashboardsData: ($stateParams, $q, DashboardsService, $state, messageService, $timeout) => {
                            'ngInject';

                            const defaultSort = {
                                page: 1,
                                pageSize: 10000,
                                sortOrder: 'ASC',
                            };

                            return DashboardsService.GetDashboards(defaultSort)
                                .then(({ success, data, message }) => {
                                    if (success) {
                                        DashboardsService.dashboards = data.results;
                                        return $q.resolve(data.results);
                                    } else {
                                        return $q.reject(message);
                                    }
                                })
                                .catch((err) => {
                                    $timeout(() => {
                                        messageService.error(err);
                                    }, 0, false);
                                });
                        },
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "dashboardList" */ '../modules/dashboards-list/dashboards-list.module');

                            return $ocLazyLoad.load(mod.dashboardsListModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load dashboardList module, ' + err);
                        }
                    },
                })
                .state('dashboard.page', {
                    url: '/:dashboardId?userId&currentUserId&currentUserName&testCaseId&testCaseName&hashcode&PARENT_JOB&PARENT_BUILD',
                    component: 'dashboardComponent',
                    data: {
                        requireLogin: true,
                        classes: 'p-dashboard',
                        isDynamicTitle: true,
                        breadcrumbsData: [
                            {
                                title: 'Dashboards',
                                state: 'dashboard.list',
                            },
                        ],
                    },
                    resolve: {
                        dashboard: ($transition$, $state, DashboardsService, $q, $timeout, messageService) => {
                            'ngInject';

                            const { dashboardId } = $transition$.params();

                            if (dashboardId) {
                                return DashboardsService.GetDashboardById(dashboardId).then(function (rs) {
                                    if (rs.success) {
                                        return rs.data;
                                    } else {
                                        //TODO: dashboards is a home page. If we redirect to dashboards we can get infinity loop. We need to add simple error page;
                                        const message = rs && rs.message || `Can\'t fetch dashboard with id: ${dashboardId}`;
                                        const is404 = rs && rs.error && rs.error.status === 404;

                                        if (!is404) {
                                            messageService.error(message);
                                        }
                                        // Timeout to avoid digest issues
                                        $timeout(() => {
                                            const state = is404 ? '404' : 'home';

                                            $state.go(state);
                                        }, 0, false);

                                        return $q.reject({ message });
                                    }
                                });
                            } else {
                                // Timeout to avoid digest issues
                                $timeout(function () {
                                    $state.go('home');
                                }, 0, false);

                                return false;
                            }
                        },
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "dashboard" */ '../_dashboards/dashboard.module.js');

                            return $ocLazyLoad.load(mod.dashboardModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load dashboard module, ' + err);
                        }
                    }
                })
                .state('signin', {
                    url: '/signin',
                    component: 'signinComponent',
                    params: {
                        location: null,
                        user: null,
                    },
                    data: {
                        title: 'Signin',
                        onlyGuests: true,
                        classes: 'body-wide body-auth'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "auth" */ '../_auth/auth.module.js');

                            return $ocLazyLoad.load(mod.authModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load auth module, ' + err);
                        }
                    }
                })
                .state('signup', {
                    url: '/signup?token',
                    component: 'signupComponent',
                    data: {
                        title: 'Signup',
                        onlyGuests: true,
                        classes: 'body-wide body-auth'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "auth" */ '../_auth/auth.module.js');

                            return $ocLazyLoad.load(mod.authModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load auth module, ' + err);
                        }
                    }
                })
                .state('logout', {
                    url: '/logout',
                    controller: function($state, authService, $timeout, observerService) {
                        'ngInject';

                        observerService.emit('logout');
                        // Timeout to avoid digest issues
                        $timeout(function() {
                            $state.go('signin');
                        }, 0, false);
                    },
                    data: {
                        requireLogin: true,
                    }
                })
                .state('forgotPassword', {
                    url: '/password/forgot',
                    component: 'forgotPasswordComponent',
                    data: {
                        title: 'Forgot password',
                        onlyGuests: true,
                        classes: 'body-wide body-auth'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "auth" */ '../_auth/auth.module.js');

                            return $ocLazyLoad.load(mod.authModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load auth module, ' + err);
                        }
                    }
                })
                .state('resetPassword', {
                    url: '/password/reset?token',
                    component: 'resetPasswordComponent',
                    data: {
                        title: 'Reset password',
                        onlyGuests: true,
                        classes: 'body-wide body-auth'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "auth" */ '../_auth/auth.module.js');

                            return $ocLazyLoad.load(mod.authModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load auth module, ' + err);
                        }
                    }
                })
                .state('users', {
                    url: '/users',
                    component: 'usersComponent',
                    data: {
                        title: 'Users',
                        requireLogin: true,
                        classes: 'p-users',
                        permissions: ['iam:users:read', 'iam:users:update'],
                        breadcrumbsData: [
                            {
                                title: 'Settings',
                                state: 'settings.home',
                            },
                        ],
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "users" */ '../_users/users.module.js');

                            return $ocLazyLoad.load(mod.usersModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load usersModule module, ' + err);
                        }
                    }
                })
                .state('userGroups', {
                    url: '/user-groups',
                    component: 'groupsComponent',
                    data: {
                        title: 'Groups and Permissions',
                        requireLogin: true,
                        classes: 'p-users-groups',
                        permissions: ['iam:groups:update'],
                        breadcrumbsData: [
                            {
                                title: 'Settings',
                                state: 'settings.home',
                            },
                        ],
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "userGroups" */ '../_groups/groups.module.js');

                            return $ocLazyLoad.load(mod.groupsModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load groupsModule module, ' + err);
                        }
                    }
                })
                .state('invitations', {
                    url: '/invitations',
                    component: 'invitationsComponent',
                    data: {
                        title: 'Invitations',
                        requireLogin: true,
                        classes: 'p-users',
                        permissions: ['iam:invitations:update', 'iam:invitations:delete'],
                        breadcrumbsData: [
                            {
                                title: 'Settings',
                                state: 'settings.home',
                            },
                        ],
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "invitations" */ '../_invitations/invitations.module.js');

                            return $ocLazyLoad.load(mod.invitationsModule);
                        }
                        catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load invitationsModule module, ' + err);
                        }
                    }
                })
                .state('userProfile', {
                    url: '/profile',
                    component: 'userComponent',
                    data: {
                        title: 'Account and Profile',
                        requireLogin: true,
                        classes: 'p-user-profile',
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "user" */ '../_user/user.module.js');

                            return $ocLazyLoad.load(mod.userModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load userModule module, ' + err);
                        }
                    }
                })
                // For github redirection
                // TODO: Should be only for guests?
                .state('scmCallback', {
                    url: '/scm/callback?code',
                    component: 'scmComponent',
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "scm" */ '../_scm/scm.module.js');

                            return $ocLazyLoad.load(mod.scmModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load scm module, ' + err);
                        }
                    }
                })
                .state('tests', {
                    url: '/test-runs',
                    abstract: true,
                    template: '<ui-view />',
                    data: {
                        requireLogin: true,
                    }
                })
                .state('tests.runs', {
                    url: '?query&status&browser&platform&fromDate&toDate&date&reviewed&environment&locale&page&{activeTestRunId:int}&filterID',
                    params: {
                        browser: { array: true },
                        environment: { array: true },
                        locale: { array: true },
                        platform: { array: true },
                        status: { array: true },
                        page: { type: 'int' },
                        filterID: { type: 'int' },
                        fromWizard: null,
                    },
                    component: 'testsRunsComponent',
                    data: {
                        title: 'Test runs',
                        requireLogin: true,
                        classes: 'p-tests-runs'
                    },
                    reloadOnSearch: false,
                    resolve: {
                        resolvedData: (
                            testsRunsService,
                            $q,
                            messageService,
                            $stateParams,
                        ) => {
                            'ngInject';

                            const searchParams = {
                                ...testsRunsService.defaultSearchParams,
                                ...testsRunsService.prepareSearchParams($stateParams),
                            };

                            return testsRunsService.getTestRuns(searchParams)
                                .catch(({ message }) => {
                                    if (message) {
                                        messageService.error(message);
                                    }
                                    //if can't return empty data
                                    return $q.resolve({ results: [] });
                                });
                        },
                        prepareRunsFilterService: (
                            RunsFilterService,
                        ) => {
                            'ngInject';

                            if (!RunsFilterService.isInitialized) {
                                return RunsFilterService.initServiceData();
                            }

                            return true;
                        },
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "tests-runs" */ '../containers/tests-runs/tests-runs.module.js');

                            return $ocLazyLoad.load(mod.testsRunsModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load testsRuns module, ' + err);
                        }
                    }
                })
                .state('tests.runDetails', {
                    url: '/:testRunId?runsState',
                    component: 'testDetailsComponent',
                    store: true,
                    params: {
                        testRun: null,
                        configSnapshot: null,
                        runsState: null,
                    },
                    data: {
                        title: 'Test results',
                        isDynamicTitle: true,
                        requireLogin: true,
                        classes: 'p-tests-run-details',
                        breadcrumbsData: [
                            {
                                title: 'Test runs',
                                customAction: true,
                            },
                        ],
                    },
                    resolve: {
                        testRun: function($stateParams, $q, $state, TestRunService, $timeout, messageService) {
                            'ngInject';

                            if ($stateParams.testRunId) {
                                const params = {
                                    id: $stateParams.testRunId
                                };

                                return TestRunService.searchTestRuns(params)
                                    .then(function(response) {
                                        if (response.success && response.data.results && response.data.results[0]) {
                                            return response.data.results[0];
                                        } else {
                                            return $q.reject({message: `Test run with ID=${$stateParams.testRunId} not found`});
                                        }
                                    })
                                    .catch(function(error) {
                                        if (error.message) {
                                            messageService.error(error.message);
                                        }
                                        // Timeout to avoid digest issues
                                        $timeout(() => {
                                            $state.go('tests.runs');
                                        }, 0, false);

                                        return $q.reject(error);
                                    });
                            } else {
                                // Timeout to avoid digest issues
                                $timeout(() => {
                                    $state.go('tests.runs');
                                }, 0, false);

                                return $q.reject({ message: 'missed "testRunId" param'});
                            }
                        },
                        configSnapshot: ($stateParams, $q) => {
                            'ngInject';

                            return $q.resolve($stateParams.configSnapshot);
                        }
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "test-details" */ '../containers/test-details/test-details.module.js');

                            return $ocLazyLoad.load(mod.testDetailsModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load testDetails module, ' + err);
                        }
                    }
                })
                .state('tests.runInfo', {
                    url: '/:testRunId/tests/:testId?parentTestId&runsState',
                    component: 'testRunInfoComponent',
                    data: {
                        requireLogin: true,
                        classes: 'p-tests-run-info',
                        isDynamicTitle: true,
                        breadcrumbsData: [
                            {
                                title: 'Test Runs',
                                customAction: true,
                            },
                            {
                                title: 'TestRun',
                                customAction: true,
                            },
                        ],
                    },
                    params: {
                        configSnapshot: null,
                        runsState: null,
                    },
                    resolve: {
                        test: ($stateParams, TestRunService, $q, TestService, $timeout, $state) => {
                            'ngInject';
                            if ($stateParams.testRunId) {
                                const params = {
                                    'page': 1,
                                    'pageSize': 100000,
                                    'testRunId': parseInt($stateParams.testRunId, 10),
                                };

                                return TestService.searchTests(params)
                                    .then((rs) => {
                                        if (rs.success) {
                                            const testId = parseInt($stateParams.testId, 10);

                                            TestService.tests = rs.data.results || [];

                                            return TestService.getTest(testId);
                                        } else {
                                            return $q.reject({message: `Can't fetch tests for test run with ID=${$stateParams.testRunId}` });
                                        }
                                    })
                                    .catch(() => {
                                        // Timeout to avoid digest issues
                                        $timeout(() => {
                                            $state.go('tests.runs');
                                        }, 0, false);
                                    });
                            } else {
                                // Timeout to avoid digest issues
                                $timeout(() => {
                                    $state.go('tests.runs');
                                }, 0, false);
                            }
                        },
                        testRun: ($stateParams, $q, $state, TestRunService, $timeout) => {
                            'ngInject';

                            if ($stateParams.testRunId) {
                                const params = {
                                    id: $stateParams.testRunId
                                };

                                return TestRunService.searchTestRuns(params)
                                    .then((response) => {
                                        if (response.success && response.data.results && response.data.results[0]) {
                                            return response.data.results[0];
                                        } else {
                                            return $q.reject({message: 'Can\'t get test run with ID=' + $stateParams.testRunId});
                                        }
                                    })
                                    .catch(function(error) {
                                        // Timeout to avoid digest issues
                                        $timeout(() => {
                                            $state.go('tests.runs');
                                        }, 0, false);
                                    });
                            } else {
                                // Timeout to avoid digest issues
                                $timeout(() => {
                                    $state.go('tests.runs');
                                }, 0, false);
                            }
                        },
                        configSnapshot: ($stateParams, $q) => {
                            'ngInject';

                            return $q.resolve($stateParams.configSnapshot);
                        },
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "testRunInfo" */ '../containers/test-run-info/test-run-info.module.js');

                            return $ocLazyLoad.load(mod.testRunInfoModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load testRunInfo module, ' + err);
                        }
                    }
                })
                .state('404', {
                    url: '/404',
                    component: 'notFoundComponent',
                    data: {
                        title: 'Not found',
                        classes: 'body-wide body-err p-not-found'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "not-found" */ '../modules/not-found/not-found.module.js');

                            return $ocLazyLoad.load(mod.notFoundModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load notFoundModule module, ' + err);
                        }
                    }
                })
                .state('500', {
                    url: '/500',
                    component: 'serverErrorComponent',
                    data: {
                        title: 'Server error',
                        classes: 'body-wide body-err p-server-error'
                    },
                    lazyLoad: async ($transition$) => {
                        const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                        try {
                            const mod = await import(/* webpackChunkName: "not-found" */ '../modules/server-error/server-error.module.js');

                            return $ocLazyLoad.load(mod.serverErrorModule);
                        } catch (err) {
                            throw new Error('ChunkLoadError: Can\'t load serverErrorModule module, ' + err);
                        }
                    }
                });

            $urlRouterProvider
                .when('/', '/dashboards')
                .when('', '/dashboards')
                .otherwise('/404');

        });
})();

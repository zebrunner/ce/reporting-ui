import template from './wizard.html';
import controller from './wizard.controller';

import './wizard.scss';

const wizardComponent = {
    template,
    controller,
};

export default wizardComponent;
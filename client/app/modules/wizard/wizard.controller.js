import Swiper from 'swiper';

const wizardController = function wizardController(
    $httpMock,
    $mdMedia,
    $scope,
    $q,
    $state,
    $stateParams,
    $timeout,
    authService,
    jsonConfigsService,
    integrationsService,
    messageService,
    pageTitleService,
    toolsService,
    UserService,
    UtilService,
) {
    'ngInject';

    const swiperOptions = {
        initialSlide: 0,
        longSwipesMs: 500,
        pagination: true,
        observer: true,
        observeSlideChildren: true,
        shortSwipes: false,
        slidesPerView: 1,
        autoHeight: true,
        allowTouchMove: false,
    };
    const vm = {
        configsData: null,
        isConfigsLoading: true,
        selectedSlide: null,

        $onInit,
        handlePaginationClick,
        handleNextSlideButton,
        checkCurrentUserState,

        get currentUser() { return UserService.currentUser; },
        get serviceUrl() { return `${ $httpMock.apiHost ? $httpMock.apiHost : location.origin }`; },
        get currentTitle() { return pageTitleService.pageTitle; },
        get currentSlideNumber() { return vm.swiper && vm.swiper.activeIndex },
        get isDesktop() { return $mdMedia('md') || $mdMedia('gt-md'); },
    };

    return vm;

    function $onInit() {
        $timeout(() => {
            vm.swiperContainer = document.querySelector('.wizard-slider__container');
            initSwiper();
        }, 0);
        initConfigsData();
    }

    function handleSlideChange() {
        const swiperInstance = this;

        if (vm.slidesBullets?.length) {
            vm.slidesBullets.forEach((slideBullet, index) => checkSlideBulletClasses(slideBullet, index));
        }
        if ($stateParams.step !== swiperInstance.activeIndex) {
            updateStateParams(swiperInstance.activeIndex);
        }
        $scope.$apply();
    }

    function checkSlideBulletClasses(slideBullet, index) {
        index < vm.currentSlideNumber ? slideBullet.classList.add('_previous') : slideBullet.classList.remove('_previous');
    }

    function handlePaginationClick($event) {
        if ($event.target.classList.contains('wizard-slider__pagination-divider')) {
            $event.stopImmediatePropagation();
        }
    }

    function handleNextSlideButton() {
        if (this.swiper.isEnd) {
            this.checkCurrentUserState(true);
        }
    }

    function checkCurrentUserState(completed) {
        if (!this.currentUser.onboardingCompleted) {
            UserService.updateUserProfile(this.currentUser.id, [{op: 'replace', path: `/onboardingCompleted`, value: true}])
                .then(() => {
                    UserService.currentUser.onboardingCompleted = true;
                    goToTestRuns(completed);
                });
        } else {
            goToTestRuns();
        }
    }

    function goToTestRuns(completed) {
        $state.go('tests.runs', completed ? {fromWizard: true} : null);
    }

    function initSwiper() {
        if (!vm.swiperContainer || typeof Swiper !== 'function') { return; }

        swiperOptions.initialSlide = 0;
        swiperOptions.navigation = {
            nextEl: '.swiper-nav-btn._next',
            prevEl: '.swiper-nav-btn._prev',
            disabledClass: '_disabled',
        };
        if (angular.isDefined($stateParams.step)) {
            swiperOptions.initialSlide = $stateParams.step;
        } else {
            updateStateParams(0);
        }
        if (UtilService.isTouchDevice()) {
            swiperOptions.allowTouchMove = true;
        }

        vm.swiper = new Swiper(vm.swiperContainer, {
            ...swiperOptions,
            pagination: {
                el: '.wizard-slider__pagination',
                clickable: true,
                bulletClass: 'wizard-slider__pagination-bullet',
                bulletActiveClass: '_active',
                renderBullet: () => `
                    <div class="wizard-slider__pagination-bullet">
                        <div class="wizard-slider__pagination-element">
                            <div class="wizard-slider__pagination-element-wrapper">
                                <div class="wizard-slider__pagination-element-icon"></div>
                            </div>
                        </div>
                        <div class="wizard-slider__pagination-divider"></div>
                    </div>
                `,
            },
            on: {
                init: function () {
                    const swiperInstance = this;

                    if (swiperInstance) {
                        $timeout(() => {
                            vm.slidesBullets = document.querySelectorAll('.wizard-slider__pagination-bullet');
                            correctPageStateParam(swiperInstance);
                            handleSlideChange.call(swiperInstance);
                        }, 0);
                    }
                },
                'slideChange': handleSlideChange,
            },
        });
    }

    // handles incorrect page param
    function correctPageStateParam(swiper) {
        if (angular.isDefined($stateParams.step) && $stateParams.step > swiper.slides.length) {
            swiper.slideTo(0)
            updateStateParams(0);
        }
    }

    function updateStateParams(step) {
        $state.go($state.current, { ...$stateParams, step }, { inherit: false });
    }

    function initConfigsData() {
        if (!vm.isDesktop) {
            vm.isConfigsLoading = false;

            return;
        }

        return authService.generateAccessToken()
            .then(({ data }) => {
                vm.configsData = vm.configsData || {};
                vm.configsData = {
                    ...vm.configsData,
                    apiAccessToken: data?.token ?? '',
                    get userName() { return UtilService.getUserName(vm.currentUser, {}, 'unknown'); },
                    get hostname() { return `${ $httpMock.apiHost ? $httpMock.apiHost : location.origin }`; },
                };
            })
            .finally(() => vm.isConfigsLoading = false);
    }
}

export default wizardController;

import template from './core-concepts-slide.html';

import './core-concepts-slide.scss';

const coreConceptsSlideComponent = {
    template,
};

export default coreConceptsSlideComponent;

const agentConfigSlideController = (
    $state,
    $stateParams,
    $timeout,
) => {
    'ngInject';

    const vm = {
        tabs: [
            {
                name: 'TestNG',
                id: 'testng',
                active: true,
            },
            {
                name: 'JUnit5',
                id: 'junit5',
                active: false,
                disabled: true,
            },
            {
                name: 'JUnit4',
                id: 'junit4',
                active: false,
                disabled: true,
            },
            {
                name: 'PyTest',
                id: 'pytest',
                active: false,
                disabled: true,
            },
        ],

        $onInit,
        onTabSelect,
    }

    return vm;

    function $onInit() {
        let activeAgent = vm.tabs[0];

        if ($stateParams.agent) {
            const passedAgent = $stateParams.agent.toLowerCase();
            let foundAgent = vm.tabs.find(({ id }) => passedAgent === id);

            if (foundAgent) {
                activeAgent = foundAgent;
            }
        }
        $timeout(() => {
            activeAgent.active = true;
        }, 0);
    }

    function updateStateParams(agent) {
        $state.go($state.current, { ...$stateParams, agent: agent.id }, { inherit: false });
    }

    function onTabSelect(agent) {
        updateStateParams(agent);
    }
}

export default agentConfigSlideController;

import template from './agent-config-slide.html';
import controller from './agent-config-slide.controller';

import './agent-config-slide.scss';

const agentConfigSlideComponent = {
    template,
    controller,
    bindings: {
        configsData: '<',
        configsDataLoading: '<',
        swiperInstance: '<',
    },
};

export default agentConfigSlideComponent;

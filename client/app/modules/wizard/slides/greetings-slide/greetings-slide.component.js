import template from './greetings-slide.html';

import './greetings-slide.scss';

const greetingsSlideComponent = {
    template,
};

export default greetingsSlideComponent;

import wizardComponent from './wizard.component';
import { agentConfigSlide } from './slides/agent-config-slide';
import { coreConceptsSlide } from './slides/core-concepts-slide';
import { greetingsSlide } from './slides/greetings-slide';
import { wizardTestngConfiguration } from './components/wizard-testng-configuration';

export const WizardModule = angular.module('wizard', [])

    .constant('ZEBRUNNER_API_PATH', '/zebrunner-ws') //TODO: do we need this?
    .component({ wizardComponent })
    .component({ agentConfigSlide })
    .component({ coreConceptsSlide })
    .component({ greetingsSlide })
    .component({ wizardTestngConfiguration })
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('wizard', {
                url: '/new?step&agent&builder',
                params: {
                    step: { type: 'int' },
                },
                component: 'wizardComponent',
                reloadOnSearch: false,
                data: {
                    title: 'Wizard',
                    classes: 'p-wizard',
                    requireLogin: true,
                },
            });
    })

    .name;

import 'brace';
import 'brace/mode/xml';
import 'brace/mode/plain_text';
import 'brace/mode/javascript';
import 'brace/theme/mono_industrial';
import 'angular-ui-ace';
import testngConfig from './codesnippet.testng.json';

const wizardTestngConfigurationController = (
    $scope,
    $state,
    $stateParams,
    $timeout,
    messageService,
    UtilService,
) => {
    'ngInject';

    let baseOptionsWatcher = angular.noop;
    let agentParamsWatcher = angular.noop;
    const aceDefaultOptions = {
        useWrapMode: true,
        showGutter: true,
        theme: 'mono_industrial',
        mode: 'plain_text',
        rendererOptions: {
            fontSize: '14px',
        },
        // to disable library's warning message in the console https://github.com/angular-ui/ui-ace/issues/104
        $blockScrolling: Infinity,
    };
    const vm = {
        activeBuilder: null,
        baseOptions: null,
        builders: [],
        builderConfig: null,
        builderOptionalConfig: null,
        agentProperties: null,
        swiperInstance: null,

        $onInit,
        $onDestroy,
        copyCodeSnippet,
        onBuilderChange,
    }

    return vm;

    function $onInit() {
        bindListeners();
    }

    function $onDestroy() {
        unbindListeners();
    }

    function bindListeners() {
        baseOptionsWatcher = $scope.$watch(() => vm.baseOptions, (newOptions) => {
            if (newOptions) {
                initSnippets();
            }
        });
        agentParamsWatcher = $scope.$watch(() => $stateParams.builder, (newOptions) => {
            if (newOptions) {
                initActiveBuilder();
            }
        });
    }

    function unbindListeners() {
        baseOptionsWatcher();
        agentParamsWatcher();
    }

    function initSnippets() {
        vm.builders = Object.keys(testngConfig.buildScript);
        initActiveBuilder(true);
        setActiveBuilderConfig();
        setAgentProperties();
        $timeout(() => {
            if (vm.swiperInstance?.update) {
                vm.swiperInstance.update();
            }
        }, 0, false);
    }

    function initActiveBuilder(isInitialization) {
        if ($stateParams.builder) {
            let activeBuilder = vm.builders.find((builder) => builder === $stateParams.builder);

            if (activeBuilder) {
                if (vm.activeBuilder !== activeBuilder) {
                    vm.activeBuilder = activeBuilder;
                    if (!isInitialization) {
                        onBuilderChange();
                    } else {
                        updateStateParams(vm.activeBuilder);
                    }
                }

               return true;
            }
        }

        vm.activeBuilder = vm.builders[0];
        updateStateParams(vm.activeBuilder);
    }

    function setActiveBuilderConfig() {
        const config = testngConfig.buildScript[vm.activeBuilder];

        vm.builderConfig = {
            aceOptions: {
                ...aceDefaultOptions,
                mode: config.dependency.lang,
                onLoad: builderAceLoaded,
            },
            model: config.dependency.snippet,
            fileName: config.dependency.fileName,
        };

        vm.builderOptionalConfig = {
            aceOptions: {
                ...aceDefaultOptions,
                mode: config.optional.lang,
                onLoad: builderOptionalAceLoaded,
            },
            model: config.optional.snippet,
            fileName: config.optional.fileName,
        };
    }

    function setAgentProperties() {
        const config = testngConfig.agentProps;

        vm.agentProperties = {
            aceOptions: {
                ...aceDefaultOptions,
                mode: config.lang,
                onLoad: agentPropsAceLoaded,
            },
            model: UtilService.replaceTextPlaceholders(config.snippet, vm.baseOptions),
            fileName: config.fileName,
        };
    }

    function builderAceLoaded(editor) {
        if (editor) {
            vm.builderConfig.editor = editor;
            fixEditorHighlighting(editor);
        }
    }

    function builderOptionalAceLoaded(editor) {
        if (editor) {
            vm.builderOptionalConfig.editor = editor;
            fixEditorHighlighting(editor);
        }
    }

    function agentPropsAceLoaded(editor) {
        if (editor) {
            vm.agentProperties.editor = editor;
            fixEditorHighlighting(editor);
        }
    }

    function fixEditorHighlighting(editor) {
        editor.setOptions({
            highlightActiveLine: false,
            highlightGutterLine: false,
            tabSize: 4,
            useSoftTabs: false,
        });
        editor.on('focus', () => {
            editor.setOptions({
                highlightActiveLine: true,
                highlightGutterLine: true,
            });
        });
        editor.on('blur', () => {
            editor.setOptions({
                highlightActiveLine: false,
                highlightGutterLine: false,
            });
        });
    }

    function copyCodeSnippet(codeSnippet) {
        codeSnippet.copyToClipboard();
        messageService.success('Code copied to clipboard');
    }

    function onBuilderChange() {
        const config = testngConfig.buildScript[vm.activeBuilder];

        vm.builderConfig.model = config.dependency.snippet;
        vm.builderConfig.fileName = config.dependency.fileName;
        if (vm.builderConfig.editor && config.dependency.lang) {
            vm.builderConfig.editor.getSession().setMode(config.dependency.lang);
        }
        vm.builderOptionalConfig.model = config.optional.snippet;
        vm.builderOptionalConfig.fileName = config.optional.fileName;
        if (vm.builderOptionalConfig.editor && config.optional.lang) {
            vm.builderOptionalConfig.editor.getSession().setMode(config.optional.lang);
        }

        updateStateParams(vm.activeBuilder);
    }

    function updateStateParams(builder) {
        $state.go($state.current, { ...$stateParams, builder }, { inherit: false });
    }
};

export default wizardTestngConfigurationController;

import template from './wizard-testng-configuration.html';
import controller from './wizard-testng-configuration.controller';

import './wizard-testng-configuration.scss';

const wizardTestngConfigurationComponent = {
    template,
    controller,
    bindings: {
        baseOptions: '<',
        configsDataLoading: '<',
        swiperInstance: '<',
    },
};

export default wizardTestngConfigurationComponent;

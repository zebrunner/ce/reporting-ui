import configurationComponent from './configuration.component';
import reportIcon from '../../../assets/images/report.svg';

import './configuration.scss';

const ConfigurationsModule = angular.module('ConfigurationsModule', [])
    .component({ configurationComponent })
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('settings', {
                url: '/settings',
                abstract: true,
                template: '<ui-view />',
                data: {
                    requireLogin: true,
                    permissions: [
                        'iam:users:update',
                        'iam:users:read',
                        'iam:groups:read',
                        'iam:groups:update',
                        'iam:invitations:update',
                        'iam:invitations:read',
                        'reporting:integrations:read',
                    ],
                },
            })
            .state('settings.home', {
                url: '',
                component: 'configurationComponent',
                data: {
                    title: 'Settings',
                    classes: 'p-settings',
                },
            });
    })
    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('reportIcon', reportIcon);
    })

    .name;

export default ConfigurationsModule;

import template from './configuration.html';
import controller from './configuration.controller';

const configurationComponent = {
    template,
    controller,
};

export default configurationComponent;

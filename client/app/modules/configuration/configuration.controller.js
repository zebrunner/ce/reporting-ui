const configurationController = function configurationController(
    authService,
    pageTitleService,
    UserService,
    $rootScope,
) {
    'ngInject';

    return {
        userHasAnyPermission: authService.userHasAnyPermission,

        get currentTitle() { return pageTitleService.pageTitle; },
        get currentUser() { return UserService.currentUser; },
        get appVersions() { return $rootScope.version; },
    };
}

export default configurationController;

import template from './integrations.html';
import controller from './integrations.controller';
import './integrations.scss';

const integrationsComponent = {
    template,
    controller,
};

export default integrationsComponent;

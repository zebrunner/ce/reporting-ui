const testProviderFormController = function testProviderFormController(
    $scope,
    $timeout,
    messageService,
    UtilService,
) {
    'ngInject';

    const connectionMessageDisplayTime = 30000;
    const integrationInitialState = {
        accessKey: '',
        enabled: false,
        hubUrl: '',
        username: '',
    };
    const defaultLabels = {
        hubUrl: 'URL',
        username: 'Username',
        accessKey: 'API Token',
    };
    const validations = {
        hubUrl: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 250 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 250 characters',
                value: 250,
            },
            {
                name: 'pattern',
                message: 'Entered value is not a valid URL',
                value: /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
        textField: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 100 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 100 characters',
                value: 100,
            },
            {
                name: 'pattern',
                message: 'Spaces are not allowed',
                value: /^\S+$/,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
    };
    let dataWatchUnsubscriber = angular.noop;
    const vm = {
        customLabels: null,
        integration: null,
        isSaving: false,
        testingStatus: 'initial',
        testingMessageTimeout: null,

        $onDestroy,
        $onInit,
        cancel,
        copyToClipboard,
        deleteIntegration,
        getFieldLabel,
        getValidationValue,
        onPassChange,
        saveIntegration,
        switchIntegrationState,
        testIntegration,

        get isTouchDevice() { return UtilService.isTouchDevice(); },
        get hasNameField() { return (vm.integration || {}).hasOwnProperty('name') },
        get validations() { return validations; },
    };

    return vm;

    function $onInit() {
        bindWatchers();
    }

    function $onDestroy() {
        unbindWatchers();
    }

    function bindWatchers() {
        dataWatchUnsubscriber = $scope.$watch(
            () => vm.integrationSource,
            initIntegrationModel,
        );
    }

    function unbindWatchers() {
        dataWatchUnsubscriber();
    }

    function saveIntegration($testProviderForm) {
        vm.isSaving = true;

        const isNewIntegration = !vm.integrationSource?.accessKey;

        vm.integration.enabled = isNewIntegration || vm.integration.enabled;
        const $data = {
            integration: vm.integration,
            callback: (responseStatus) => {
                vm.isSaving = false;
                if (responseStatus === 'success') {
                    UtilService.untouchForm($testProviderForm);
                }
            },
        };

        vm.onSave({ $data });
    }

    function deleteIntegration($testProviderForm) {
        vm.isSaving = true;

        const $data = {
            integration: vm.integration,
            callback: () => {
                vm.isSaving = false;
                UtilService.untouchForm($testProviderForm);
                resetTestingStatus();
            },
        };

        vm.onDelete({$data});
    }

    function switchIntegrationState() {
        vm.isSaving = true;

        const $data = {
            integration: vm.integration,
            callback: () => { vm.isSaving = false; },
        };

        vm.onUpdate({$data});
    }

    function testIntegration() {
        vm.isSaving = true;
        vm.testingStatus = 'loading';

        const $data = {
            integration: vm.integration,
            callback: (connectionStatus, messages) => {
                vm.isSaving = false;
                vm.testingStatus = connectionStatus;
                vm.testingStatusMessages = messages || [];

                if (vm.testingMessageTimeout) {
                    $timeout.cancel(vm.testingMessageTimeout);
                }
                vm.testingMessageTimeout = $timeout(() => {
                    vm.testingStatus = 'initial';
                    vm.testingStatusMessages = null;
                }, connectionMessageDisplayTime);
            },
        };

        vm.onTest({$data});
    }

    function cancel($testProviderForm) {
        UtilService.untouchForm($testProviderForm);
        initIntegrationModel(vm.integrationSource);
        resetTestingStatus();
    }

    function resetTestingStatus() {
        if (vm.testingStatus !== 'initial') {
            if (vm.testingMessageTimeout) {
                $timeout.cancel(vm.testingMessageTimeout);
            }
            vm.testingStatus = 'initial';
            vm.testingStatusMessages = null;
        }
    }

    function initIntegrationModel(integrationSource) {
        if (integrationSource) {
            vm.integration = { ...integrationSource };
        } else {
            vm.integration = { ...integrationInitialState };
        }
    }

    function getValidationValue(field, propName) {
        const validationsArray = vm.validations[field];
        const item = validationsArray.find(item => item.name === propName);

        return item ? item.value : null;
    }

    function onPassChange(passFieldData) {
        if (!vm.integrationSource) { return false; }

        const isSourceEncrypted = !!vm.integrationSource.encrypted;
        const isFieldChanged = !passFieldData.$pristine;
        const isPassChanged = vm.integrationSource.accessKey !== vm.integration.accessKey;

        vm.integration.encrypted = isSourceEncrypted && (!isFieldChanged || !isPassChanged);
    }

    function copyToClipboard(text) {
        text.copyToClipboard();
        messageService.success('Code copied to clipboard');
    }

    function getFieldLabel(fieldKey) {
        return vm.customLabels && vm.customLabels[fieldKey] ? vm.customLabels[fieldKey] : defaultLabels[fieldKey];
    }
};

export default testProviderFormController;

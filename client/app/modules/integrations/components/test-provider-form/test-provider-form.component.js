import controller from './test-provider-form.controller';
import template from './test-provider-form.html';
import './test-provider-form.scss';

export const testProviderFormComponent = {
    controller,
    template,
    transclude: true,
    bindings: {
        customLabels: '<?',
        displayCopyButtons: '<?',
        displayToken: '<?',
        isUnableDelete: '<',
        isSaving: '=?',
        isReadOnlyForm: '<',
        integrationSource: '<',
        onDelete: '&',
        onSave: '&',
        onTest: '&',
        onUpdate: '&',
        simpleForm: '<',
    },
};

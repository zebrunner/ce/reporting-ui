import template from './integration-card.html';
import controller from './integration-card.controller';
import './integration-card.scss';

const integrationCardComponent = {
    template,
    controller,
    bindings: {
        integration: '<',
    },
};

export default integrationCardComponent;

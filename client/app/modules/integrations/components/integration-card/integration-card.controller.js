const integrationCardController = function integrationCardController() {
    'ngInject';

    const vm = {
        integration: null,

        get integrationPath() { return vm.integration.path ? `/integrations/${vm.integration.path}` : vm.integration.externalLink },
    };

    return vm;
}

export default integrationCardController;

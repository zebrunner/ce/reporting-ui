const integrationConfigIntroController = function integrationConfigIntroController(

) {
    'ngInject';

    const docPath = 'https://zebrunner.com/documentation/integrations';

    const vm = {
        $onInit,
        createDocPath,
    };

    return vm;

    function $onInit() {
        formatProviderDescription();
        prepareAdditionalText();
    }

    function formatProviderDescription() {
        if (!vm.infoData?.description) { return; }

        vm.infoData.formattedDescription = getSplitText(vm.infoData.description);
    }

    function prepareAdditionalText() {
        if (!vm.infoData?.additionalText) { return; }

        const keys = Object.keys(vm.infoData.additionalText);

        if (keys.length) {
            vm.infoData.formattedAdditionalText = {};
            keys.forEach((key) => {
                vm.infoData.formattedAdditionalText[key] = getSplitText(vm.infoData.additionalText[key]);
            });
        }
    }

    function getSplitText(text) {
        return text
            .split(/\n/)
            .filter(Boolean);
    }

    function createDocPath(itemPath) {
        return `${docPath}${itemPath}`;
    }
}

export default integrationConfigIntroController;

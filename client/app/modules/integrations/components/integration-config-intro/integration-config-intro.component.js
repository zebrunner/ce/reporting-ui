import template from './integration-config-intro.html';
import controller from './integration-config-intro.controller';
import './integration-config-intro.scss';

const integrationConfigIntroComponent = {
    template,
    controller,
    bindings: {
        infoData: '<',
    },
};

export default integrationConfigIntroComponent;

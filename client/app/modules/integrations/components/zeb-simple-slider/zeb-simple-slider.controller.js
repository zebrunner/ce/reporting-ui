import Swiper from 'swiper';

const zebSimpleSliderController = function zebSimpleSliderController() {
    'ngInject';

    const swiperOptions = {
        initialSlide: 0,
        longSwipesMs: 500,
        observer: true,
        shortSwipes: false,
        slidesPerView: 1,
    };

    return {
        swiperOptions,
    };
}

export default zebSimpleSliderController;

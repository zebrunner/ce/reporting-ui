import Swiper from 'swiper';

export function zebSimpleSliderDirective($timeout) {
    'ngInject';

    const defaultSliderOptions = {
        navigation: {
            nextEl: '.zeb-simple-slider__btn._next',
            prevEl: '.zeb-simple-slider__btn._prev',
            disabledClass: '_disabled',
            hiddenClass: '_hidden',
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
        },
    }
    return {
        restrict: 'A',
        scope: {
            sliderOptions: '<',
        },
        link: ($scope, $element) => {
            const containerElem = $element[0];
            const swiperOptions = {
                ...defaultSliderOptions,
                ...($scope.sliderOptions || {}),
            }
            let swiperInstance = null;

            if (typeof Swiper !== 'function' || !containerElem.classList.contains('swiper-container')) { return; }

            $timeout(() => {
                swiperInstance = new Swiper(containerElem, swiperOptions);
            }, 0);
        }
    };
}


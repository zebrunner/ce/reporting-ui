import template from './zeb-simple-slider.html';
import controller from './zeb-simple-slider.controller';
import './zeb-simple-slider.scss';

const zebSimpleSliderComponent = {
    template,
    controller,
    bindings: {
        slides: '<',
    },
    transclude: true,
};

export default zebSimpleSliderComponent;

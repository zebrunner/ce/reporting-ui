import zebSimpleSliderComponent from './zeb-simple-slider.component';
import { zebSimpleSliderDirective } from './zeb-simple-slider.directive';

export {
    zebSimpleSliderComponent,
    zebSimpleSliderDirective,
};

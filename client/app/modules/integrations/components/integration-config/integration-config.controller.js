const integrationConfigController = function integrationConfigControlle(
    pageTitleService,
) {
    'ngInject';

    return {
        get currentTitle() { return pageTitleService.pageTitle; },
    };
}

export default integrationConfigController;

import template from './integration-config.html';
import controller from './integration-config.controller';
import './integration-config.scss';

const integrationConfigComponent = {
    template,
    controller,
    bindings: {
        integrationData: '<',
        integrations: '<',
    },
};

export default integrationConfigComponent;

'use strict';

const IntegrationsController = function IntegrationsController(
    pageTitleService,
) {
    'ngInject';

    const integrations = [
        {
            categoryName: 'Tools and Services',
            items: [
                {
                    name: 'Jenkins',
                    icon: 'jenkins',
                    available: true,
                    path: 'jenkins',
                },
                {
                    name: 'Slack',
                    icon: 'slack',
                    available: true,
                    path: 'slack',
                },
                {
                    name: 'Jira',
                    icon: 'jira',
                    available: true,
                    path: 'jira',
                },
                {
                    name: 'TestRail',
                    icon: 'testrail',
                    available: true,
                    path: 'testrail',
                },
            ],
        },
        {
            categoryName: 'Test automation frameworks',
            items: [
                {
                    name: 'Carina',
                    icon: 'carina',
                    available: true,
                    externalLink: 'https://zebrunner.com/documentation/reporting/carina',
                },
                {
                    name: 'TestNG',
                    icon: 'testNg',
                    available: true,
                    externalLink: 'https://zebrunner.com/documentation/reporting/testng',
                },
                {
                    name: 'JUnit',
                    icon: 'junit',
                    available: true,
                    externalLink: 'https://zebrunner.com/documentation/reporting/junit4',
                },
                {
                    name: 'JUnit5',
                    icon: 'junit5',
                    available: true,
                    externalLink: 'https://zebrunner.com/documentation/reporting/junit5',
                },
                {
                    name: 'PyTest',
                    icon: 'pytest',
                    available: true,
                    externalLink: 'https://zebrunner.com/documentation/reporting/pytest',
                },
            ],
        },
    ];

    return {
        get integrations() { return integrations; },
        get currentTitle() { return pageTitleService.pageTitle; },
    };
}

export default IntegrationsController;

import { integrationCardComponent } from './components/integration-card';
import integrationsComponent from './integrations.component';
import { integrationConfigComponent } from './components/integration-config';
import { integrationConfigIntroComponent } from './components/integration-config-intro';
import { testProviderFormComponent } from './components/test-provider-form';
import { zebSimpleSliderComponent, zebSimpleSliderDirective } from './components/zeb-simple-slider';

import jiraIcon from '../../../assets/images/_icons_tools/jira.svg';
import testrail from '../../../assets/images/_icons_tools/testrail.svg';
import zebrunnerEngine from '../../../assets/images/_icons_tools/zebrunnerengine.svg';
import slack from '../../../assets/images/_icons_tools/slack.svg';
import jenkins from '../../../assets/images/_icons_tools/ci-jenkins.svg';
import carina from '../../../assets/images/_icons_tools/carina.svg';
import pytest from '../../../assets/images/_icons_tools/pytest.svg';
import junit5 from '../../../assets/images/_icons_tools/junit5.svg';
import junit from '../../../assets/images/_icons_tools/junit.svg';
import testNg from '../../../assets/images/_icons_tools/testng.svg';

import jenkinsInfoData from './modules/integration-jenkins/assets/description.json';
const jenkinsSliderImages = [
    require('../../../assets/images/integrations/integrations_jenkins_1.jpeg'),
    require('../../../assets/images/integrations/integrations_jenkins_2.jpeg'),
];
import jiraInfoData from './modules/integration-jira/assets/description.json';
const jiraSliderImages = [
    require('../../../assets/images/integrations/integrations_jira_1.jpeg'),
    require('../../../assets/images/integrations/integrations_jira_2.jpeg'),
    require('../../../assets/images/integrations/integrations_jira_3.jpeg'),
];
import slackInfoData from './modules/integration-slack/assets/description.json';
const slackSliderImages = [
    require('../../../assets/images/integrations/integrations_slack_1.jpg'),
    require('../../../assets/images/integrations/integrations_slack_2.jpg'),
];
import testrailInfoData from './modules/integration-testrail/assets/description.json';
const testrailSliderImages = [
    require('../../../assets/images/integrations/integrations_testrail_1.jpeg'),
    require('../../../assets/images/integrations/integrations_testrail_2.jpeg'),
    require('../../../assets/images/integrations/integrations_testrail_3.jpeg'),
];

export const IntegrationsModule = angular.module('IntegrationsModule', [])
    .component({ integrationsComponent })
    .component('integrationCard', integrationCardComponent)
    .component('integrationConfig', integrationConfigComponent)
    .component('integrationConfigIntro', integrationConfigIntroComponent)
    .component('testProviderForm', testProviderFormComponent)
    .component('zebSimpleSlider', zebSimpleSliderComponent)
    .directive('zebSimpleSlider', zebSimpleSliderDirective)

    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('jira', jiraIcon)
            .icon('testrail', testrail)
            .icon('junit5', junit5)
            .icon('junit', junit)
            .icon('zebrunnerEngine', zebrunnerEngine)
            .icon('slack', slack)
            .icon('testNg', testNg)
            .icon('jenkins', jenkins)
            .icon('carina', carina)
            .icon('pytest', pytest)
            ;
    })
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('integrations', {
                url: '/integrations',
                abstract: true,
                template: '<ui-view />',
                data: {
                    requireLogin: true,
                    permissions: ['reporting:integrations:read'],
                },
            })
            .state('integrations.list', {
                url: '',
                component: 'integrationsComponent',
                data: {
                    title: 'Integrations',
                    requireLogin: true,
                    classes: 'p-integrations',
                    breadcrumbsData: [
                        {
                            title: 'Settings',
                            state: 'settings.home',
                        },
                    ],
                },
            })
            .state('integration', {
                url: '/integrations',
                component: 'integrationConfig',
                abstract: true,
                data: {
                    requireLogin: true,
                    permissions: ['reporting:integrations:read'],
                }
            })
            .state('integration.jira', {
                url: '/jira',
                data: {
                    classes: 'p-integration-jira',
                    requireLogin: true,
                    title: jiraInfoData.title,
                    breadcrumbsData: [
                        {
                            title: 'Settings',
                            state: 'settings.home',
                        },
                        {
                            title: 'Integrations',
                            state: 'integrations.list',
                        },
                    ],
                },
                views: {
                    info: {
                        component: 'integrationConfigIntro',
                    },
                    configForm: {
                        component: 'jiraIntegrationForm',
                    },
                },
                resolve: {
                    infoData: () => {
                        'ngIjnect';

                        jiraInfoData.slides = jiraSliderImages;

                        return jiraInfoData;
                    },
                    integrations: (
                        $q,
                        $state,
                        $stateParams,
                        $timeout,
                        messageService,
                        integrationsService,
                    ) => {
                        'ngInject';

                        return integrationsService.getIntegration('jira')
                            .then((response) => {
                                if  (!response.success && response.error?.status === 404) {
                                    response = {
                                        ...response,
                                        success: true,
                                        data: null,
                                    };
                                }

                                return response;
                            })
                            .then(({ success, data, message, error }) => {
                                if (success) {
                                    return data ? [data] : [];
                                } else {
                                    if (error?.status !== 403 && message) {
                                        messageService.error(message);
                                    }
                                    $timeout(() => {
                                        $state.go('integrations.list');
                                    });
                                    return $q.reject(message);
                                }
                            });
                    },
                },
                lazyLoad: async ($transition$) => {
                    const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                    try {
                        const mod = await import(/* webpackChunkName: "integrationJira" */ './modules/integration-jira');

                        return $ocLazyLoad.load(mod.IntegrationJiraModule);
                    } catch (err) {
                        throw new Error('ChunkLoadError: Can\'t load module, ' + err);
                    }
                },
            })
            .state('integration.testrail', {
                url: '/testrail',
                data: {
                    classes: 'p-integration-testrail',
                    requireLogin: true,
                    title: testrailInfoData.title,
                    breadcrumbsData: [
                        {
                            title: 'Settings',
                            state: 'settings.home',
                        },
                        {
                            title: 'Integrations',
                            state: 'integrations.list',
                        },
                    ],
                },
                views: {
                    info: {
                        component: 'integrationConfigIntro',
                    },
                    configForm: {
                        component: 'testrailForm',
                    },
                },
                resolve: {
                    infoData: () => {
                        'ngInject';

                        testrailInfoData.slides = testrailSliderImages;

                        return testrailInfoData;
                    },
                    integrations: (
                        $q,
                        $state,
                        $stateParams,
                        $timeout,
                        messageService,
                        integrationsService,
                    ) => {
                        'ngInject';

                        return integrationsService.getIntegration('testrail')
                            .then((response) => {
                                if  (!response.success && response.error?.status === 404) {
                                    response = {
                                        ...response,
                                        success: true,
                                        data: null,
                                    };
                                }

                                return response;
                            })
                            .then(({ success, data, message, error }) => {
                                if (success) {
                                    return data ? [data] : [];
                                } else {
                                    if (error?.status !== 403 && message) {
                                        messageService.error(message);
                                    }
                                    $timeout(() => {
                                        $state.go('integrations.list');
                                    });
                                    return $q.reject(message);
                                }
                            });
                    },
                },
                lazyLoad: async ($transition$) => {
                    const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                    try {
                        const mod = await import(/* webpackChunkName: "integrationTestrail" */ './modules/integration-testrail');

                        return $ocLazyLoad.load(mod.IntegrationTestrailModule);
                    } catch (err) {
                        throw new Error('ChunkLoadError: Can\'t load module, ' + err);
                    }
                },
            })
            .state('integration.jenkins', {
                url: '/jenkins',
                data: {
                    classes: 'p-integration-jenkins',
                    requireLogin: true,
                    title: jenkinsInfoData.title,
                    breadcrumbsData: [
                        {
                            title: 'Settings',
                            state: 'settings.home',
                        },
                        {
                            title: 'Integrations',
                            state: 'integrations.list',
                        },
                    ],
                },
                views: {
                    info: {
                        component: 'integrationConfigIntro',
                    },
                    configForm: {
                        component: 'jenkinsForm',
                    },
                },
                resolve: {
                    infoData: () => {
                        'ngInject';

                        jenkinsInfoData.slides = jenkinsSliderImages;

                        return jenkinsInfoData;
                    },
                    integrations: (
                        $q,
                        $state,
                        $stateParams,
                        $timeout,
                        messageService,
                        integrationsService,
                    ) => {
                        'ngInject';

                        return integrationsService.getIntegration('jenkins')
                            .then((response) => {
                                if  (!response.success && response.error?.status === 404) {
                                    response = {
                                        ...response,
                                        success: true,
                                        data: null,
                                    };
                                }

                                return response;
                            })
                            .then(({ success, data = {}, message, error }) => {
                                if (success) {
                                    return data.items || [];
                                } else {
                                    if (error?.status !== 403 && message) {
                                        messageService.error(message);
                                    }
                                    $timeout(() => {
                                        $state.go('integrations.list');
                                    });
                                    return $q.reject(message);
                                }
                            });
                    },
                },
                lazyLoad: async ($transition$) => {
                    const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                    try {
                        const mod = await import(/* webpackChunkName: "integrationJenkins" */ './modules/integration-jenkins');

                        return $ocLazyLoad.load(mod.IntegrationJenkinsModule);
                    } catch (err) {
                        throw new Error('ChunkLoadError: Can\'t load module, ' + err);
                    }
                },
            })
            .state('integration.slack', {
                url: '/slack',
                data: {
                    classes: 'p-integration-slack',
                    requireLogin: true,
                    title: slackInfoData.title,
                    breadcrumbsData: [
                        {
                            title: 'Settings',
                            state: 'settings.home',
                        },
                        {
                            title: 'Integrations',
                            state: 'integrations.list',
                        },
                    ],
                },
                views: {
                    info: {
                        component: 'integrationConfigIntro',
                    },
                    configForm: {
                        component: 'slackForm',
                    },
                },
                resolve: {
                    infoData: () => {
                        'ngIjnect';

                        slackInfoData.slides = slackSliderImages;

                        return slackInfoData;
                    },
                    integrations: (
                        $q,
                        $state,
                        $timeout,
                        messageService,
                        integrationsService,
                    ) => {
                        'ngInject';

                        return integrationsService.getIntegration('slack')
                            .then((response) => {
                                if  (!response.success && response.error?.status === 404) {
                                    response = {
                                        ...response,
                                        success: true,
                                        data: null,
                                    };
                                }

                                return response;
                            })
                            .then(({ success, data, message, error }) => {
                                if (success) {
                                    return data ? [data] : [];
                                } else {
                                    if (error?.status !== 403 && message) {
                                        messageService.error(message);
                                    }
                                    $timeout(() => {
                                        $state.go('integrations.list');
                                    });
                                    return $q.reject(message);
                                }
                            });
                    },
                },
                lazyLoad: async ($transition$) => {
                    const $ocLazyLoad = $transition$.injector().get('$ocLazyLoad');

                    try {
                        const mod = await import(/* webpackChunkName: "integrationSlack" */ './modules/integration-slack');

                        return $ocLazyLoad.load(mod.IntegrationSlackModule);
                    } catch (err) {
                        throw new Error('ChunkLoadError: Can\'t load module, ' + err);
                    }
                },
            });
    })

    .name;

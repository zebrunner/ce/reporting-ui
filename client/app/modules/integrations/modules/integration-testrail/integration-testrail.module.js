import { testrailFormComponent } from './components/testrail-form';

import testRail from '../../../../../assets/images/_icons_tools/testrail.svg';

export const IntegrationTestrailModule = angular.module('integration-testrail', [])

    .component('testrailForm', testrailFormComponent)

    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('testRail', testRail);
    });

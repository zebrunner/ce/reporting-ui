const testrailFormController = function testrailFormController(
    messageService,
    integrationsService,
) {
    'ngInject';

    const apiPath = 'testrail';
    const modelFieldsMap  = {
        accessKey: 'password',
        hubUrl: 'url',
    };
    const modelFieldsReversedMap  = {
        password: 'accessKey',
        url: 'hubUrl',
    };
    const vm = {
        activeIntegration: null,
        customLabels: {
            accessKey: 'Password',
        },
        isSaving: false,

        $onInit,
        onSaveIntegration,
        onTestIntegrationConnection,
        onUpdateIntegration,
    };

    return vm;

    function $onInit() {
        if (vm.integrations?.length) {
            vm.integrations[0].encrypted = true;
            vm.activeIntegration = mapModelFields(modelFieldsReversedMap, vm.integrations[0]);
        }
    }

    function onSaveIntegration({ integration, callback = angular.noop } = {}) {
        if (integration) {
            const mappedIntegration = mapModelFields(modelFieldsMap, integration);

            integrationsService.createIntegration(apiPath, mappedIntegration)
                .then(({ success, message, data: savedIntegration }) => {
                    if (success) {
                        vm.activeIntegration =  { ...mapModelFields(modelFieldsReversedMap, savedIntegration) };
                        messageService.success('Integration changes were successfully saved');
                    } else if (message) {
                        messageService.error(message);
                    }

                    callback(success ? 'success' : 'error');
                });
        }
    }

    function onUpdateIntegration({ integration, callback = angular.noop } = {}) {
        if (integration) {
            const patchData = [{
                op: 'replace',
                path: '/enabled',
                value: !integration.enabled,
            }];

            integrationsService.updateIntegration(apiPath, patchData)
                .then(({ success, message }) => {
                    if (success) {
                        integration.enabled = !integration.enabled;
                        vm.activeIntegration =  { ...integration };
                        messageService.success(`The integration was successfully ${integration.enabled ? 'enabled': 'disabled'}`);
                    } else if (message) {
                        messageService.error(message);
                    }
                })
                .finally(callback);
        }
    }

    function onTestIntegrationConnection({ integration = {}, callback = angular.noop } = {}) {
        const mappedIntegration = mapModelFields(modelFieldsMap, integration);

        integrationsService.testIntegration(apiPath, mappedIntegration)
            .then(({ success, data = {}, error }) => {
                let state = 'success';
                let responseMessages = [];

                switch (true) {
                    case !success:
                        state = 'error';
                        if (error?.data?.message) {
                            responseMessages = [error.data.message];
                        }
                        break;
                    case success && !data.reachable:
                        state = 'fail';
                        if (data.message) {
                            responseMessages = (data.message)
                                .split(/\n/g)
                                .filter(Boolean);
                        }
                        break;
                }
                callback(state, responseMessages);
            });
    }

    function mapModelFields(fieldsMap, integration) {
        return  Object.keys(integration).reduce((accum, key) => {
            const keyToUse = fieldsMap[key] || key;
            accum[keyToUse]  = integration[key];

            return accum;
        }, {});
    }
};

export default testrailFormController;

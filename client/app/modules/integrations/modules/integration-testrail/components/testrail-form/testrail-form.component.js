import controller from './testrail-form.controller';
import template from './testrail-form.html';
import './testrail-form.scss';

export const testrailFormComponent = {
    controller,
    template,
    bindings: {
        integrations: '<',
    },
};

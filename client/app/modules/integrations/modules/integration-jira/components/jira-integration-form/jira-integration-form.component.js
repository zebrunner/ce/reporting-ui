import controller from './jira-integration-form.controller';
import template from './jira-integration-form.html';
import './jira-integration-form.scss';

export const jiraIntegrationFormComponent = {
    controller,
    template,
    bindings: {
        integrations: '<',
    },
};

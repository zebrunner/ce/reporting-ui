const jiraIntegrationFormController = function jiraIntegrationFormController(
    $timeout,
    messageService,
    integrationsService,
    UtilService,
) {
    'ngInject';

    const apiPath = 'jira';
    const connectionMessageDisplayTime = 30000;
    const integrationTypes = [
        {
            label: 'Server / DC',
            value: 'SERVER_DC',
        },
        {
            label: 'Cloud',
            value: 'CLOUD',
        },
    ];
    const userInputKeys = [
        'url',
        'token',
        'username',
    ];
    const jiraInitialState = {
        enabled: false,
        encrypted: false,
        url: '',
        token: '',
        username: '',
        type: integrationTypes[0].value,
    };
    const validations = {
        url: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 250 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 250 characters',
                value: 250,
            },
            {
                name: 'pattern',
                message: 'Entered value is not a valid URL',
                value: /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
        textField: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 100 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 100 characters',
                value: 100,
            },
            {
                name: 'pattern',
                message: 'Spaces are not allowed',
                value: /^\S+$/,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
    };
    const vm = {
        jiraIntegration: null,
        jiraIntegrationSource: null,
        isSaving: false,
        isWarningVisible: false,
        testingMessageTimeout: null,
        testingStatus: 'initial',
        testingStatusMessages: null,

        $onInit,
        closeWarningMessage,
        getValidationValue,
        onCancelFormChanges,
        onIntegrationTypeChange,
        onSaveIntegration,
        onTestIntegrationConnection,
        onTokenChange,
        onUpdateIntegration,

        get integrationTypes() { return integrationTypes; },
        get isNewJiraIntegration() { return !vm.jiraIntegrationSource; },
        get isTouchDevice() { return UtilService.isTouchDevice(); },
        get validations() { return validations; },
    };

    return vm;

    function $onInit() {
        initWarningMessageState();
        initIntegrationModels();
    }

    function initIntegrationModels() {
        if (Array.isArray(vm.integrations) && vm.integrations[0]) {
            setIntegrationModels(vm.integrations[0]);
        } else {
            setCleanIntegrationModels();
        }
    }

    function setIntegrationModels(integrationData) {
        vm.jiraIntegrationSource = integrationData;
        vm.jiraIntegration = { ...integrationData };
    }

    function resetIntegrationModels() {
        vm.jiraIntegrationSource = null;
        setCleanIntegrationModels();
    }

    function setCleanIntegrationModels() {
        vm.jiraIntegration = { ...jiraInitialState };
    }

    function restoreIntegrationModels() {
        vm.jiraIntegration = vm.jiraIntegrationSource ? { ...vm.jiraIntegrationSource } : { ...jiraInitialState };
    }

    function onCancelFormChanges($form) {
        UtilService.untouchForm($form);
        restoreIntegrationModels();
        resetTestingStatus();
    }

    function onSaveIntegration($form) {
        if ($form.$invalid) { return; }

        let integration = { ...vm.jiraIntegration };
        const isNewIntegration = !vm.jiraIntegrationSource?.token;

        integration.enabled = isNewIntegration || integration.enabled;
        vm.isSaving = true;
        integrationsService.createIntegration(apiPath, integration)
                .then(({ success, message, data }) => {
                    if (success) {
                        setIntegrationModels(data);
                        messageService.success('Integration changes were successfully saved');
                        UtilService.untouchForm($form);
                    } else if (message) {
                        messageService.error(message);
                    }
                })
                .finally(() => vm.isSaving = false);
    }

    function onUpdateIntegration($form) {
        if ($form.$invalid) { return; }

        const patchData = [{
            op: 'replace',
            path: '/enabled',
            value: !vm.jiraIntegration.enabled,
        }];

        vm.isSaving = true;
        integrationsService.updateIntegration(apiPath, patchData)
            .then(({ success, message }) => {
                if (success) {
                    vm.jiraIntegration.enabled = !vm.jiraIntegration.enabled;
                    vm.jiraIntegrationSource = { ...vm.jiraIntegration };
                    messageService.success(`The integration was successfully ${vm.jiraIntegration.enabled ? 'enabled': 'disabled'}`);
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => vm.isSaving = false);
    }

    function onTestIntegrationConnection() {
        vm.isSaving = true;
        vm.testingStatus = 'loading';

        const jiraIntegrationData = {
            url: vm.jiraIntegration.url,
            username: vm.jiraIntegration.username,
            token: vm.jiraIntegration.token,
            encrypted: vm.jiraIntegration.encrypted,
            type: vm.jiraIntegration.type,
        };

        integrationsService.testIntegration(apiPath, jiraIntegrationData)
            .then(handleTestResponse)
            .finally(() => vm.isSaving = false);
    }

    function handleTestResponse({ success, data = {}, error }) {
        let state = 'success';
        let responseMessages = [];

        switch (true) {
            case !success:
                state = 'error';
                if (error?.data?.message) {
                    responseMessages = [error.data.message];
                }
                break;
            case success && !data.reachable:
                state = 'fail';
                if (data.message) {
                    responseMessages = (data.message)
                        .split(/\n/g)
                        .filter(Boolean);
                }
                break;
        }

        vm.testingStatus = state;
        vm.testingStatusMessages = responseMessages;

        if (vm.testingMessageTimeout) {
            $timeout.cancel(vm.testingMessageTimeout);
        }

        vm.testingMessageTimeout = $timeout(() => {
            vm.testingStatus = 'initial';
            vm.testingStatusMessages = null;
        }, connectionMessageDisplayTime);
    }

    function resetTestingStatus() {
        if (vm.testingStatus !== 'initial') {
            if (vm.testingMessageTimeout) {
                $timeout.cancel(vm.testingMessageTimeout);
            }
            vm.testingStatus = 'initial';
            vm.testingStatusMessages = null;
        }
    }

    function getValidationValue(field, propName) {
        const validationsArray = vm.validations[field];
        const item = validationsArray.find(item => item.name === propName);

        return item ? item.value : null;
    }

    function onTokenChange(fieldData) {
        if (!vm.jiraIntegrationSource || vm.jiraIntegration.type !== vm.jiraIntegrationSource.type) { return false; }

        const isSourceEncrypted = !!vm.jiraIntegrationSource.encrypted;
        const isFieldChanged = !fieldData.$pristine;
        const isTokenChanged = vm.jiraIntegrationSource.token !== vm.jiraIntegration.token;

        vm.jiraIntegration.encrypted = isSourceEncrypted && (!isFieldChanged || !isTokenChanged);
    }

    function onIntegrationTypeChange($form) {
        const currentType = vm.jiraIntegration.type;

        UtilService.untouchForm($form);
        if (vm.jiraIntegrationSource && currentType === vm.jiraIntegrationSource.type) {
            restoreIntegrationModels();
        } else {
            setCleanIntegrationModels();
            vm.jiraIntegration.type = currentType;
        }
        resetTestingStatus();
    }

    function initWarningMessageState() {
        vm.isWarningVisible = localStorage.getItem('zeb-jira-integration-warning-hide') !== 'true';
    }

    function closeWarningMessage() {
        localStorage.setItem('zeb-jira-integration-warning-hide', 'true');
        vm.isWarningVisible = false;
    }
};

export default jiraIntegrationFormController;

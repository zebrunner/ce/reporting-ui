import { jiraIntegrationFormComponent } from './components/jira-integration-form';

import jiraIcon from '../../../../../assets/images/_icons_tools/jira.svg';

export const IntegrationJiraModule = angular.module('integration-jira', [])

    .component('jiraIntegrationForm', jiraIntegrationFormComponent)

    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('jira', jiraIcon);
    });

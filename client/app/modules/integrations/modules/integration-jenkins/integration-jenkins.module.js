import { jenkinsFormComponent } from './components/jenkins-form';

import jenkins from '../../../../../assets/images/_icons_tools/ci-jenkins.svg';

export const IntegrationJenkinsModule = angular.module('integration-jenkins', [])

    .component('jenkinsForm', jenkinsFormComponent)

    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('jenkins', jenkins);
    });

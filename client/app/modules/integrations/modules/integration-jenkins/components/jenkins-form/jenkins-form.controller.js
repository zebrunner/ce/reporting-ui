import warningModalController from '../../../../../../shared/modals/warning-modal/warning-modal.controller';
import warningModalTemplate from '../../../../../../shared/modals/warning-modal/warning-modal.html';

const jenkinsFormController = function jenkinsFormController(
    $mdDialog,
    messageService,
    integrationsService,
) {
    'ngInject';

    const apiPath = 'jenkins';
    const modelFieldsMap  = {
        accessKey: 'token',
        hubUrl: 'url',
    };
    const modelFieldsReversedMap  = {
        token: 'accessKey',
        url: 'hubUrl',
    };
    const vm = {
        activeIntegration: null,
        isSaving: false,

        $onInit,
        onDeleteIntegration,
        onIntegrationSelect,
        onSaveIntegration,
        onTestIntegrationConnection,
        onUpdateIntegration,
    };

    return vm;

    function $onInit() {
        if (vm.integrations?.length) {
            vm.integrations[0].encrypted = true;
            vm.activeIntegration = mapModelFields(modelFieldsReversedMap, vm.integrations[0]);
        }
    }

    function onSaveIntegration({ integration, callback = angular.noop } = {}) {
        if (integration) {
            const mappedIntegration = mapModelFields(modelFieldsMap, integration);

            integrationsService.createIntegration(apiPath, mappedIntegration)
                .then(({ success, message, data: savedIntegration }) => {
                    if (success) {
                        vm.activeIntegration =  { ...mapModelFields(modelFieldsReversedMap, savedIntegration) };
                        messageService.success('Integration changes were successfully saved');
                    } else if (message) {
                        messageService.error(message);
                    }

                    callback(success ? 'success' : 'error');
                });
        }
    }

    function onUpdateIntegration({ integration, callback = angular.noop } = {}) {
        if (integration) {
            const patchData = [{
                op: 'replace',
                path: '/enabled',
                value: !integration.enabled,
            }];

            integrationsService.updateIntegration(apiPath, patchData, integration.id)
                .then(({ success, message }) => {
                    if (success) {
                        const updatingIntegration = vm.integrations.find(({ id }) => id === integration.id);

                        integration.enabled = !integration.enabled;
                        vm.activeIntegration = { ...integration };
                        updatingIntegration.enabled = vm.activeIntegration.enabled;
                        messageService.success(`The integration was successfully ${integration.enabled ? 'enabled': 'disabled'}`);
                    } else if (message) {
                        messageService.error(message);
                    }
                })
                .finally(callback);
        }
    }

    function onDeleteIntegration({ integration, callback = angular.noop } = {}) {
        $mdDialog
            .show({
                controller: warningModalController,
                controllerAs: '$ctrl',
                template: warningModalTemplate,
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    title: 'Delete Integration',
                    message: 'Please note! This integration will be deleted immediately. You can\'t undo this action.',
                    warningMessage: `Are you sure you want to delete the${integration.name ? (' "' + integration.name + '"') : ''} integration?`,
                    actionText: 'Delete',
                },
            })
            .then(() => {
                integrationsService.deleteIntegration(apiPath)
                    .then(({ success, message }) => {
                        if (success) {
                            vm.activeIntegration =  null;
                            messageService.success('The integration was successfully deleted');
                        } else if (message) {
                            messageService.error(message);
                        }
                    })
                    .finally(callback);
            })
            .catch(callback);
    }

    function onTestIntegrationConnection({ integration = {}, callback = angular.noop } = {}) {
        const mappedIntegration = mapModelFields(modelFieldsMap, integration);

        integrationsService.testIntegration(apiPath, mappedIntegration)
            .then(({ success, data = {}, error }) => {
                let state = 'success';
                let responseMessages = [];

                switch (true) {
                    case !success:
                        state = 'error';
                        if (error?.data?.message) {
                            responseMessages = [error.data.message];
                        }
                        break;
                    case success && !data.reachable:
                        state = 'fail';
                        if (data.message) {
                            responseMessages = (data.message)
                                .split(/\n/g)
                                .filter(Boolean);
                        }
                        break;
                }
                callback(state, responseMessages);
            });
    }

    function onIntegrationSelect(selectedIntegration) {
        if (selectedIntegration?.id === vm.activeIntegration?.id) { return; }

        vm.activeIntegration = mapModelFields(modelFieldsReversedMap, selectedIntegration);
    }

    function mapModelFields(fieldsMap, integration) {
        return  Object.keys(integration).reduce((accum, key) => {
            const keyToUse = fieldsMap[key] || key;
            accum[keyToUse]  = integration[key];

            return accum;
        }, {});
    }
};

export default jenkinsFormController;

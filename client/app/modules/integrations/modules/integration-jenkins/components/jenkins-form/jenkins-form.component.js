import controller from './jenkins-form.controller';
import template from './jenkins-form.html';
import './jenkins-form.scss';

export const jenkinsFormComponent = {
    controller,
    template,
    bindings: {
        integrations: '<',
    },
};

import { slackFormComponent } from './components/slack-form';

import slack from '../../../../../assets/images/_icons_tools/slack.svg';

export const IntegrationSlackModule = angular.module('integration-slack', [])

    .component('slackForm', slackFormComponent)

    .config(($mdIconProvider) => {
        'ngInject';

        $mdIconProvider
            .icon('slack', slack);
    });

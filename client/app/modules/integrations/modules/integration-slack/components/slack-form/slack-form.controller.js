import uploadImageModalController from '../../../../../../shared/modals/upload-image-modal/upload-image-modal.controller';
import uploadImageModalTemplate from '../../../../../../shared/modals/upload-image-modal/upload-image-modal.html';

const slackFormController = function slackFormController(
    $mdDialog,
    $q,
    $timeout,
    messageService,
    integrationsService,
    UtilService,
) {
    'ngInject';

    const apiPath = 'slack';
    const connectionMessageDisplayTime = 30000;
    const slackInitialState = {
        token: '',
        botName: '',
        botIconUrl: null,
        enabled: false,
        encrypted: false,
    };
    const validations = {
        token: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 250 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 250 characters',
                value: 250,
            },
            {
                name: 'pattern',
                message: 'Spaces are not allowed',
                value: /^\S+$/,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
        botName: [
            {
                name: 'minlength',
                message: 'Must be between 1 and 100 characters',
                value: 1,
            },
            {
                name: 'maxlength',
                message: 'Must be between 1 and 100 characters',
                value: 100,
            },
            {
                name: 'required',
                message: 'This field is required',
            },
        ],
    };
    const vm = {
        isSaving: false,
        slackIntegration: null,
        slackIntegrationSource: null,
        isWarningVisible: false,
        testingMessageTimeout: null,
        testingStatus: 'initial',
        testingStatusMessages: null,

        $onInit,
        getValidationValue,
        onSaveIntegration,
        onTestIntegrationConnection,
        onUpdateIntegration,
        onCancelFormChanges,
        onTokenChange,
        showUploadImageDialog,

        get isTouchDevice() { return UtilService.isTouchDevice(); },
        get validations() { return validations; },
    };

    return vm;

    function $onInit() {
        initIntegrationModels();
    }

    function initIntegrationModels() {
        if (Array.isArray(vm.integrations) && vm.integrations[0]) {
            setIntegrationModels(vm.integrations[0]);
        } else {
            setCleanIntegrationModels();
        }
    }

    function onSaveIntegration($form) {
        if ($form.$invalid) { return; }

        const isNewIntegration = !vm.slackIntegrationSource?.token;
        let integration = { ...vm.slackIntegration };

        Reflect.deleteProperty(integration, 'encrypted');
        integration.enabled = isNewIntegration || integration.enabled;
        vm.isSaving = true;
        integrationsService.createIntegration(apiPath, integration)
            .then(({ success, message, data }) => {
                if (success) {
                    setIntegrationModels(data);
                    messageService.success('Integration changes were successfully saved');
                    UtilService.untouchForm($form);
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => vm.isSaving = false);
    }

    function onUpdateIntegration($form) {
        if ($form.$invalid) { return; }

        const patchData = [{
            op: 'replace',
            path: '/enabled',
            value: !vm.slackIntegration.enabled,
        }];

        vm.isSaving = true;
        integrationsService.updateIntegration(apiPath, patchData)
            .then(({ success, message }) => {
                if (success) {
                    vm.slackIntegration.enabled = !vm.slackIntegration.enabled;
                    vm.slackIntegrationSource = { ...vm.slackIntegration };
                    messageService.success(`The integration was successfully ${vm.slackIntegration.enabled ? 'enabled': 'disabled'}`);
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => vm.isSaving = false);
    }

    function setIntegrationModels(integrationData) {
        const slackIntegration = integrationData;

        vm.slackIntegrationSource = slackIntegration;
        vm.slackIntegration = { ...slackIntegration };
    }

    function getValidationValue(field, propName) {
        const validationsArray = vm.validations[field];
        const item = validationsArray.find(item => item.name === propName);

        return item ? item.value : null;
    }

    function onCancelFormChanges($form) {
        UtilService.untouchForm($form);
        restoreIntegrationModels();
        resetTestingStatus();
    }

    function restoreIntegrationModels() {
        vm.slackIntegration = vm.slackIntegrationSource ? { ...vm.slackIntegrationSource } : { ...slackInitialState };
    }

    function resetTestingStatus() {
        if (vm.testingStatus !== 'initial') {
            if (vm.testingMessageTimeout) {
                $timeout.cancel(vm.testingMessageTimeout);
            }
            vm.testingStatus = 'initial';
            vm.testingStatusMessages = null;
        }
    }

    function onTestIntegrationConnection() {
        vm.isSaving = true;
        vm.testingStatus = 'loading';

        const slackIntegrationData = {
            token: vm.slackIntegration.token,
            botName: vm.slackIntegration.botName,
            botIconUrl: vm.slackIntegration.botIconUrl,
            encrypted: vm.slackIntegration.encrypted,
        };

        integrationsService.testIntegration(apiPath, slackIntegrationData)
                .then(handleTestResponse)
                .finally(() => vm.isSaving = false);
    }

    function handleTestResponse({ success, data = {}, error }) {
        let state = 'success';
        let responseMessages = [];

        switch (true) {
            case !success:
                state = 'error';
                if (error?.data?.message) {
                    responseMessages = [error.data.message];
                }
                break;
            case success && !data.reachable:
                state = 'fail';
                if (data.message) {
                    responseMessages = (data.message)
                        .split(/\n/g)
                        .filter(Boolean);
                }
                break;
        }

        vm.testingStatus = state;
        vm.testingStatusMessages = responseMessages;

        if (vm.testingMessageTimeout) {
            $timeout.cancel(vm.testingMessageTimeout);
        }

        vm.testingMessageTimeout = $timeout(() => {
            vm.testingStatus = 'initial';
            vm.testingStatusMessages = null;
        }, connectionMessageDisplayTime);
    }

    function onTokenChange(fieldData) {
        if (!vm.slackIntegrationSource) { return false; }

        const isSourceEncrypted = !!vm.slackIntegrationSource.encrypted;
        const isFieldChanged = !fieldData.$pristine;
        const isTokenChanged = vm.slackIntegrationSource.token !== vm.slackIntegration.token;

        vm.slackIntegration.encrypted = isSourceEncrypted && (!isFieldChanged || !isTokenChanged);
    }

    function setCleanIntegrationModels() {
        vm.slackIntegration = { ...slackInitialState };
    }

    function showUploadImageDialog($event, $form) {
        $mdDialog.show({
            controller: uploadImageModalController,
            controllerAs: '$ctrl',
            template: uploadImageModalTemplate,
            parent: angular.element(document.body),
            targetEvent: $event,
            clickOutsideToClose: true,
            multiple: true,
            locals: {
                urlHandler: (url) => {
                    if (url) {
                        vm.slackIntegration.botIconUrl = url;

                        return $q.resolve(true);
                    }

                    return $q.reject(false);
                },
                fileTypes: 'ORG_ASSET',
            },
        })
        .then((res) => {
            if (res && $form.$valid) {
                vm.isSaving = false;
                $form.$pristine = false;
            }
        });
    }

    function resetIntegrationModels() {
        vm.slackIntegrationSource = null;
        setCleanIntegrationModels();
    }
};

export default slackFormController;

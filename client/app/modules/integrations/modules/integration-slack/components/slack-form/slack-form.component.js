import controller from './slack-form.controller';
import template from './slack-form.html';
import './slack-form.scss';

export const slackFormComponent = {
    controller,
    template,
    bindings: {
        integrations: '<',
    },
};
import dashboardsListComponent from './dashboards-list.component';
import '../../../styles/layout/ze-tables.scss';

export const dashboardsListModule = angular
    .module('dashboardsList', [])
    .component({ dashboardsListComponent });

import dashboardModalController from '../../shared/modals/dashboard-settings-modal/dashboard-settings-modal.controller';
import dashboardModal from '../../shared/modals/dashboard-settings-modal/dashboard-settings-modal.html';

const dashboardsListController = function dashboardsListController(
    $mdDialog,
    $q,
    $state,
    $timeout,
    authService,
    DashboardsService,
    messageService,
    pageTitleService,
    UserService,
    UtilService,
) {
    'ngInject';

    const initialCountToDisplay = 100;
    const scrollableElement = document.querySelector('.dashboards-table__container');
    const DEFAULT_SC = {
        page: 1,
        pageSize: initialCountToDisplay,
        sortBy: null,
        sortOrder: 'ASC',
        query: null,
    };
    const vm = {
        status: 'pending',
        dashboards: [],
        sc: angular.copy(DEFAULT_SC),

        $onInit,
        userHasAnyPermission: authService.userHasAnyPermission,
        isDashboardsEmpty,
        showDashboardDialog,
        goToDashboard,
        handleSortCriteriaChange,
        getAllDashboards,

        get currentSortCriteria() { return vm.sc.sortBy },
        get currentSortOrder() { return vm.sc.sortOrder },
        get currentTitle() { return pageTitleService.pageTitle; },
        get currentUser() { return UserService.currentUser; },
        get dashboardList() { return DashboardsService.dashboards; },
        get isTouchDevice() { return UtilService.isTouchDevice(); },
    };

    return vm;

    function $onInit() {
        if (vm.dashboardsData) {
            $timeout(() => {
                vm.dashboards = vm.dashboardsData || [];
                vm.status = 'success';
            }, 500);
        }
    }

    function getAllDashboards(reset) {
        if (reset) { vm.sc.query = ''; }
        DashboardsService.GetDashboards(vm.sc)
            .then(({ success, data, message }) => success
                ? $q.resolve(data)
                : $q.reject(message))
            .then((data) => {
                vm.dashboards = data.results || [];
                vm.status = 'completed';
                $timeout(() => {
                    scrollableElement.scrollTop = 0;
                }, 0, false);
            })
            .catch(message => {
                vm.status = 'error';

                if (message) {
                    messageService.error(message);
                }
            });
    }

    function isDashboardsEmpty() {
        return vm.dashboards && !vm.dashboards.length;
    }

    function showDashboardDialog($event, dashboard = {}, status = null) {
        $mdDialog.show({
            controller: dashboardModalController,
            template: dashboardModal,
            parent: angular.element(document.body),
            targetEvent: $event,
            clickOutsideToClose: true,
            fullscreen: false,
            autoWrap: false,
            locals: {
                dashboard,
                status,
                defaultDataForRequest: vm.sc,
                allDashboards: vm.dashboards,
            },
        })
            .then((rs) => {
                if (rs && !status) {
                    return goToDashboard(rs);
                }
                getAllDashboards();
            });
    }

    function goToDashboard(dashboard) {
        $state.go('dashboard.page', { dashboardId: dashboard.id });
    }

    function handleSortCriteriaChange(criteria) {
        if (!vm.sc || !criteria) { return; }

        vm.sc.sortOrder = vm.sc?.sortBy?.toLowerCase() === criteria.toLowerCase() && vm.sc.sortOrder === 'ASC' ? 'DESC' : 'ASC';
        vm.sc.sortBy = vm.sc?.sortBy?.toLowerCase() === criteria.toLowerCase() && vm.sc.sortOrder === 'ASC' ? null : criteria;
        getAllDashboards();
    }
}

export default dashboardsListController;

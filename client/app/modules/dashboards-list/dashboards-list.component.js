import template from './dashboards-list.html';
import controller from './dashboards-list.controller';

const dashboardsListComponent = {
    template,
    controller,
    bindings: {
        dashboardsData: '<',
    },
};

export default dashboardsListComponent;
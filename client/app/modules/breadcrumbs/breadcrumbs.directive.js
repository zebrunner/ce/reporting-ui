'use strict';

import controller from './breadcrumbs.controller';
import template from './breadcrumbs.html';
require('./breadcrumbs.scss');

const breadcrumbsDirective = function () {
    return {
        template,
        controller,
        scope: {
            customActions: '=',
        },
        controllerAs: '$ctrl',
        restrict: 'E',
        replace: true,
        bindToController: true,
    };
};

export default breadcrumbsDirective;
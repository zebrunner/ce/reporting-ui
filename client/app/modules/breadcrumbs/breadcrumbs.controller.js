'use strict';

const breadcrumbsController = function breadcrumbsController(
    $state,
    $mdMedia,
) {
    'ngInject';

    const vm = {
        breadcrumbs: null,
        state: $state,
        customActions: null,

        $onInit,
        getBreadcrumbTitle,

        get isMobile() { return $mdMedia('xs'); },
        get first() { return vm.breadcrumbs[0]; },
        get middle() { return vm.breadcrumbs.slice(1, vm.breadcrumbs.length - 1); },
        get last() { return vm.breadcrumbs[vm.breadcrumbs?.length - 1]; },
    };

    return vm;

    function $onInit() {
        vm.breadcrumbs = $state.current?.data?.breadcrumbsData || [];
    }

    function getBreadcrumbTitle(breadcrumb) {
        if (vm.customActions && vm.customActions[breadcrumb.title]?.customItemTitle) {
            return vm.customActions[breadcrumb.title].customItemTitle;
        }

        return breadcrumb.title;
    }
};

export default breadcrumbsController;

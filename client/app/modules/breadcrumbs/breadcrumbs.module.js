'use strict';

import appBreadcrumbs from './breadcrumbs.directive';

export const AppBreadcrumbsModule = angular.module('appBreadcrumbs', [])
    .directive({ appBreadcrumbs })

    .name;

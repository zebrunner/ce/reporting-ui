import template from './groups.html';
import controller from './groups.controller';
import './groups.scss';

const groupsComponent = {
    template,
    controller,
};

export default groupsComponent;

'use strict';

const appHeaderController = function(
    $state,
    $mdMedia,
    authService,
) {
    'ngInject';

    const vm = {
        isOpenedMenu: false,
        menuButtonEl: null,
        isOpenForm: false,

        changeMobileMenuIcon,
        checkActiveMenuItem,
        onSwipeUp,
        openForm,
        openMenu,
        
        userHasAnyPermission: authService.userHasAnyPermission,
        $onInit() {
            vm.menuButtonEl = document.querySelector('.app-header__burger-button-icon');
        },
        get isMobile() { return $mdMedia('xs'); },
        get isTablet() {return $mdMedia('max-width: 768px'); },
    };

    return vm;

    function changeMobileMenuIcon() {
        vm.isOpenedMenu = !vm.isOpenedMenu;
        if (vm.isOpenedMenu) {
            vm.menuButtonEl?.style.setProperty('--afterAnimation', 'bottomMenuElActive .25s ease');
            vm.menuButtonEl?.style.setProperty('--beforeAnimation', 'topMenuElActive .25s ease');
        } else {
            vm.menuButtonEl?.style.setProperty('--afterAnimation', 'bottomMenuElInactive .25s ease');
            vm.menuButtonEl?.style.setProperty('--beforeAnimation', 'topMenuElInactive .25s ease');
        };
    }

    function checkActiveMenuItem(path) {
        return $state.includes(path);
    }

    function onSwipeUp(event, target) {
        if (vm.isOpenedMenu) {
            changeMobileMenuIcon();
        }
    }

    function openMenu() {
        vm.isOpenedMenu = !vm.isOpenedMenu;
    }

    function openForm(status) {
        vm.isOpenForm = status;
    }

};

export default appHeaderController;

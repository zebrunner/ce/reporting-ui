'use strict';

import appSidebar from './app-sidebar.directive';
import usersIcon from '../../../../assets/images/_icons_sidebar/users.svg';
import rocketIcon from '../../../../assets/images/_icons_sidebar/rocket.svg';
import analyticsIcon from '../../../../assets/images/_icons_sidebar/analytics.svg';
import connectIcon from '../../../../assets/images/_icons_sidebar/connect.svg';

angular.module('app.appSidebar', [])
    .directive({ appSidebar })
    .config(function ($mdIconProvider) {
        'ngInject';

        $mdIconProvider
            .icon('analyticsIcon', analyticsIcon)
            .icon('rocketIcon', rocketIcon)
            .icon('connectIcon', connectIcon)
            .icon('usersIcon', usersIcon)
            ;
    })

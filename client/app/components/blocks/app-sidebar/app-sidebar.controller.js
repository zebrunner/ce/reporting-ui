'use strict';

const AppSidebarController = function (
    $mdMedia,
    $state,
    $transitions,
    authService,
    mainMenuService,
    UserService,
) {
    'ngInject';

    let navElem = null;
    let navContainerElem = null;
    let transSubscription;
    let isMainMenuOpened = false;
    const mainMenuConfig = {
        liSelector: 'main-nav__list-item',
        openClassifier: 'open',
        mobileClass: 'toggle-bottom',
    };
    // breakpoint when menu ribbon becomes hidden
    const menuMobileBreakpoint = 600;
    const DEFAULT_SC = {
        page: 1,
        pageSize: 10000,
        sortBy: null,
        sortOrder: 'ASC',
        query: null,
    };

    const vm = {
        sc: angular.copy(DEFAULT_SC),
        version: null,
        $state,

        userHasAnyPermission: authService.userHasAnyPermission,
        handleMenuClick,
        $onDestroy() { unbindListeners(); },
        toggleMobileMenu,
        handleExpandButtonClick,
        isActiveLink,
        isMenuItemVisible,

        get currentUser() { return UserService.currentUser; },
        get isMobile() { return $mdMedia('xs'); },
        get isDesktop() { return $mdMedia('gt-md'); },
        get menuItems() { return mainMenuService.items; },
        get isSidebarExpanded() { return mainMenuService.isSidebarExpanded; },
    };

    vm.$onInit = initController;

    return vm;

    function initController() {
        setTimeout(() => {
            navElem = document.querySelector('.main-nav__list');
            navContainerElem = navElem.closest('#nav-container');
            bindListeners();
        }, 0);
    }

    function isActiveLink(state) {
        return $state.current.name.includes(state)
            || $state.current.name.includes(state.slice(0, -1))
            || $state.current.name.split('.').includes(state.split('.')[0]);
    }

    function bindListeners() {
        if (navElem) {
            transSubscription = $transitions.onBefore({}, closeMenuOnRouteTransition);
            window.addEventListener('resize', closeMenuOnResize);
        }
    }

    function unbindListeners() {
        if (transSubscription) {
            transSubscription();
        }
        window.removeEventListener('resize', closeMenuOnResize);
    }

    function closeMenuOnRouteTransition() {
        closeMenu();
    }

    function closeMenuOnResize() {
        if (isMainMenuOpened) {
            closeMenu();
        }
    }

    // TODO: refactor (all states need such params?)
    function handleMenuClick(state) {
        if (!navElem || !vm.currentUser.onboardingCompleted || !state) { return; }

        $state.go(state);
    }

    function handleExpandButtonClick() {
        mainMenuService.isSidebarExpanded = !mainMenuService.isSidebarExpanded;
    }

    function toggleMobileMenu() {
        if (navContainerElem.classList.contains(mainMenuConfig.mobileClass)) {
            navContainerElem.classList.remove(mainMenuConfig.mobileClass);
            closeMenu();
            removeClickListenerOnDocument();
        } else {
            navContainerElem.classList.add(mainMenuConfig.mobileClass);
            addClickListenerOnDocument();
        }
    }

    function addClickListenerOnDocument() {
        document.addEventListener('click', closeMobileMenuOnOutsideClick);
    }

    function removeClickListenerOnDocument() {
        document.removeEventListener('click', closeMobileMenuOnOutsideClick);
    }

    function closeMobileMenuOnOutsideClick(event) {
        let targetElement = event.target;
        while (targetElement) {
            if (targetElement.classList.contains('main-nav__container')) {
                return;
            }
            targetElement = targetElement.parentElement;
        }
        navContainerElem.classList.remove(mainMenuConfig.mobileClass);
    }

    function closeMenu(openedLiElement = navElem.querySelector(`.${mainMenuConfig.liSelector}.${mainMenuConfig.openClassifier}`)) {
        // auto-close mobile menu
        if ($mdMedia(`max-width: ${menuMobileBreakpoint}px`) && navContainerElem.classList.contains(mainMenuConfig.mobileClass)) {
            navContainerElem.classList.remove(mainMenuConfig.mobileClass);
        }
        // there is no opened element
        if (!openedLiElement) { return; }

        openedLiElement.classList.remove(mainMenuConfig.openClassifier);
        isMainMenuOpened = false;
    }

    function isMenuItemVisible(menuItem) {
        return menuItem.permissions?.length ? vm.userHasAnyPermission(menuItem.permissions) : true;
    }

};

export default AppSidebarController;

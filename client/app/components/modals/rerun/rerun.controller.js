(function () {
    'use strict';

    angular.module('app').controller('TestRunRerunController', TestRunRerunController);

    function TestRunRerunController($scope, $mdDialog, TestRunService, testRun, toolsService, messageService) {
        'ngInject';

        $scope.testRun = testRun;
        $scope.rerunFailures = !!($scope.testRun.failed + $scope.testRun.skipped);

        Object.defineProperty($scope, 'displayFailuresOption', {
            get: () => !!($scope.testRun.failed + $scope.testRun.skipped),
        });
        Object.defineProperty($scope, 'displayAllOption', {
            get: () => !!($scope.testRun.passed + $scope.testRun.aborted),
        });

        $scope.rebuild = function (testRun, rerunFailures) {
            // TODO: refactor this after jenkins integration finished
            // if (toolsService.isToolConnected('JENKINS')) {
                TestRunService.rerunTestRun(testRun.id, rerunFailures)
                    .then(function(rs) {
                        if (rs.success) {
                            testRun.status = 'IN_PROGRESS';
                            messageService.success("Rebuild triggered in CI service");
                        } else {
                            messageService.error(rs.message);
                        }
                    });
            // } else {
            //     window.open(testRun.jenkinsURL + '/rebuild/parameterized', '_blank');
            // }

            $scope.hide(true);
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }

})();

const widgetWizardController = function WidgetWizardController(
    $scope,
    $mdDialog,
    $q,
    $widget,
    DashboardsService,
    widget,
    dashboard,
    currentUserId,
    messageService,
) {
    'ngInject';

    const vm = {
        showAdvanced: false,
        needToLoadWidgetParams: true,

        changeAdvancedFlag,
        $onInit: initController,
    };

    $scope.START_MODE = widget && widget.id ? 'UPDATE' : 'NEW';
    let MODE = $scope.START_MODE === 'UPDATE' ? 'UPDATE' : 'ADD';

    const CARDS = {
        WIDGET_TEMPLATE: {
            index: 1,
            id: 'choose',
            title: 'Choose template',
            nextDisabled: () => !$scope.widget.widgetTemplate,
            onLoad: () => {
                if (!$scope.templates) {
                    DashboardsService.GetWidgetTemplates().then((rs) => {
                        if (rs.success) {
                            $scope.templates = rs.data;
                        } else {
                            messageService.error(rs.message);
                        }
                    });
                }
            },
            scValue: 'name',
        },
        WIDGET: {
            index: 2,
            id: 'choose-create',
            title: 'Choose widget',
            nextDisabled: () => !$scope.widget.widgetTemplate,
            onLoad: () => {
                if (!$scope.widgets) {
                    DashboardsService.GetWidgets().then((rs) => {
                        if (rs.success) {
                            $scope.widgets = rs.data.filter((widget) => widget.widgetTemplate && !widget.widgetTemplate.hidden);
                            updateWidgetsToAdd();
                        } else {
                            messageService.error(rs.message);
                        }
                    });
                }
            },
            scValue: 'title',
        },
        CONFIG: {
            index: 3,
            stepCount: 1,
            id: 'set',
            title: 'Set parameters',
            nextDisabled: (form) => form.params.$invalid,
            onLoad: () => {
                if (widget.id) {
                    initLegendConfigObject();
                }
            },
        },
        INFO: {
            index: 4,
            stepCount: 2,
            id: 'save',
            title: 'Save',
            nextDisabled: () => {},
            onLoad: () => {
                if (MODE === 'UPDATE') {
                    if (!DashboardsService.dashboards.length) {
                        DashboardsService.GetDashboards().then(() => {
                            $scope.dashboards = filterDashboards();
                        });
                    } else {
                        $scope.dashboards = filterDashboards();
                    }
                }
                initLegendConfigObject();
            },
        },
    };

    const CARDS_QUEUE = {
        QUEUE: {
            NEW: [CARDS.WIDGET_TEMPLATE, CARDS.WIDGET, CARDS.CONFIG, CARDS.INFO],
            UPDATE: [CARDS.WIDGET, CARDS.CONFIG, CARDS.INFO],
        },
        STORED_WAY: [],
    };

    function filterDashboards() {
        return DashboardsService.dashboards.filter((d) => d.id !== dashboard.id && d.widgets && d.widgets.indexOfField('id', $scope.widget.id) !== -1);
    }

    function initCard(card) {
        $scope.sc = {};
        card.onLoad();
        $scope.card = card;
    }

    $scope.getCardById = (id) => {
        $scope.widget = {};
        $scope.tempData.widget = {};
        const card = getCardById(id);
        initCard(card);
        CARDS_QUEUE.STORED_WAY = [card];
    };

    $scope.isCardWithId = (id) => $scope.card.id === id;

    function getCardById(id) {
        const index = CARDS_QUEUE.QUEUE[$scope.START_MODE].indexOfField('id', id);
        return CARDS_QUEUE.QUEUE[$scope.START_MODE][index];
    }

    $scope.clearSearch = () => {
        $scope.sc[$scope.card.scValue] = '';
    };

    $scope.next = () => {
        const nextCard = getNextCard();
        initCard(nextCard);
        return nextCard;
    };

    $scope.back = () => {
        const prevCard = getPreviousCard();
        initCard(prevCard);
        return prevCard;
    };

    function getNextCard() {
        if ($scope.isLastCard()) {
            return $scope.card;
        }
        let currCardIndex = $scope.card ? CARDS_QUEUE.QUEUE[$scope.START_MODE].indexOfField('index', $scope.card.index) : -1;
        let nextCard;
        while (!nextCard || ($scope.card && $scope.card.stepCount === nextCard.stepCount)) {
            currCardIndex += 1;
            nextCard = CARDS_QUEUE.QUEUE[$scope.START_MODE][currCardIndex];
        }
        CARDS_QUEUE.STORED_WAY.push(nextCard);
        return nextCard;
    }

    function getPreviousCard() {
        if ($scope.isFirstCard()) {
            return $scope.card;
        }
        CARDS_QUEUE.STORED_WAY.pop();
        return CARDS_QUEUE.STORED_WAY[CARDS_QUEUE.STORED_WAY.length - 1];
    }

    $scope.isLastCard = () => {
        if (!$scope.card) {
            return false;
        }
        const lastCard = CARDS_QUEUE.QUEUE[$scope.START_MODE][CARDS_QUEUE.QUEUE[$scope.START_MODE].length - 1];
        return lastCard.index === $scope.card.index;
    };

    $scope.isFirstCard = () => {
        if ($scope.card) {
            return false;
        }
        const firstCard = CARDS_QUEUE.QUEUE[$scope.START_MODE][0];
        return firstCard.index === $scope.card.index;
    };

    $scope.isNextButtonPresent = () => !$scope.isLastCard();

    $scope.isBackButtonPresent = () => {
        const cardIndices = $scope.START_MODE === 'NEW' ? [3, 4] : [4];
        return !$scope.isFirstCard() && cardIndices.indexOf($scope.card.index) !== -1;
    };

    $scope.chartActions = [];

    function initLegendConfigObject() {
        $scope.widgetBuilder.legendConfigObject = $widget.buildLegend($scope.widget);
        if ($scope.widgetBuilder.legendConfigObject.legend) {
            $scope.widgetBuilder.legendConfigObject.legend.forEach((legendName) => {
                $scope.onLegendChange(legendName);
            });
        }
    }

    $scope.addToDashboard = () => {
        $scope.hide('ADD', $scope.widget);
    };

    $scope.removeWidgetFromDashboard = (widget) => {
        const confirmedDelete = confirm(`Would you like to remove widget "${ widget.title }" from dashboard ?`);
        if (confirmedDelete) {
            DashboardsService.DeleteDashboardWidget(dashboard.id, widget.id).then((rs) => {
                if (rs.success) {
                    dashboard.widgets.splice(dashboard.widgets.indexOfId(widget.id), 1);
                    const deletedWidgetIndex = $scope.widgets.indexOfField('id', widget.id);
                    $scope.widgets[deletedWidgetIndex].existing = false;
                    messageService.success('Widget was successfully deleted from dashboard');
                } else {
                    messageService.error(rs.message);
                }
            });
        }
    };

    $scope.deleteWidget = (widget) => {
        const confirmedDelete = confirm(`Would you like to delete widget "${ widget.title }" ?`);
        if (confirmedDelete) {
            DashboardsService.DeleteWidget(widget.id).then((rs) => {
                if (rs.success) {
                    $scope.widgets.splice($scope.widgets.indexOfId(widget.id), 1);
                    if (dashboard.widgets.indexOfId(widget.id) >= 0) {
                        dashboard.widgets.splice(dashboard.widgets.indexOfId(widget.id), 1);
                    }
                    updateWidgetsToAdd();
                    messageService.success('Widget deleted');
                } else {
                    messageService.error(rs.message);
                }
            });
        }
    };

    function updateWidgetsToAdd() {
        if ($scope.widgets && dashboard.widgets) {
            dashboard.widgets.forEach((w) => {
                const widgetIndex = $scope.widgets.indexOfField('id', w.id);
                if (widgetIndex !== -1) {
                    $scope.widgets[widgetIndex].existing = true;
                }
            });
        }
    }

    function changeAdvancedFlag() {
        vm.showAdvanced = !vm.showAdvanced;
    }

    $scope.widget = {};
    $scope.widgetBuilder = {};
    $scope.selectedBuilderValues = {};
    $scope.widgetParamsLoadingInProgress = false;

    $scope.onChange = () => {
        $scope.selectedBuilderValues = {}; // reset saved values on select to avoid applying params from another widget
        $scope.lastSelectedPeriod = null;
        vm.needToLoadWidgetParams = true;
        vm.showAdvanced = false;

        return $q((resolve, reject) => {
            $scope.widgetBuilder = {};
            prepareWidgetTemplate($scope.widget.widgetTemplate.id)
                .then((rs) => {
                    const paramsConfigValues = Object.keys(rs.paramsConfig);

                    paramsConfigValues.forEach((value) => {
                        if (!rs.paramsConfig[value]?.required) {
                            Reflect.deleteProperty(rs.paramsConfig, value);
                        }
                    });

                    $scope.widget.widgetTemplate = rs;
                    $scope.buildConfigs(null, true);
                    resolve();
                }, (rs) => {
                    messageService.error(rs);
                    reject();
                })
                .then(() => {
                    if ($scope.START_MODE === 'UPDATE') {
                        $scope.getWidgetParams();
                    }
                });
        });
    };

    function prepareWidgetTemplate(id) {
        return $q((resolve, reject) => {
            DashboardsService.PrepareWidgetTemplate(id)
                .then((rs) => {
                    if (rs.success) {
                        resolve(rs.data);
                    } else {
                        reject(rs.message);
                    }
                });
        });
    }

    $scope.handleRequiredParamChange = () => {
        vm.needToLoadWidgetParams = true;
        vm.showAdvanced = false;
    };

    $scope.getWidgetParams = (form) => {
        if ($scope.widgetParamsLoadingInProgress || (form && !form.$valid)) { return; }
        $scope.widgetParamsLoadingInProgress = true;

        const period = $scope.widget.widgetTemplate.paramsConfig?.PERIOD?.value
            ? $scope.widget.widgetTemplate.paramsConfig?.PERIOD?.value
            : $scope.widget.paramsConfig
                ? JSON.parse($scope.widget.paramsConfig)?.PERIOD
                : $scope.widget.widgetTemplate.paramsConfig?.PERIOD?.values[0];

        saveWidgetBuilderSelectedParams();
        $scope.lastSelectedPeriod = period;

        DashboardsService.GetWidgetTemplateParameters(
            $scope.widget.widgetTemplate.id,
            { period },
        )
            .then(({ success, data }) => {
                if (success) {
                    Object.assign($scope.widget.widgetTemplate.paramsConfig, data);
                    $scope.buildConfigs(form, true);
                    vm.needToLoadWidgetParams = false;
                }
            })
            .finally(() => {
                $scope.widgetParamsLoadingInProgress = false;
            });
    };

    $scope.refactorWidgetParamsValues = (widget) => {
        const widgetParams = Object.keys(widget.widgetTemplate.paramsConfig);

        widgetParams.forEach((item) => {
            const paramValuesArray = widget.widgetTemplate.paramsConfig[item]?.values;

            if (paramValuesArray && paramValuesArray.length) {
                widget.widgetTemplate.paramsConfig[item].values = paramValuesArray.map((item) => item === null ? 'null' : item);
            }
        });
    };

    $scope.onOptionsLoaded = (opts) => {
        if (opts && opts.defaultSize) {
            $scope.widget.defaultSize = opts.defaultSize;
        }
    };

    $scope.buildConfigs = (form, force) => {
        if (!form || form.$valid) {
            $scope.widget.widgetTemplate.chartConfig = replaceFontSize($scope.widget.widgetTemplate.chartConfig);
            $scope.executeWidget($scope.widget, null, force);

            $scope.echartConfig.previousTemplate = $scope.echartConfig.currentTemplate ? $scope.echartConfig.currentTemplate : undefined;
            $scope.echartConfig.currentTemplate = $scope.widget.widgetTemplate;

            if ($scope.echartConfig.clear && $scope.echartConfig.previousTemplate
                && $scope.echartConfig.previousTemplate.type !== $scope.echartConfig.currentTemplate.type) {
                $scope.echartConfig.clear();
            }

            if ($scope.echartConfig.isDisposed && !$scope.echartConfig.isDisposed()) {
                $scope.echartConfig.dispose();
            }
        }
    };

    $scope.getWidgetDataset = function getWidgetDataset(widget) {
        if (widget.data && Array.isArray(widget.data.dataset)) {
            return widget.data.dataset;
        }

        return [];
    };

    $scope.switchInputEnabled = (paramValue) => {
        if (!widget.id) {
            if (paramValue.input_enabled) {
                paramValue.value = getFirstValue(paramValue);
            } else {
                paramValue.value = getEmptyValue(paramValue);
            }
        } else if (paramValue.input_enabled) {
            paramValue.value = paramValue.oldValue ? paramValue.oldValue : getFirstValue(paramValue);
        } else {
            paramValue.oldValue = paramValue.oldValue || angular.copy(paramValue.value);
            paramValue.value = getEmptyValue(paramValue);
        }
    };

    function getFirstValue(paramValue) {
        return paramValue.values
            ? paramValue.values.length
                ? paramValue.multiple
                    ? [paramValue.values[0]]
                    : paramValue.values[0]
                : []
            : '';
    }

    function getEmptyValue(paramValue) {
        return paramValue.multiple ? [] : '';
    }

    $scope.executeWidget = (widget, isTable, force) => {
        isTable = isTable || widget.widgetTemplate ? widget.widgetTemplate.type === 'TABLE' : false;

        if (!$scope.widgetBuilder.paramsConfigObject) {
            $scope.widgetBuilder.paramsConfigObject = build($scope.widget, dashboard, currentUserId);
        }

        if (force && Object.keys($scope.selectedBuilderValues).length) {
            $scope.widgetBuilder.paramsConfigObject = applySavedWidgetBuilderSelectedParams();
        }

        const sqlTemplateAdapter = {
            templateId: widget.widgetTemplate.id,
            dashboardId: dashboard.id,
            paramsConfig: Object.keys($scope.widgetBuilder.paramsConfigObject).reduce((acc, item) => ({ ...acc, [item]: $scope.widgetBuilder.paramsConfigObject[item].value }), {}),
        };

        DashboardsService.ExecuteWidgetTemplateSQL(sqlTemplateAdapter)
            .then(({ success, data, message }) => {
                if (success) {
                    let columns = [];
                    let defaultSize = {};

                    if (!widget.widgetTemplate.chartConfig) {
                        columns = Object.keys(data[0] || {});
                    } else if (widget.widgetTemplate.type === 'TABLE') {
                        const chartConfig = JSON.parse(widget.widgetTemplate.chartConfig);

                        columns = chartConfig.columns;
                        defaultSize = chartConfig.defaultSize;
                    }
                    widget.widgetTemplate.model = isTable ? { columns, defaultSize } : widget.widgetTemplate.chartConfig;
                    widget.data = { dataset: data };
                    $scope.refactorWidgetParamsValues(widget);
                } else if (message) {
                    messageService.error(message);
                }
            });
    };

    // Save widget config:

    function saveWidgetBuilderSelectedParams() {
        if (!$scope.widgetBuilder.paramsConfigObject) { return; }

        $scope.selectedBuilderValues = {};

        Object.keys($scope.widgetBuilder.paramsConfigObject)
            .forEach((item) => {
                const {
                    value,
                    input_enabled,
                    type,
                    multiple,
                } = $scope.widgetBuilder.paramsConfigObject[item];

                $scope.selectedBuilderValues = {
                    ...$scope.selectedBuilderValues,
                    [item]: {
                        value,
                        input_enabled,
                        type,
                        multiple,
                    },
                };
            });
    }

    function applySavedWidgetBuilderSelectedParams() {
        if (!$scope.selectedBuilderValues) { return; }

        const newParamsConfigObject = build($scope.widget, dashboard, currentUserId);

        Object.keys($scope.selectedBuilderValues)
            .forEach((item) => {
                const { type, multiple } = $scope.selectedBuilderValues[item];
                let { value, input_enabled } = $scope.selectedBuilderValues[item];

                switch (type) {
                    case 'array':
                        if (multiple) {
                            value = value.filter((val) => isSelectedValueAvaliable(val, newParamsConfigObject[item].values));
                        }
                        break;
                    default:
                        break;
                }

                newParamsConfigObject[item].value = value;
                newParamsConfigObject[item].input_enabled = input_enabled;
            });

        return newParamsConfigObject;
    }

    function isSelectedValueAvaliable(value, values) {
        return values.includes(value);
    }

    // end of save config

    function build(widget, dashboard, currentUserId) {
        const paramsConfigObject = $widget.build(widget, dashboard, currentUserId);
        enableInputs(paramsConfigObject);
        return paramsConfigObject;
    }

    function enableInputs(paramsConfigObject) {
        angular.forEach(paramsConfigObject, (paramValue) => {
            paramValue.input_enabled = paramValue.multiple
                ? !!paramValue.value && !!paramValue.value.length
                : ['title'].indexOf(paramValue.type) !== -1
                    ? false
                    : !!paramValue.value;
        });
    }

    $scope.hasEmptyOptionalParams = function (revert) {
        let result = false;
        angular.forEach($scope.widgetBuilder.paramsConfigObject, (value, key) => {
            const predicate = revert ? value.input_enabled === true : value.input_enabled === false;
            if (!value.required && predicate) {
                result = true;
                return;
            }
        });
        return result;
    };

    function replaceFontSize(chartConfStr) {
        if (!chartConfStr) {
            return;
        }
        return chartConfStr.replace(/.{1}fontSize.{1} *: *(\d+)/, '"fontSize": 6');
    }

    $scope.echartConfig = {
        previousTemplate: undefined,
        currentTemplate: undefined,
    };

    $scope.asString = (value) => {
        if (value) {
            value = value.toString();
        }
        return value;
    };

    $scope.onLegendChange = (legendName) => {
        if (['TABLE'].indexOf($scope.widget.widgetTemplate.type) === -1) {
            $scope.chartActions.push({
                type: $scope.widgetBuilder.legendConfigObject.legendItems[legendName] ? 'legendSelect' : 'legendUnSelect',
                name: legendName,
            });
        }
    };

    $scope.createWidget = () => {
        const widgetData = prepareWidget();

        DashboardsService.CreateWidget(widgetData)
            .then((rs) => {
                if (rs.success) {
                    messageService.success('Widget was created');
                    $scope.hide('CREATE', rs.data);
                } else {
                    messageService.error(rs.message);
                }
            });
    };

    $scope.updateWidget = () => {
        const widgetData = prepareWidget();

        Reflect.deleteProperty($scope.widget, 'model');
        DashboardsService.UpdateWidget(widgetData)
            .then(({ success, data, message }) => {
                if (success) {
                    if (MODE === 'ADD') {
                        messageService.success('Widget was added');
                        $scope.hide('ADD', data);
                    } else {
                        messageService.success('Widget was updated');
                        $scope.hide('UPDATE', data);
                    }
                } else {
                    messageService.error(message);
                }
            });
    };

    function prepareWidget() {
        $scope.widget.paramsConfig = JSON.stringify(
            Object.keys($scope.widgetBuilder.paramsConfigObject)
                .reduce((acc, item) => ({ ...acc, [item]: $scope.widgetBuilder.paramsConfigObject[item].value }), {}), null, 2,
        );

        if ($scope.widgetBuilder.legendConfigObject && $scope.widgetBuilder.legendConfigObject.legendItems) {
            $scope.widget.legendConfig = JSON.stringify($scope.widgetBuilder.legendConfigObject.legendItems, null, 2);
        }

        const {
            title, description, id: widgetId, paramsConfig, legendConfig, widgetTemplate: { id: templateId },
        } = $scope.widget;
        const { id: dashboardId } = dashboard;

        return {
            widgetId,
            title,
            description,
            paramsConfig,
            legendConfig,
            templateId,
            dashboardId,
        };
    }

    $scope.tempData = {
        widget: {},
    };

    $scope.onWidgetChange = () => {
        if ($scope.tempData.widget.id) {
            $scope.widget = $scope.tempData.widget;
            MODE = $scope.widget.existing ? 'UPDATE' : 'ADD';
            return $scope.onChange();
        }
    };

    $scope.hide = (action, widget) => {
        $mdDialog.hide({ action, widget });
    };

    $scope.cancel = () => {
        $mdDialog.cancel();
    };

    function initController() {
        if ($scope.START_MODE === 'UPDATE') {
            $scope.card = getCardById('set');
            CARDS_QUEUE.STORED_WAY.push($scope.card);
            $scope.widget = angular.copy(widget);
            $scope.onChange().then(() => {
                initCard($scope.card);
            });
        } else {
            $scope.next();
        }
    }

    return vm;
};

export default widgetWizardController;

'use strict';

import { Subject, from as rxFrom } from 'rxjs';
import { switchMap, takeUntil, tap, filter } from 'rxjs/operators';

const IssuesModalController = function IssuesModalController(
    $scope,
    $mdDialog,
    $mdMedia,
    $q,
    TestService,
    test,
    messageService,
    integrationsService,
) {
    'ngInject';

    const chunkSize = 20; // 20 is max for current (get issues summary) API
    const jiraIdSubject$ = new Subject();
    const destroy$ = new Subject();
    const vm = {
        attachedIssue: null,
        activeIssue: null,
        issueValueExists: false,
        issueDescriptionStatus: 'pending',
        test,
        issues: [],
        jiraIntegration: {},
        jiraIdPattern: /^([A-z0-9]+-[0-9]+|\d+)$/,
        //https://confluence.atlassian.com/display/JIRA050/Configuring+Project+Keys
        //https://confluence.atlassian.com/stashkb/integrating-with-custom-jira-issue-key-313460921.html
        //used reversed regular expression to use look-ahead assertion instead of unsupported look-behind
        jiraNativeIdPattern: /^(\d+-[A-Z0-9]+[A-Z](?!-?([A-Z0-9]{1,10})))$/,
        issueDescriptionCompleteStatus: false,

        cancel,
        saveIssue,
        linkIssue,
        unlinkIssue,
        onJiraIdChange,
        $onInit: initController,

        get isMobile() { return $mdMedia('xs'); },
    };

    return vm;

    function initController() {
        jiraIdSubject$
            .pipe(
                takeUntil(destroy$),
                filter(value => !!value),
                tap(() => {
                    vm.issueValueExists = false;
                }),
                tap((jiraId) => findExistingIssue(jiraId)),
                filter((jiraId) => vm.jiraNativeIdPattern.test(jiraId.split('').reverse().join(''))),
                switchMap(getJiraTickets$),
                tap(({ success, data: jiraIssue = {} }) => {
                    vm.issueValueExists = success;
                    vm.activeIssue.description = jiraIssue.summary || '';
                    vm.activeIssue.assignee = jiraIssue.assignee || '';
                    vm.activeIssue.reporter = jiraIssue.reporter || '';
                    vm.activeIssue.status = jiraIssue.status?.toUpperCase();
                }),
            )
            .subscribe();

        getJiraIntegration();
        vm.attachedIssue = getAttachedIssue();
        if (vm.attachedIssue.value) {
            TestService.getJiraTicket(vm.attachedIssue.value)
                .then(handleJiraTicketResponse);
        }
        initEmptyIssue();
        getIssues();
        bindEvents();
    }

    function unlinkIssue() {
        const requestBody = {
            testId: vm.test.id,
            issueReferenceId: vm.attachedIssue.id,
        };

        return TestService.deleteTestIssueReference(requestBody)
            .then(({ success }) => {
                if (success) {
                    vm.attachedIssue = {};
                    initEmptyIssue();
                    sortIssues();
                    markAttachedIssue();
                    messageService.success(`Issue was unlinked successfully`);
                } else {
                    messageService.error(`Unable to unlink issue`);
                }
            });
    }

    function linkIssue(issue) {
        if (vm.isLoading) { return; }

        vm.isLoading = true;
        const requestBody = {
            items: [
                {
                    testId: vm.test.id,
                    ...issue,
                },
            ],
        };

        return TestService.createTestsIssueReferences(requestBody)
            .then(({ success, data: { items: linkedIssuesArray }}) => {
                if (success) {
                    const linkedIssue = linkedIssuesArray[0].issue;

                    vm.test.issueReferences = [linkedIssue];
                    vm.attachedIssue = linkedIssue;
                    vm.activeIssue = linkedIssue;
                    getIssues();
                    messageService.success(`Issue was linked successfully`);
                } else {
                    messageService.error(`Unable to link issue`);
                }
                return success;
            })
            .then((linked) => {
                if (linked) {
                    TestService.getJiraTicket(issue.value)
                        .then(handleJiraTicketResponse);
                }
            })
            .finally(() => vm.isLoading = false);
    }

    function handleJiraTicketResponse({success, data: jiraIssue, message}) {
        if (success) {
            vm.issueValueExists = true;
            vm.activeIssue.description = jiraIssue.summary;
            vm.activeIssue.assignee = jiraIssue.assignee || '';
            vm.activeIssue.reporter = jiraIssue.reporter || '';
            vm.activeIssue.status = jiraIssue.status.toUpperCase();
        } else if (message) {
            messageService.error(message);
        }
    }

    function saveIssue() {
        const action = vm.activeIssue.id ? 'update' : 'save';
        const requestBody = {
            items: [
                {
                    testId: vm.test.id,
                    type: 'JIRA',
                    value: vm.activeIssue.value,
                },
            ],
        };

        vm.activeIssue.testCaseId = vm.activeIssue.testCaseId || vm.test.testCaseId;

        return TestService.createTestsIssueReferences(requestBody)
            .then(({ success, data: {items: savedIssuesArray }}) => {
                if (success) {
                    const issue = savedIssuesArray[0]?.issue;

                    Object.assign(vm.activeIssue, issue);
                    vm.test.issueReferences = [issue];
                    vm.attachedIssue = angular.copy(vm.activeIssue);
                    getIssues();
                    messageService.success(`Issue was ${action}d successfully`);
                } else {
                    messageService.error(`Unable to ${action} issue`);
                }
            });
    }

    function getAttachedIssue() {
        return vm.test.issueReferences?.length ? vm.test.issueReferences[0] : {};
    }

    function initEmptyIssue() {
        vm.activeIssue = {
            testCaseId: vm.test.testCaseId,
        };
    }

    function getIssues() {
        return TestService.getTestCaseIssueReferencesByType(vm.test.id)
            .then((rs) => {
                if (rs.success) {
                    vm.issues = rs.data.items || [];
                    sortIssues();
                    getAllIssuesDescription();

                    if (vm.attachedIssue) {
                        markAttachedIssue();
                        vm.activeIssue = {...vm.attachedIssue};
                    }
                } else {
                    messageService.error(rs.message);
                }
            });
    }

    function getAllIssuesDescription() {
        if (!vm.issues.length) { return; }

        vm.issueDescriptionCompleteStatus = false;
        const issuesQueryArray = getPreparedIssuesKeys();
        const promises = issuesQueryArray.map((issuesString) => getIssuesDescription(issuesString));

        $q.all(promises)
            .then(() => {
                vm.issueDescriptionCompleteStatus = true;
            });
    }

    function getIssuesDescription(issuesString) {
        return TestService.getIssueReferencesByValue(issuesString)
            .then(({ success, data: {items: issuesData}, message }) => {
                if (success) {
                    const issuesSummary = issuesData.reduce((acc, item) => {
                        acc[item.key] = item.data?.summary;

                        return acc;
                    }, {});

                    vm.issues = vm.issues.map((item) => (
                        item.description
                            ? item
                            : {
                                ...item,
                                description: issuesSummary[item.value],
                            }
                    ));
                } else {
                    if (message) {
                        messageService.error(message);
                    }
                }
            })
            .catch(angular.noop);
    }

    function getPreparedIssuesKeys() {
        const issues = vm.issues.map((item) => item.value);
        const uniqIssuesValues = getUniqValuesArray(issues);
        const slicedIssuesArray = sliceArray(uniqIssuesValues, chunkSize);
        const uniqIssuesStringsArray = slicedIssuesArray.map((item) => item.join(','));

        return uniqIssuesStringsArray;
    }

    function getUniqValuesArray(array) {
        return [...new Set(array)];
    }

    function sliceArray(initialArray = [], chunkSize = 1) {
        return initialArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / chunkSize);

            resultArray[chunkIndex]
                ? resultArray[chunkIndex].push(item)
                : resultArray[chunkIndex] = [item];

            return resultArray;
          }, []);
    }

    function getJiraIntegration() {
        return integrationsService.getIntegration('jira')
            .then(({ data = {} }) => vm.jiraIntegration = data);
    }

    function bindEvents() {
        $scope.$on('$destroy', function() {
            destroy$.next();
        });
    }

    function cancel() {
        $mdDialog.cancel(vm.test);
    }

    function sortIssues() {
        vm.issues.sort((a, b) => b.modifiedAt - a.modifiedAt);
    }

    function markAttachedIssue() {
        const attachedIssue = vm.issues.find(({id}) => id === vm.attachedIssue?.id);

        vm.issues.forEach(item => item === attachedIssue ? item.attached = true : Reflect.deleteProperty(item, 'attached'));
    }

    function onJiraIdChange(isValid) {
        if (!isValid) {
            return;
        }

        jiraIdSubject$.next(vm.activeIssue.value);
    }

    function getJiraTickets$(value) {
        return rxFrom(TestService.getJiraTicket(value));
    }

    function findExistingIssue(value) {
        const existingIssue = vm.issues.find((foundIssue) => foundIssue.value === value);

        if (existingIssue) {
            vm.activeIssue = { ...existingIssue };
        }
    }

};

export default IssuesModalController;

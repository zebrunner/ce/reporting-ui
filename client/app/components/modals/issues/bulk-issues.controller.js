'use strict';

import { Subject, from as rxFrom } from 'rxjs';
import { switchMap, takeUntil, tap, filter } from 'rxjs/operators';

const BulkIssuesModalController = function BulkIssuesModalController(
    $scope,
    $mdDialog,
    $mdMedia,
    TestService,
    testRunId,
    tests,
    messageService,
    integrationsService,
) {
    'ngInject';

    const jiraIdSubject$ = new Subject();
    const destroy$ = new Subject();
    const vm = {
        isBulkModal: true,
        activeIssue: null,
        issueJiraIdExists: false,
        testRunId,
        tests,
        jiraIntegration: {},
        jiraIdPattern: /^([A-z0-9]+-[0-9]+|\d+)$/,
        //https://confluence.atlassian.com/display/JIRA050/Configuring+Project+Keys
        //https://confluence.atlassian.com/stashkb/integrating-with-custom-jira-issue-key-313460921.html
        //used reversed regular expression to use look-ahead assertion instead of unsupported look-behind
        jiraNativeIdPattern: /^(\d+-[A-Z0-9]+[A-Z](?!-?([A-Z0-9]{1,10})))$/,

        cancel,
        saveIssue,
        onJiraIdChange,
        $onInit: initController,

        get isMobile() { return $mdMedia('xs'); },
    };

    return vm;

    function initController() {
        jiraIdSubject$
            .pipe(
                takeUntil(destroy$),
                filter(value => !!value),
                tap(() => {
                    vm.issueJiraIdExists = false;
                }),
                filter((jiraId) => vm.jiraNativeIdPattern.test(jiraId.split('').reverse().join(''))),
                switchMap(getJiraTickets$),
                filter(({ success, data }) => success && data),
                tap(({ data: jiraIssue }) => {
                    vm.issueJiraIdExists = true;
                    vm.activeIssue.description = jiraIssue.summary;
                    vm.activeIssue.assignee = jiraIssue.assignee || '';
                    vm.activeIssue.reporter = jiraIssue.reporter || '';
                    vm.activeIssue.status = jiraIssue.status.toUpperCase();
                }),
            )
            .subscribe();

        getJiraIntegration();
        vm.activeIssue = {};
        bindEvents();
    }

    function saveIssue() {
        const requestBody = {
            items: vm.tests.map((test) => ({
                    testId: test.id,
                    type: 'JIRA',
                    value: vm.activeIssue.value,
                })
            ),
        }

        return TestService.createTestsIssueReferences(requestBody)
            .then(({ success }) => {
                if (success) {
                    messageService.success('Issue was linked successfully');
                } else {
                    messageService.error('Failed to link issues');
                }
            });
    }

    function getJiraIntegration() {
        return integrationsService.getIntegration('jira')
            .then(({ data = {} }) => vm.jiraIntegration = data);
    }

    function bindEvents() {
        $scope.$on('$destroy', function() {
            destroy$.next();
        });
    }

    function cancel() {
        $mdDialog.cancel();
    }

    function onJiraIdChange(isValid) {
        if (!isValid) {
            return;
        }

        jiraIdSubject$.next(vm.activeIssue.value);
    }

    function getJiraTickets$(value) {
        return rxFrom(TestService.getJiraTicket(value));
    }
};

export default BulkIssuesModalController;

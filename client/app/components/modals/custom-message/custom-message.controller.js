'use strict';

const CustomMessageController = function CustomMessageController(
    $mdToast,
) {
    'ngInject';

    return {
        actionText: this.actionText,
        cancelText: this.cancelText,
        actionFunc: this.actionFunc,
        toastMessage: this.toastMessage,
        closeToast,
    }

    function closeToast() {
        $mdToast.hide();
    }
}

export default CustomMessageController;
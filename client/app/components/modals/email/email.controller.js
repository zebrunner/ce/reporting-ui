(function () {
    'use strict';

    angular.module('app').controller('EmailController', EmailController);

    function EmailController(
        $mdDialog,
        $mdConstant,
        UserService,
        TestRunService,
        testRuns,
        messageService,
        UtilService,
        $q
        ) {
        'ngInject';

        const keys = [
            $mdConstant.KEY_CODE.ENTER,
            $mdConstant.KEY_CODE.TAB,
            $mdConstant.KEY_CODE.COMMA,
            $mdConstant.KEY_CODE.SPACE,
            $mdConstant.KEY_CODE.SEMICOLON,
        ];
        let stopCriteria = '########';
        let usersSearchCriteria = {};
        const vm = {
            UtilService,
            querySearch,
            sendEmail,
            clearInputOnSelect,
            keys,
            email: {
                recipients: [],
            },
            searchText: '',
            currentUser: null,
            cancel,
            users: [],
        };

        function sendEmail() {
            const resultsCounter = {
                success: 0,
                fail: 0,
                total: testRuns.length,
            };
            const email = {...vm.email};
            let promises;

            email.recipients = email.recipients.toString();
            vm.sendingEmail = true;
            promises = testRuns.map((testRun) => {
                return TestRunService.sendTestRunResultsEmail(testRun.id, email)
                    .then(({ success }) => {
                        if (success) {
                            testRun.selected = false;
                            resultsCounter.success += 1;
                        } else {
                            resultsCounter.fail += 1;
                        }
                    });
            });

            $q.all(promises)
                .then(() => {
                    vm.sendingEmail = false;
                    hide(resultsCounter);
                });
        }

        function querySearch() {
            usersSearchCriteria.query = vm.searchText;
            if (!vm.searchText.includes(stopCriteria)) {
                stopCriteria = '########';

                return UserService.searchUsers(usersSearchCriteria, true)
                    .then((rs) => {
                        if (rs.success) {
                            const results = rs.data?.results ?? [];

                            if (!results.length) {
                                stopCriteria = vm.searchText;
                            }

                            return UtilService.filterUsersForSend(results, vm.users);
                        } else {
                            return '';
                        }
                    });
            }

            return '';
        }

        function clearInputOnSelect() {
            vm.searchText = '';
            vm.currentUser = null;
        }

        function hide(resultsCounter) {
            $mdDialog.hide(resultsCounter);
        }

        function cancel() {
            $mdDialog.cancel();
        }

        return vm;
    }

})();

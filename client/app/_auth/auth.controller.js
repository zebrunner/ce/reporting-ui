'use strict';

const authController = function authController(
    $scope,
    $rootScope,
    $location,
    $state,
    $templateCache,
    $stateParams,
    authService,
    UserService,
    UtilService,
    InvitationService,
    messageService,
) {
    'ngInject';

    $scope.UtilService = UtilService;
    $scope.credentials = {
        valid: true
    };
    $scope.errMessage = '';

    $scope.VALIDATIONS = UtilService.validations;

    $scope.invitation = {};

    $scope.getInvitation = function (token) {
        InvitationService.getInvitation(token).then(function (rs) {
            if(rs.success) {
                $scope.invitation = rs.data;
                $scope.user = {};
                $scope.user.email = $scope.invitation.email;
                $scope.user.source = $scope.invitation.source;
            } else {
                $state.go('signin');
            }
        });
    };

    var token;

    $scope.forgotPasswordType = {};
    $scope.forgotPasswordEmailWasSent = false;
    $scope.emailType = {};

    $scope.forgotPassword = function (forgotPassword) {
        authService.forgotPassword(forgotPassword)
            .then((rs) => {
                if(rs.success) {
                    $scope.forgotPassword = {};
                    $scope.forgotPasswordEmailWasSent = true;
                } else {
                    messageService.error(rs.message);
                }
            });
    };

    $scope.getForgotPasswordInfo = function (token) {
        authService.getForgotPasswordInfo(token).then(function (rs) {
            if(rs.success) {
                $scope.forgotPasswordType.email = rs.data.email;
            }
        });
    };

    $scope.resetPassword = function (credentials) {
        credentials.userId = 0;
        authService.resetPassword(credentials, token).then(function (rs) {
            if(rs.success) {
                messageService.success('Your password was changed successfully');
                $state.go('signin');
            } else {
                messageService.error(rs.message);
            }
        });
    };

    $scope.goToState = function (state) {
        $state.go(state);
    };

    (function initController() {
        switch($state.current.name) {
            case 'signup':
                token = $location.search()['token'];
                $scope.getInvitation(token);
                break;
            case 'forgotPassword':
                break;
            case 'resetPassword':
                token = $location.search()['token'];
                if(! token) {
                    $state.go('signin');
                    return;
                }
                $scope.getForgotPasswordInfo(token);
            default:
                break;
        }
        if ($stateParams.user) {
            $scope.credentials.usernameOrEmail = $stateParams.user.email;
            $scope.credentials.password = $stateParams.user.password;
        }
    })();

    $scope.signin = function(credentials) {
        authService.login(credentials.usernameOrEmail, credentials.password)
            .then(function(rs) {
                if (rs.success) {
                    $scope.errMessage = '';
                    var payload = {
                        auth: rs.data
                    };

                    if ($state.params.location) {
                        payload.location = $state.params.location;
                    }

                    $rootScope.$broadcast('event:auth-loginSuccess', payload);
                } else {
                    messageService.error(rs.message);
                }
            });
    };

    $scope.signup = function(user, form) {
        $scope.credentials = {
            usernameOrEmail: user.username,
            password: user.password,
        };

        authService.signUp(user, token).then(function(rs) {
                if (rs.success) {
                    $scope.signin($scope.credentials);
                } else {
                    messageService.error(rs.error.data?.message || rs.message);
                }
            });
    };

    $scope.onChange = function(input) {
        input.$setValidity('validationError', true);
    };
};

export default authController;

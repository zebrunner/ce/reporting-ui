(function () {
    'use strict';

    angular.module('app.copyright').directive('copyright', function () {
        return {
            template: require('./copyright.html'),
            controller: function copyrightController($rootScope, moment) {
                'ngInject';

                return {
                    get version() { return $rootScope.version; },
                    get year() { return moment().format('YYYY'); },
                };
            },
            controllerAs: '$ctrl',
            restrict: 'E',
            replace: true,
        };
    });
})();

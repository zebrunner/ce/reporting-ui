export const authServiceProvider = function authServiceProvider() {
    let authData = null;

    return {
        cleanServiceData,
        initService() {
            if (localStorage.getItem('zeb-auth')) {
                authData = JSON.parse(localStorage.getItem('zeb-auth'));
            }
        },
        $get(
            $httpMock,
            $state,
            UtilService,
            UserService,
        ) {
            'ngInject';

            const service = {
                login,
                forgotPassword,
                getForgotPasswordInfo,
                resetPassword,
                signUp,
                setCredentials,
                renewToken,
                generateAccessToken,
                userHasAnyPermission,
                hasValidToken,

                get authData() { return authData; },
                get isLoggedIn() { return !!(this.authData && UserService.currentUser); },
                get tenant() { return this.authData?.tenantName; },
                get refreshToken() { return authData?.refreshToken; },
            };

            return service;

            function login(username, password) {
                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/auth/login`, { username, password })
                    .then(handleLoginSuccess)
                    .catch((err) => {
                        let errMessage = 'Invalid credentials or user has been deactivated';

                        if (err.status === -1 && !navigator.onLine) {
                            errMessage = 'Something went wrong. Check your internet connection or try again later.';
                        }

                        return UtilService.handleError(errMessage)(err);
                    });
            }

            function forgotPassword(forgotPassword) {
                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/users/password-resets`, forgotPassword)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to restore password'));
            }

            function getForgotPasswordInfo(token) {
                return $httpMock.get(`${$httpMock.apiHost}/api/iam/v1/users/password-resets?token=${token}`)
                    // TODO: move redirection action from service
                    .then(UtilService.handleSuccess, () => {
                        $state.go('signin');
                    });
            }

            function resetPassword(credentials, token) {
                const data = { resetToken: token, newPassword: credentials.password };

                return $httpMock.delete(`${$httpMock.apiHost}/api/iam/v1/users/password-resets`, {data: data, headers: {'Content-Type': 'application/json'}})
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to restore password'));
            }

            function signUp(user, token) {
                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/users?invitation-token=${token}`, user)
                    .then(UtilService.handleSuccess)
                    .catch((err) => {
                        let errMessage = 'Failed to sign up';

                        if (err.status === -1 && !navigator.onLine) {
                            errMessage = 'Something went wrong. Check your internet connection or try again later.';
                        }

                        return UtilService.handleError(errMessage)(err);
                    });
            }

            function renewToken(token) {
                const params = { refreshToken: token };
                const settings = { skipAuthorization: true };

                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/auth/refresh`, params, settings)
                    .then(UtilService.handleSuccess, UtilService.handleError('Invalid refresh token'));
            }

            function generateAccessToken() {
                return $httpMock.get(`${$httpMock.apiHost}/api/iam/v1/auth/access`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to generate token'));
            }

            function setCredentials(auth) {
                localStorage.setItem('zeb-auth', JSON.stringify(auth));
                authData = auth;
            }

            function hasValidToken() {
                return authData?.authToken;
            }

            /**
             * returns if current user has any of provided permissions
             * @param {String[]} permissions - array of permission names
             * @returns {boolean}
             */
            function userHasAnyPermission(permissions) {
                if (!service.isLoggedIn) { return false; }

                return (UserService.currentUser.permissions || []).some((name) => permissions.includes(name));
            }

            function handleLoginSuccess(res) {
                const headers = res.headers();

                return { success: true, data: res.data, firstLogin: headers['x-zbr-first-login'] };
            }
        },
    };

    function cleanServiceData() {
        localStorage.removeItem('zeb-auth');
        authData = null;
    }
};

(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('DownloadService', ['$httpMock', '$rootScope', 'UtilService', DownloadService])

    function DownloadService($httpMock, $rootScope, UtilService) {
        var service = {};

        service.plainDownload = plainDownload;

        return service;

        function plainDownload(url) {
            return $httpMock.get(url, { responseType: 'blob' })
                .then(function(res) {return {success: true, res: res}}, UtilService.handleError('Unable to download file'));
        }
    }
})();

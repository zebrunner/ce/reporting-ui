export const integrationsService = function integrationsService(
    $httpMock,
    UtilService,
) {
    'ngInject';

    return {
        createIntegration,
        deleteIntegration,
        getAutomationServers,
        getIntegration,
        updateIntegration,
        testIntegration,
        renewZebrunnerEngineToken,
    };

    function getIntegration(integrationPath, params = {}) {
        const config = {
            params,
        };

        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}`, config)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to fetch integration data'));
    }

    function createIntegration(integrationPath, integration) {
        return $httpMock.put(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}`, integration)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to create integration'));
    }

    function updateIntegration(integrationPath, patchData, integrationId) {
        if (integrationId) {
            return $httpMock.patch(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}/${integrationId}`, patchData)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to update integration'));
        }

        return $httpMock.patch(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}`, patchData)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to update integration'));
    }

    function deleteIntegration(integrationPath) {
        return $httpMock.delete(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}`)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete integration'));
    }

    function testIntegration(integrationPath, integration, config = {}) {
        return $httpMock.post(`${$httpMock.apiHost}/api/reporting/v1/integrations/${integrationPath}/connectivity-checks`, integration, config)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to check integration connection'));
    }

    function renewZebrunnerEngineToken() {
        return $httpMock.post(`${$httpMock.apiHost}/api/eqs/zebrunner-engine-token/regenerate`)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to regenerate token'));
    }

    function getAutomationServers(onlyActive = false) {
        const params = {
            onlyActive,
            decryptValues: true,
        };

        return getIntegration('jenkins', 1, params);
    }
};

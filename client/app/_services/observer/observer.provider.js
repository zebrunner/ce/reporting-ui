export const observerServiceProvider = function observerServiceProvider() {
    let _subscribers = {};

    return {
        on,
        $get() {
            return {
                on,
                emit,
            };
        },
    };

    /**
     * Subscribes listeners
     * @param {String} name - type of event
     * @param {Function} fn - callback which will be executed on appropriate event emission
     * @returns {function(): *} - unsubscribe function
     */
    function on(name, fn) {
        _subscribers[name] = _subscribers[name] || [];
        _subscribers[name].push(fn);

        return () => _subscribers[name] = _subscribers[name].filter(_fn => _fn !== fn);
    }

    /**
     * Emits event
     * @param {String} name - type of event
     * @param {*} [data] - emitted data
     */
    function emit(name, data) {
        if (Array.isArray(_subscribers[name])) {
            _subscribers[name].forEach(fn => typeof fn === 'function' && fn(data));
        }
    }
}

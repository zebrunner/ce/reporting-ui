'use strict';

import RFB from '@novnc/novnc';

const JSZip = require('jszip');
const ArtifactService = function ArtifactService($window, $q, UtilService, messageService, DownloadService) {
    'ngInject';

    const fileExtensionPattern = /\.([0-9a-z]+)(?:[?#]|$)/i;
    const fileTypes = ['pdf', 'apk', 'exe', 'doc', 'xls', 'txt', 'png'];
    return {
        connectVnc,
        downloadArtifacts,
        extractImageArtifacts,
        getArtifactIconId,
    };

    function connectVnc(containerElement, wsURL) {
        const playerElem = containerElement.querySelector('.vnc-player');

        if (playerElem) {
            const params = {
                shared: true,
                credentials: {
                    password: 'selenoid',
                },
            };
            const rfb = new RFB(playerElem, wsURL, params);

            rfb.scaleViewport = true;
            rfb.resizeSession = true;
            rfb._display._scale = 1;

            return rfb;
        }
    }

    function extractImageArtifacts(tests) {
        tests.forEach(test => {
            const imageArtifacts = test.artifacts.reduce((collected, artifact) => {
                const name = artifact.name.toLowerCase();

                if (!name.includes('live') && !name.includes('video') && artifact.value) {
                    const links = artifact.value.split(' ');
                    let link = links[0];
                    const extensionMatch = link.match(fileExtensionPattern);

                    if (extensionMatch) {
                        artifact.extension = extensionMatch[1];
                        if (artifact.extension === 'png') {
                            artifact.value = link;
                            artifact.thumb = links[1] ? links[1] : link;
                            collected.push(artifact);
                        }
                    }
                }

                return collected;
            }, []);

            if (imageArtifacts) {
                test.imageArtifacts = imageArtifacts;
            }
        });
    }

    function downloadArtifacts({ data = [], field = 'artifacts', name: archiveName = 'artifacts' }) {
        if (!data.length) { return; }
        const zip = new JSZip();

        // if there is only one test, name of the archive should be the same as folder name
        if (data.length === 1 && archiveName === 'artifacts') {
            const test = data[0];

            archiveName = normilizeName(`${test.id}. ${test.name}`.replace(/[^a-z0-9]/gi, '_'));
        }

        data
            .filter(test => (test[field] && test[field].length))
            .forEach(test => {
                const name = normilizeName(`${test.id}. ${test.name}`.replace(/[^a-z0-9]/gi, '_'));
                const folder = zip.folder(name);

                test[field].forEach(artifact => {
                    const fileName = getUrlFilename(artifact.value);
                    const formattedFileName = `${artifact.name}_${fileName}.${artifact.extension}`.getValidFilename();
                    const options = { base64: true };
                    const contentPromise = DownloadService.plainDownload(artifact.value)
                        .then(response => {
                            if (response.success) {
                                return response.res.data;
                            }

                            //broken artifact will be an empty file
                            return '';
                        });

                    folder.file(formattedFileName, contentPromise, options);
                });
            });

        zip
            .generateAsync({ type: "blob" })
            .then(function (content) {
                content.download(archiveName + '.zip');
            })
            .catch(err => {
                console.error(err);
            });
    }

    function normilizeName(str) {
        const maxLength = 256;

        if (str.length > maxLength) {
            return str.slice(0, maxLength - 3) + '...';
        }

        return str;
    }

    function getUrlFilename(url) {
        const urlSlices = url.split(/\#|\?/)[0].split('/');
        return urlSlices[urlSlices.length - 1].split('.')[0].trim();
    }

    function scrollLogsOnBottom(logsContainer) {
        logsContainer.scrollTop = logsContainer.scrollHeight;
    }

    function getArtifactIconId(extension) {
        extension = extension.slice(0, 3);
        const type = fileTypes.includes(extension) ? extension : 'bin';
        return `artifacts:${type}`;
    }
};

export default ArtifactService;


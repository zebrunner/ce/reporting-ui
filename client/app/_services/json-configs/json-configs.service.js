'use strict';

const jsonConfigsService = function jsonConfigsService(
    $httpMock,
) {
    'ngInject';

    return {
        fetchConfig,
        fetchFile,
    };

    function fetchFile(url) {
        return $httpMock.get(`${url}?timestamp=${Date.now()}`, { skipAuthorization: true });
    }

    function fetchConfig(filePath, directory) {
        filePath = handleRelativePaths(filePath, directory);

        return fetchFile(filePath);
    }

    function handleRelativePaths(filePath, directory) {
        if (filePath.startsWith('http')) {
            return filePath;
        }
        if (filePath[0] !== '/') {
            filePath = `/${filePath}`;
        }

        return `${$httpMock.apiHost}/assets/${directory}${filePath}`;
    }
};

export default jsonConfigsService;

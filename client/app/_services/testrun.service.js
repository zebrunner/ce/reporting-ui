(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('TestRunService', ['$httpMock', 'UtilService', TestRunService]);

    function TestRunService($httpMock, UtilService) {
        return {
            searchTestRuns,
            abortTestRun,
            abortCIJob,
            getTestRun,
            deleteTestRun,
            sendTestRunResultsEmail,
            exportTestRunResultsHTML,
            markTestRunAsReviewed,
            rerunTestRun,
            buildTestRun,
            getEnvironments,
            getJobParameters,
            getLocales,
            getPlatforms,
            getBrowsers,
            getShortName,
            getTestRailTestCase,
        };

        function searchTestRuns(params) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/search`, { params })
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to search test runs'));
        }

        function abortTestRun(id, ciRunId, comment) {
            const config = { params: { id, ciRunId } };

            return $httpMock.post(
                `${$httpMock.apiHost}/api/reporting/api/tests/runs/abort`, comment, config)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to abort test run'));
        }

        function getTestRun(id) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get test run by id'));
        }

        function deleteTestRun(id) {
            return $httpMock.delete(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete test run'));
        }

        function sendTestRunResultsEmail(id, email, filter, showStacktrace) {
            return $httpMock.post(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/email`, email, {params:{'filter': filter, 'showStacktrace': showStacktrace}})
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to send test run results email'));
        }

        function exportTestRunResultsHTML(id) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/export`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get test run results HTML'));
        }

        function markTestRunAsReviewed(id, comment) {
            return $httpMock.post(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/markReviewed`, comment)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to mark test run as reviewed'));
        }

        function rerunTestRun(id, rerunFailures) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/rerun`, {params:{'rerunFailures': rerunFailures}})
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to rerun test run'));
        }

        function buildTestRun(id, jobParameters) {
            return $httpMock.post(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/build`, jobParameters)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to build test run'));
        }

        function abortCIJob(id, ciRunId) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/abort/ci`, {params:{'id': id, 'ciRunId': ciRunId}})
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to abort CI Job'));
        }

        function getJobParameters(id) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${id}/jobParameters`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get job parameters'));
        }

        function getEnvironments() {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/environments`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get environments'));
        }

        function getPlatforms() {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/platforms`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get platforms list'));
        }

        function getBrowsers() {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/browsers`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get browsers list'));
        }

        function getLocales() {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/api/tests/runs/locales`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get locales list'));
        }

        function getShortName(name, startCharts, artifactsLimitLetters) {
            if (name.length > artifactsLimitLetters) {
                const artifactStartName = name.slice(0, startCharts);
                const artifactEndName = name.slice(name.length - 5);
                return `${artifactStartName}...${artifactEndName}`;
            }

            return name;
        }

        function getTestRailTestCase(testCaseId) {
            return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/integrations/testrail/test-cases/${testCaseId}`)
                .then(UtilService.handleSuccess, UtilService.handleError('Unable to get TestRail test case'));
        }
    }
})();

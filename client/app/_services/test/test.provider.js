export const testServiceProvider = function testServiceProvider() {
    let local = {
        tests: null,
        previousUrl: null,
    };
    // do not reset this on user change
    let expandAllLabels = false;

    return {
        cleanServiceData() {
            local = {
                tests: null,
                previousUrl: null,
            };
        },
        initService() {
            expandAllLabels = localStorage.getItem('zeb-labels');
        },
        $get(
            $httpMock,
            $rootScope,
            UtilService,
        ) {
            'ngInject';

            const service = {
                searchTests,
                updateTest,
                getTestCaseIssueReferencesByType,
                getIssueReferencesByValue,
                createTestsIssueReferences,
                deleteTestIssueReference,
                getJiraTicket,
                subscribeOnLocationChangeStart,
                get tests() {
                    return local.tests;
                },
                set tests(tests) {
                    local.tests = tests;
                },
                get expandAllLabels() {
                    return expandAllLabels;
                },
                set expandAllLabels(value) {
                    value ? localStorage.setItem('zeb-labels', value) : localStorage.removeItem('zeb-labels');
                    return expandAllLabels = value;
                },
                getTest,
                clearDataCache,
                locationChange: null,
                clearPreviousUrl,
                getPreviousUrl,
                unsubscribeFromLocationChangeStart,
                updateTestsStatus,
            };

            return service;

            function subscribeOnLocationChangeStart() {
                service.locationChange = $rootScope.$on("$locationChangeStart", function (event, newUrl, oldUrl) {
                    local.previousUrl = oldUrl;
                });
            }

            function getPreviousUrl() {
                return local.previousUrl;
            }

            function clearPreviousUrl() {
                local.previousUrl = null;
            }

            function unsubscribeFromLocationChangeStart() {
                if (service.locationChange) {
                    service.locationChange();
                }
            }

            function getTest(id) {
                return local.tests.find((test) => test.id === id);
            }

            function clearDataCache() {
                local.tests = null;
            }

            function searchTests(criteria) {
                return $httpMock.post(`${$httpMock.apiHost}/api/reporting/api/tests/search`, criteria)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to search tests'));
            }

            function updateTest(test) {
                return $httpMock.put(`${$httpMock.apiHost}/api/reporting/api/tests`, test)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update test'));
            }

            function getTestCaseIssueReferencesByType(testId) {
                const config = {
                    params: {
                        testId,
                    },
                };

                return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/issue-references`, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to get test issue references by type'));
            }

            function getIssueReferencesByValue(values) {
                const config = {
                    params: {
                        ids: values,
                    },
                };

                return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/integrations/jira/issues`, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to get test issue references by type'));
            }

            function createTestsIssueReferences(requestBody) {
                return $httpMock.post(`${$httpMock.apiHost}/api/reporting/v1/issue-references`, requestBody)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to create test issue reference'));
            }

            function deleteTestIssueReference(requestBody) {
                const config = {
                    data: requestBody,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                return $httpMock.delete(`${$httpMock.apiHost}/api/reporting/v1/issue-references`, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete test issue reference'));
            }

            function getJiraTicket(issueId) {
                return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/integrations/jira/issues/${issueId}`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to get issue from Jira'));
            }

            /**
             * Bulk updates statuses
             * @param {Number} testRunId - ID of the test run which holds passed tests
             * @param {Object} params - action params (array of tests IDs, action type and status value)
             * @returns {PromiseLike<any> | Promise<any>}
             */
            function updateTestsStatus(testRunId, params) {
                return $httpMock.patch(`${$httpMock.apiHost}/api/reporting/api/tests/runs/${testRunId}`, params)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update statuses'));
            }
        },
    };
};

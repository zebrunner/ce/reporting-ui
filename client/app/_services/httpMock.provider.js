export function httpMockProvider() {
    const local = {
        apiHost: '',
    };

    return {
        initApiHost(apiHost = '') {
            const lastIndex = apiHost.length - 1;

            //remove ending slash
            if (apiHost[lastIndex] === '/') {
                apiHost = apiHost.slice(0, lastIndex);
            }

            local.apiHost = apiHost;
        },
        $get(
            $http,
        ) {
            'ngInject';

            return {
                post: request('post'),
                get: request('get'),
                delete: request('delete'),
                put: request('put'),
                patch: request('patch'),

                get apiHost() { return local.apiHost; },
            };

            function request(method) {
                if (!$http[method]) {
                    method = 'get';
                }

                return (url, ...params) => {
                    params.unshift(url);

                    return $http[method]
                        .apply($http, params);
                }
            }
        }
    };
}

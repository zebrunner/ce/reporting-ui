import controller from './../../components/modals/custom-message/custom-message.controller';
import template from './../../components/modals/custom-message/custom-message.html';

'use strict';

const messageService = function messageService($mdToast) {
    'ngInject';

    const mainCSSClass = 'message-toast';
    const defOptions = {
        position: 'bottom right',
        hideDelay: 5000,
    };

    return {
        message,
        success,
        error,
        warning,
        openCustomMessage,
    };

    function message(text, options) {
        const defOptions = {
            cancelText: 'CANCEL',
            hideDelay: 0,
        };
        checkMessage(text, {...defOptions, ...options});
    }

    function success(text, options) {
        checkMessage(text, options, '_success');
    }

    function error(text, options) {
        const defOptions = {
            cancelText: 'CANCEL',
            hideDelay: 0,
        };
        checkMessage(text, {...defOptions, ...options}, '_error');
    }

    function warning(text, options) {
        checkMessage(text, options, '_warning');
    }

    function openCustomMessage(text, options, cssClass) {
        options = {
            ...options,
            controller,
            template,
            controllerAs: '$ctrl',
            bindToController: true,
            toastClass: [mainCSSClass, cssClass].join(' '),
            locals: {
                toastMessage: text,
                actionText: options.action,
                cancelText: options.cancelText,
                actionFunc: options.actionFunc,
            },
        };
        $mdToast.show(options);
    }

    function checkMessage(text, options, cssClass) {
        if ((options?.actionFunc && options?.action) || options?.cancelText) {
            openCustomMessage(text, options, cssClass);
        } else {
            showToast(text, options, cssClass);
        }
    }


    function showToast(text, options = {}, localCSSClass = '') {
        const toast = $mdToast.simple().content(text);

        toast._options = {...toast._options, ...defOptions, ...options};
        toast._options.toastClass = [toast._options.toastClass, mainCSSClass, localCSSClass].join(' ');

        $mdToast.show(toast);
    }
};

export default messageService;

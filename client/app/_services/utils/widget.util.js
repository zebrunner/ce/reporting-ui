(function () {
    angular
        .module('app.services')
        .factory('$widget', ['$location', WidgetUtilService]);

    function WidgetUtilService($location) {
        return { build, buildLegend };

        function build(widget, dashboard, userId) {
            const config = widget.widgetTemplate.paramsConfig;
            const envParams = getENVParams(dashboard, userId);
            angular.forEach(config, (paramValue, paramName) => {
                const type = paramValue.type
                    ? paramValue.type
                    : paramValue.value !== undefined
                        ? getType(paramValue.value)
                        : paramValue.values && angular.isArray(paramValue.values)
                            ? 'array'
                            : undefined;
                const isExistingWidget = widget.id && widget.paramsConfig && widget.paramsConfig.length;
                const value = getValue(widget.paramsConfig, paramName, paramValue, type, isExistingWidget, envParams);
                if (type) {
                    setParameter(config, paramName, value);
                    paramValue.type = type;
                }
            });
            angular.forEach(envParams, (value, key) => {
                if (!config[key]) {
                    setParameter(config, key, value);
                    config[key].hidden = true;
                }
            });
            return config;
        }

        function getValue(paramsConfig, paramName, paramValue, type, isExistingWidget, envParams) {
            let value;
            const required = !!paramValue.required;
            const overrideWithEnvParams = !!envParams && !!envParams[paramName];

            if (isExistingWidget) {
                const conf = JSON.parse(paramsConfig);

                if (!Object.keys(conf).includes(paramName)) {
                    value = undefined;
                } else {
                    value = conf[paramName]
                        ? conf[paramName]
                        : ['array', 'radio'].indexOf(type) !== -1
                            ? paramValue.multiple
                                ? paramValue.values
                                : required
                                    ? paramValue.values[0]
                                    : undefined
                            : paramValue.value;
                }
            } else {
                value = ['array', 'radio'].indexOf(type) !== -1
                    ? paramValue.multiple
                        ? paramValue.values && paramValue.values.length
                            ? required
                                ? [paramValue.values[0]]
                                : undefined
                            : paramValue.values
                        : required
                            ? paramValue.values[0]
                            : undefined
                    : paramValue.value;
            }
            value = overrideWithEnvParams
                ? (['array', 'radio'].indexOf(type) !== -1 && envParams && paramValue.values.indexOf(envParams[paramName]) !== -1) || ['string'].indexOfField(type) !== -1
                    ? getValueByType(envParams[paramName], getType(value))
                    : value
                : value;
            value = paramValue.multiple && getType(value) !== 'array'
                ? value
                    ? [value]
                    : []
                : value;
            value = required ? value : value || '';
            return value;
        }

        function buildLegend(widget) {
            const legendConfig = {};
            if (widget.widgetTemplate.legendConfig) {
                const parsedWidgetTemplateLegendConfig = JSON.parse(widget.widgetTemplate.legendConfig);
                legendConfig.legend = parsedWidgetTemplateLegendConfig.legend;
                legendConfig.legendItems = {};
                const parsedConfig = widget.legendConfig ? JSON.parse(widget.legendConfig) : null;
                angular.forEach(legendConfig.legend, (legendName) => {
                    const useExistingWidgetLegendConfig = !!(widget.id && parsedConfig[legendName] !== undefined && widget.legendConfig);
                    legendConfig.legendItems[legendName] = useExistingWidgetLegendConfig && parsedConfig ? parsedConfig[legendName] : true;
                });
            }
            return legendConfig;
        }

        function setParameter(config, key, value) {
            config[key] = config[key] || {};
            config[key].value = value;
        }

        // Get query params merged by hierarchy: dashboardName -> currentUserId -> queryParams
        function getENVParams(dashboard, currentUserId) {
            const params = {};

            if (dashboard) {
                params.dashboardName = dashboard.title; // override with origin dashboard name
            }

            if (currentUserId) {
                params.currentUserId = currentUserId; // override with origin current user id
            }

            angular.forEach($location.search(), (value, key) => {
                params[key] = value; // override with query params
            });

            return params;
        }

        function getValueByType(stringValue, type) {
            let result;
            switch (type) {
                case 'boolean':
                    result = stringValue === 'true';
                    break;
                case 'int':
                    result = parseInt(stringValue, 10);
                    break;
                default:
                    result = stringValue;
                    break;
            }
            return result;
        }

        // array, boolean, string, int, none
        function getType(value) {
            return angular.isArray(value)
                ? 'array'
                : typeof value === 'boolean'
                    ? 'boolean'
                    : typeof value === 'string' || value instanceof String
                        ? 'string'
                        : Number.isInteger(value)
                            ? 'int'
                            : 'none';
        }
    }
})();

export const notificationService = function notificationService(
    $httpMock,
    $rootScope,
    UtilService,
) {
    'ngInject';

    return {
        sendReviewNotification,
    };

    function sendReviewNotification(testRunId) {
        const config = {
            params: { testRunId },
        };

        return $httpMock.post(`${$httpMock.apiHost}/api/reporting/v1/notifications`, null, config)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to send review notification'));
    }
};

export const userServiceProvider = function userServiceProvider() {
    let _currentUser = null;
    let _getUserProfileRequest = null;

    return {
        cleanServiceData,
        $get(
            $httpMock,
            $q,
            UtilService,
        ) {
            'ngInject';

            const service = {
                searchUsers,
                updateUserProfile,
                updateUserPassword,
                createUser,
                addUserToGroup,
                deleteUserFromGroup,
                initCurrentUser,

                get currentUser() {
                    return _currentUser;
                },
                set currentUser(user) {
                    _currentUser = user;
                },
            };

            return service;

            function getUserProfileById(id) {
                return $httpMock.get(`${$httpMock.apiHost}/api/iam/v1/users/${id}?_embedPreferences=true`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to get user profile'));
            }

            function searchUsers(params, onlyPublic) {
                if (onlyPublic) {
                    params.public = true;
                }

                return $httpMock.get(`${$httpMock.apiHost}/api/iam/v1/users`, { params })
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to search users'));
            }

            /**
             * updated users profile data
             * @param {Number} userId - user's identifier
             * @param {Object} profileData - user's profile data. Required fields are: username, firstName, lastName
             * @returns {Promise<T | {success: boolean, message: string, error: *}>}
             */
            function updateUserProfile(userId, profileData) {
                return $httpMock.patch(`${$httpMock.apiHost}/api/iam/v1/users/${userId}`, profileData)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update user profile'));
            }

            function updateUserPassword(id, password) {
                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/users/${id}/password`, password)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update user password'));
            }

            function createUser(user){
                return $httpMock.post(`${$httpMock.apiHost}/api/iam/v1/users`, user)
                    .then(UtilService.handleSuccess, UtilService.handleError('Failed to create user'));
            }

            function addUserToGroup(user, groupId){
                return $httpMock.put(`${$httpMock.apiHost}/api/iam/v1/users/${user.id}/groups/${groupId}`, user)
                    .then(UtilService.handleSuccess, UtilService.handleError('Failed to add user to group'));
            }

            function deleteUserFromGroup(idUser, idGroup){
                return $httpMock.delete(`${$httpMock.apiHost}/api/iam/v1/users/${idUser}/groups/${idGroup}`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Failed to delete user from group'));
            }

            function initCurrentUser(force, userId) {
                if (service.currentUser && !force) {
                    return $q.resolve(service.currentUser);
                }

                if (_getUserProfileRequest && !force) {
                    return _getUserProfileRequest;
                }

                _getUserProfileRequest = getUserProfileById(userId)
                    .then(({ success, data }) => {
                        if (success) {
                            _getUserProfileRequest = null;
                            service.currentUser = { ...data };
                            setUserPermissions();
                            setDefaultPreferences(service.currentUser.preferences);

                            return service.currentUser;
                        } else {
                            return $q.reject({ success: false, message: `Couldn't get profile data. Try to login once again.` });
                        }
                    });
                return _getUserProfileRequest;
            }

            function setUserPermissions() {
                const authData = JSON.parse(localStorage.getItem('zeb-auth')) || {};
                const { permissionsSuperset = [] } = authData;

                service.currentUser.permissions = [...permissionsSuperset];
            }

            function setDefaultPreferences(userPreferences) {
                if (!Array.isArray(userPreferences)) {
                    return;
                }

                userPreferences.forEach((userPreference) => {
                    switch(userPreference.name) {
                        case 'DEFAULT_DASHBOARD':
                            service.currentUser.defaultDashboard = userPreference.value;
                            break;
                        case 'REFRESH_INTERVAL':
                            service.currentUser.refreshInterval = parseInt(userPreference.value, 10);
                            break;
                        case 'THEME':
                            service.currentUser.theme = userPreference.value;
                            break;
                        case 'DEFAULT_TEST_VIEW':
                            service.currentUser.testsView = userPreference.value;
                            break;
                        default:
                            break;
                    }
                });
            }
        },
    };

    function cleanServiceData() {
        _currentUser = null;
        _getUserProfileRequest = null;
    }
};

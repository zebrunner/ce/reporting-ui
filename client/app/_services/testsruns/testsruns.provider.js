export const testsRunsServiceProvider = function testsRunsServiceProvider() {
    const skippedSearchParams = ['activeTestRunId', 'fromWizard'];

    return {
        cleanServiceData() {
            sessionStorage.removeItem('launchers');
        },
        $get(
            $q,
            DEFAULT_SC,
            TestRunService,
        ) {
            'ngInject';

            return  {
                addNewLauncher,
                deleteLauncherFromCache,
                getCachedLaunchers,
                getTestRuns,
                initTestRunData,
                normalizeTestPlatformData,
                prepareSearchParams,
                prepareQueryParams,
                preparePlatformsData,

                get defaultSearchParams() { return DEFAULT_SC; },
            };

            function normalizeTestPlatformData(data) {
                let browser;
                let browserVersion;
                let platform;
                let platformVersion;

                if (data) {
                    browserVersion = data.browserVersion;
                    platform = data.platform && data.platform.toLowerCase();
                    platformVersion = data.platformVersion;
                    if (data.browser && (data.platform && (data.platform.toLowerCase() === 'ios' || data.platform.toLowerCase() === 'android'))) {
                        browser = `${data.browser.toLowerCase()}-mobile`;
                    } else {
                        browser = data.browser && `${data.browser.toLowerCase()}`;
                    }
                }

                return { browser, browserVersion, platform, platformVersion };
            }

            function preparePlatformsData(testRun) {
                if (testRun.config) {
                    [testRun.platformIcon, testRun.platformVersion] = refactorPlatformData(testRun.config);
                }
            }

            function refactorPlatformData(data) {
                let item;
                let itemVersion;

                if (data) {
                    if (data.browser && (!data.platform || ((data.platform.toLowerCase() !== 'ios' && data.platform.toLowerCase() !== 'android')))) {
                        item = data.browser.toLowerCase();
                        itemVersion = data.browserVersion;
                    } else if (data.browser) {
                        item = `${data.browser.toLowerCase()}-mobile`;
                        itemVersion = data.browserVersion;
                    } else if (data.platform) {
                        item = data.platform.toLowerCase();
                        itemVersion = data.platformVersion;
                    } else {
                        item = 'unknown';
                    }
                }

                return [item, itemVersion];
            }

            function getTestRuns(searchParams) {
                return TestRunService.searchTestRuns(searchParams)
                    .then(({ success, data, message, error }) => {
                        if (success) {
                            return prepareRunsData(data);
                        }

                        return $q.reject({ message, error });
                    });
            }

            function prepareRunsData(data) {
                data.results = data.results || [];
                data.results.forEach(initTestRunData);
                filterCachedLaunchers(data.results);
                checkForDestroyingLaunchersByTime();

                return data;
            }

            function initTestRunData(testRun) {
                addJob(testRun);
                testRun.tests = null;
            }

            function filterCachedLaunchers(testsRuns) {
                const launchers = getCachedLaunchers()
                    .filter((launcher) => !testsRuns.find((testsRun) => testsRun.ciRunId === launcher.ciRunId));

                cacheLaunchers(launchers);

                return launchers;
            }

            function checkForDestroyingLaunchersByTime() {
                const dateNow = new Date();
                const launchers = getCachedLaunchers().filter((launcher) => {
                    return launcher.shouldBeDestroyedAt - dateNow.getTime() > 0;
                });

                cacheLaunchers(launchers);
            }

            function addJob(testRun) {
                if (testRun.job && testRun.job.jobURL) {
                    const buildNumber = testRun.buildNumber ? `/${testRun.buildNumber}` : '';
                    testRun.jenkinsURL = `${testRun.job.jobURL}${buildNumber}`;
                    testRun.UID = `${testRun.testSuite.name} ${testRun.jenkinsURL}`;
                }
            }

            function cacheLaunchers(launchers) {
                if (launchers.length) {
                    sessionStorage.setItem('launchers', JSON.stringify(launchers));
                } else {
                    sessionStorage.removeItem('launchers');
                }
            }

            function getCachedLaunchers() {
                let launchers = [];
                const cachedLaunchers = sessionStorage.getItem('launchers');

                if (cachedLaunchers) {
                    launchers = JSON.parse(cachedLaunchers);
                }

                return launchers;
            }

            function deleteLauncherFromCache(ciRunId) {
                const launchers = getCachedLaunchers()
                    .filter((launcher) => launcher.ciRunId !== ciRunId);

                cacheLaunchers(launchers);

                return launchers;
            }

            function addNewLauncher(newTestRun) {
                const dateNow = new Date();
                let launchers = getCachedLaunchers();

                newTestRun.tests = null;
                newTestRun.startedAt = dateNow.getTime();
                dateNow.setMinutes( dateNow.getMinutes() + 5 );
                newTestRun.shouldBeDestroyedAt = dateNow.getTime();
                launchers = [ ...launchers, newTestRun ];
                cacheLaunchers(launchers);

                return launchers;
            }

            function prepareSearchParams(stateParams) {
                const searchParams = {};

                Object.keys(stateParams)
                    .filter((key) => !skippedSearchParams.includes(key) && stateParams[key])
                    .forEach((key) => searchParams[key] = stateParams[key]);

                return searchParams;
            }

            function prepareQueryParams(searchParams, activeTestRunId) {
                const params = { ... searchParams };

                // at the moment we don't change this param
                Reflect.deleteProperty(params, 'pageSize');
                if (activeTestRunId) {
                    params.activeTestRunId = activeTestRunId;
                }

                return params;
            }
        },
    };
};

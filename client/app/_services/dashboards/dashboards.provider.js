export const DashboardsServiceProvider = function DashboardsServiceProvider(
) {
    'ngInject';

    let dashboards = [];

    return {
        cleanServiceData,
        $get(
            $httpMock,
            $q,
            UtilService,
        ) {
            'ngInject';

            return {
                GetDashboards,
                CreateDashboard,
                UpdateDashboard,
                DeleteDashboard,
                GetDashboardById,
                CheckDuplicateDashboardTitle,
                AddDashboardWidget,
                UpdateDashboardWidgets,
                DeleteDashboardWidget,
                SendDashboardByEmail,
                SwitchDashboard,
                GetWidgets,
                CreateWidget,
                UpdateWidget,
                DeleteWidget,
                ExecuteWidgetSQL,
                GetWidgetTemplates,
                PrepareWidgetTemplate,
                ExecuteWidgetTemplateSQL,
                GetWidgetTemplateParameters,

                get dashboards() {
                    return dashboards;
                },
                set dashboards(data) {
                    dashboards = data;
                },
            };

            function GetDashboards(params) {
                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/dashboards/search`, { params })
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load dashboards'));
            }

            function SwitchDashboard(dashboardId) {
                return $httpMock.put(`${ $httpMock.apiHost }/api/reporting/v1/dashboard-switches`, { dashboardId })
                    .then(UtilService.handleSuccess, UtilService.handleError('Failed to switch dashboard'));
            }

            function CreateDashboard(dashboard) {
                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/dashboards`, dashboard)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to create dashboard'));
            }

            function UpdateDashboard(dashboard) {
                return $httpMock.put(`${ $httpMock.apiHost }/api/reporting/api/dashboards`, dashboard)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update dashboard'));
            }

            function DeleteDashboard(id) {
                return $httpMock.delete(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ id }`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete dashboard'));
            }

            function GetDashboardById(id) {
                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ id }`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load dashboard'));
            }

            function CheckDuplicateDashboardTitle({ title }) {
                const config = {
                    params: {
                        title,
                    },
                };

                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/dashboards`, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load dashboard'));
            }

            function AddDashboardWidget(dashboardId, requestData, requestParams) {
                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ dashboardId }/widgets`, requestData, { params: requestParams })
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to add widget to dashboard'));
            }

            function UpdateDashboardWidgets(dashboardId, widgets) {
                return $httpMock.put(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ dashboardId }/widgets/all`, widgets)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update widgets on dashboard'));
            }

            function DeleteDashboardWidget(dashboardId, widgetId) {
                return $httpMock.delete(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ dashboardId }/widgets/${ widgetId }`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete widget from dashboard'));
            }

            function SendDashboardByEmail(multipart, email, dashboardId) {
                const config = { headers: { 'Content-Type': undefined }, transformRequest: angular.identity };

                multipart.append('email', new Blob([JSON.stringify(email)], { type: 'application/json' }));

                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/dashboards/${ dashboardId }/email?file=`, multipart, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to send dashboard by email'));
            }

            function GetWidgets() {
                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/widgets`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load widgets'));
            }

            function CreateWidget(widget) {
                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/widgets`, widget)
                    .then(UtilService.handleSuccess, (response) => UtilService.handleCreateWidgetError(response));
            }

            function UpdateWidget(widget) {
                return $httpMock.put(`${ $httpMock.apiHost }/api/reporting/api/widgets/${ widget.widgetId }`, widget)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to update widget'));
            }

            function DeleteWidget(id) {
                return $httpMock.delete(`${ $httpMock.apiHost }/api/reporting/api/widgets/${ id }`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete widget'));
            }

            function ExecuteWidgetSQL(sqlAdapter, params) {
                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/widgets/sql${ params }`, sqlAdapter)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load chart'));
            }

            function GetWidgetTemplates() {
                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/widgets/templates`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load widget templates'));
            }

            function PrepareWidgetTemplate(id) {
                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/widgets/templates/${ id }/prepare`)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to prepare widget template'));
            }

            function ExecuteWidgetTemplateSQL(sqlTemplateAdapter) {
                return $httpMock.post(`${ $httpMock.apiHost }/api/reporting/api/widgets/templates/sql`, sqlTemplateAdapter)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to load chart'));
            }

            function GetWidgetTemplateParameters(widgetTemplateId, requestData) {
                const config = {
                    params: {
                        ...requestData,
                    },
                };

                return $httpMock.get(`${ $httpMock.apiHost }/api/reporting/api/widgets/templates/${ widgetTemplateId }/parameters`, config)
                    .then(UtilService.handleSuccess, UtilService.handleError('Unable to get widget template parameters'));
            }
        },
    };

    function cleanServiceData() {
        dashboards = [];
    }
};

const toolsService = function toolsService(
    $httpMock,
    UtilService,
) {
    'ngInject';

    return {
        getIfAnyNotificationToolConnected,
        getIfTestingPlatformConnected,
        getNotificationToolsConnection,
        getTestingPlatformsConnections,
    };

    function getNotificationToolsConnection() {
        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/notification-tools`)
            .then(UtilService.handleSuccess, UtilService.handleError(`Unable to get notification tools connection`));
    }

    function getIfAnyNotificationToolConnected() {
        return getNotificationToolsConnection()
            .then(({ success, data = {} }) => {
                let isAvailableAnyNotificationTool = false;

                if (success) {
                    isAvailableAnyNotificationTool = Object.values(data).some((value) => value);
                }

                return isAvailableAnyNotificationTool;
            });
    }

    function getTestingPlatformsConnections() {
        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/testing-platforms`)
            .then(UtilService.handleSuccess, UtilService.handleError(`Unable to fetch testing platform connections`));
    }

    function getIfTestingPlatformConnected() {
        return getTestingPlatformsConnections()
            .then(({ success, data = {} }) => {
                let isAvailableAnyNotificationTool = false;

                if (success) {
                    isAvailableAnyNotificationTool = Object.values(data).some((value) => value);
                }

                return isAvailableAnyNotificationTool;
            });
    }
};

export default toolsService;

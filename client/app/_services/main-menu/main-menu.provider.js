export const mainMenuServiceProvider = function mainMenuServiceProvider() {
    'ngInject';

    /** TODO: refactor this description
     * @param {string} item.name - name of subitem (something like slug)
     * @param {string} item.title - displayed value
     * @param {string} item.linkType - 'internal' | 'external'. Internal link is a sref, angular link. External link is a link to other page
     * @param {string} item.link - link to the page
     * @param {string} item.className - additional class name for customize a menu
     * @param {string} item.matIcon - additional class name for customize a menu
     * @param {string} item.permissions - permissions, which define link visibility
     * */
    let menuItems = [
        {
            name: 'Dashboards',
            className: 'dashboards',
            linkType: 'internal',
            state: 'dashboard.list',
            icon: 'analyticsIcon',
            customIcon: true,
            order: 1,
        },
        {
            name: 'Test runs',
            className: 'tests',
            linkType: 'internal',
            state: 'tests.runs',
            icon: 'rocketIcon',
            customIcon: true,
            order: 2,
        },
    ];
    let isSidebarExpanded = false;

    return {
        cleanServiceData,
        initService,
        $get() {
            return {
                get items() { return menuItems; },
                get isSidebarExpanded() { return isSidebarExpanded; },
                set isSidebarExpanded(value) {
                    if (value) {
                        localStorage.setItem('zeb-sidebar-expanded', value);
                        isSidebarExpanded = value;
                    } else {
                        cleanServiceData();
                    }
                },
            };
        },
    };

    function initService() {
        isSidebarExpanded = localStorage.getItem('zeb-sidebar-expanded');
    }

    function cleanServiceData() {
        isSidebarExpanded = false;
        localStorage.removeItem('zeb-sidebar-expanded');
    }
};

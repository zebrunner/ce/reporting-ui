'use strict';

const testsSessionsService = function testsSessionsService(
    $httpMock,
    UtilService,
) {
    'ngInject';

    return {
        getSessions,
        formatSessionValues,
    };

    function getSessions(testRunId, testId, options = {}) {
        if (testId) {
            options.params = { testId };
        }

        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/test-runs/${testRunId}/test-sessions`, options)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to get sessions'));
    }

    function formatSessionValues(session) {
        session.formattedValues = {};
        [session.formattedValues.browserIcon, session.formattedValues.browserVersion] = refactorBrowserData(session);

        if (session.deviceName) {
            session.formattedValues.deviceName = session.deviceName.replace(/_/g, ' ');
        }
        if (session.platformName) {
            session.formattedValues.platformName = session.platformName.toLowerCase();
        }
    }

    function refactorBrowserData(session) {
        let item;
        let itemVersion;

        if (session.browserName && (!session.platformName || ((session.platformName.toLowerCase() !== 'ios' && session.platformName.toLowerCase() !== 'android')))) {
            item = session.browserName.toLowerCase();
            itemVersion = session.browserVersion;
        } else if (session.browserName) {
            item = `${session.browserName.toLowerCase()}-mobile`;
            itemVersion = session.browserVersion;
        }

        return [item, itemVersion];
    }
};

export default testsSessionsService;

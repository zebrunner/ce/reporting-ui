import appHealthService from './app-health/app-health.service';
import toolsService from './tools/tools.service';
import progressbarService from './progressbar/progressbar.service';
import messageService from './messages/message.service';
import ArtifactService from './artifact.service';
import jsonConfigsService from './json-configs/json-configs.service';
import testsSessionsService from './tests-sessions/tests-sessions.service';
import pageTitleService from './page-title/page-title.service';
import logLevelService from './log-level.service';
import fullScreenService from './full-screen/full-screen.service';
import { authServiceProvider } from './auth/auth.provider';
import { observerServiceProvider } from './observer/observer.provider';
import { notificationService } from './notification/notification.service';
import { httpMockProvider } from './httpMock.provider';
import { mainMenuServiceProvider } from './main-menu/main-menu.provider';
import { integrationsService } from './integrations/integrations.service';
import { testServiceProvider } from './test/test.provider';
import { testsRunsServiceProvider } from './testsruns/testsruns.provider';
import { userServiceProvider } from './user/user.provider';
import { DashboardsServiceProvider } from './dashboards/dashboards.provider';

angular.module('app.services', [])
    .provider('$httpMock', httpMockProvider)
    .provider('observerService', observerServiceProvider)
    .provider('authService', authServiceProvider)
    .provider('UserService', userServiceProvider)
    .provider('mainMenuService', mainMenuServiceProvider)
    .provider('TestService', testServiceProvider)
    .provider('testsRunsService', testsRunsServiceProvider)
    .provider('DashboardsService', DashboardsServiceProvider)
    .service({ appHealthService })
    .service({ ArtifactService })
    .service({ integrationsService })
    .service({ toolsService })
    .service({ logLevelService })
    .service({ fullScreenService })
    .service({ messageService })
    .service({ notificationService })
    .service({ progressbarService })
    .service({ jsonConfigsService })
    .service({ pageTitleService })
    .service({ testsSessionsService });

require('./auth.intercepter');
require('./utils/safari-fixes.util');
require('./utils/TableExpandUtil');
require('./utils/widget.util');
require('./config.service');
require('./download.service');
require('./filter.service');
require('./group.service');
require('./invitation.service');
require('./modals.service');
require('./permission.service');
require('./testrun.service');
require('./upload.service');
require('./util.service');

const zebPageSubheaderController = function zebPageSubheaderController(
    $timeout,
) {
    'ngInject';

    return {
        searchQuery: '',
        searchPlaceholder: 'Search',
        search() {
            $timeout(this.onSearch, 0);
        },
        reset() {
            $timeout(this.onReset, 0);
        }
    }
};

export default zebPageSubheaderController;

import template from './zeb-page-subheader.html';
import controller from './zeb-page-subheader.controller';
import './zeb-page-subheader.scss';

const zebPageSubheaderComponent = {
    bindings: {
        onReset: '&',
        onSearch: '&',
        searchPlaceholder: '@?',
        searchQuery: '=',
    },
    controller,
    template,
    transclude: true,
};

export default zebPageSubheaderComponent;

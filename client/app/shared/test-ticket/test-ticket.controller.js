'use strict';

export default function TestTicketController() {
    'ngInject';

    return {
        issueReference: null,
        jiraIntegration: {},
    };
}

import template from './test-session-info-loader.html';
import './test-session-info-loader.scss';

const testSessionInfoLoaderComponent = {
    template,
    bindings: {
        logsView: '<',
    },
};

export default testSessionInfoLoaderComponent;

// TODO: it would be great to scroll elapsed item to the top of the viewport. Especially on mobile screens
const testSessionsController = function testSessionInfoController(
    $httpMock,
    $timeout,
    $sce,
    $scope,
    $stateParams,
    testsSessionsService,
) {
    'ngInject';

    let testIdWatcher;
    const vm = {
        isSessionsLoading: false,
        sessions: [],
        test: null,

        $onDestroy() {
            unbindListeners();
        },
        $onInit() {
            bindListeners();
        },
        onAccordionCollapse,
    };

    return vm;

    function bindListeners() {
        if (!vm.logsView) {
            testIdWatcher = $scope.$watch(() => vm.test, (test) => {
                if (test) {
                    loadTestSessions(test);
                }
            });
        }
    }

    function loadTestSessions(test) {
        vm.isSessionsLoading = true;
        testsSessionsService.getSessions(test.testRunId, test.id)
            .then(({ success, data, message }) => {
                if (success) {
                    vm.sessions = data?.items ?? [];
                    if (vm.sessions.length) {
                        let activeSession = null;

                        vm.sessions.forEach((session) => testsSessionsService.formatSessionValues(session));
                        if (vm.sessionId) {
                            activeSession = vm.sessions.find((session) => session.id === vm.sessionId);
                        } else {
                            activeSession = vm.sessions[0];
                        }

                        activeSession.isActive = true;
                    }
                }
            })
            .finally(() => vm.isSessionsLoading = false);
    }

    function unbindListeners() {
        if (testIdWatcher) { testIdWatcher(); }
    }

    function onAccordionCollapse(index, id) {
        const videoElem = document.querySelector(`#${id} video`);

        if (videoElem && !videoElem.paused) {
            videoElem.pause();
        }
    }
}

export default testSessionsController;

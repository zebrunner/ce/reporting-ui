import template from './test-sessions.html';
import controller from './test-sessions.controller';
import './test-sessions.scss';

const testSessionsComponent = {
    template,
    controller,
    bindings: {
        logsView: '<',
        sessionId: '<',
        sessions: '<',
        test: '<',
    },
};

export default testSessionsComponent;

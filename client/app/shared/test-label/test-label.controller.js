'use strict';

export default function TestLabelController(
    $timeout,
    messageService,
    TestRunService,
    $mdMedia,
) {
    'ngInject';

    const MENU_EDGE_MARGIN = 8; // this is defined in the mdMenuService, so we have to take it into account
    // dimensions for modal used in CSS
    const labelModalDimensions = {
        width: 500,
        height: 200,
        maxHeight: 400,
        extraMargin: 8,
    };
    let mdOffsetValue = {
        x: 0,
        y: 0,
    };
    const vm = {
        jiraIntegration: null,
        loadingTestRailData: false,
        loadingTestRailStatus: 'pending',
        qtestIntegration: null,
        testLabel: null,
        testRailData: null,
        testrailIntegration: null,

        getTestURL,
        openTestRailInfo,

        get labelValue() { return vm.testLabel?.normalizedValue ? vm.testLabel.normalizedValue : vm.testLabel?.value || ''; },
        get isTestRailLabel() {
            return vm.testLabel.key === 'com.zebrunner.app/tcm.testrail.testcase-id';
        },
        get isTestRailLink() {
            return vm.isTestRailLabel && vm.testrailIntegration?.url && vm.testrailIntegration?.enabled;
        },
        get isQTestLabel() {
            return vm.testLabel.key === 'com.zebrunner.app/tcm.qtest.testcase-id';
        },
        get isQTestLink() {
            return vm.isQTestLabel && vm.qtestIntegration?.url && vm.qtestIntegration?.enabled;
        },
        get isXRayLabel() {
            return vm.testLabel.key === 'com.zebrunner.app/tcm.xray.test-key';
        },
        get isXRayLink() {
            return vm.isXRayLabel && vm.jiraIntegration?.url && vm.jiraIntegration?.enabled;
        },
        get isLink() { return vm.isQTestLink || vm.isTestRailLink || vm.isXRayLink; },
        get displayIcon() { return vm.isTestRailLabel || vm.isQTestLabel || vm.isXRayLabel; },
        get mdOffset() { return `${mdOffsetValue.x} ${mdOffsetValue.y}`; },
        set mdOffset(value) { mdOffsetValue = value; },
        get isMobile() { return $mdMedia('xs'); },
    };

    return vm;

    function openTestRailInfo($event, menu, link) {
        if (vm.isMobile) {
            window.open(link);
            return;
        }

        initMenuOffsets($event, labelModalDimensions);
        getTestRailTestCase()
            .then(() => fixModalPosition($event));
        $timeout(() => {
            menu.open($event);
        }, 0);
    }

    function initMenuOffsets($event, labelModalDimensions) {
        const labelSizes = getLabelSizes($event);
        const modalSizes = getModalSizes(labelModalDimensions, labelSizes);

        setMdOffset(modalSizes, labelSizes);
    }

    function getLabelSizes($event) {
        const clientWidth = window.innerWidth || document.documentElement.clientWidth;
        const clientHeight = window.innerHeight || document.documentElement.clientHeight;
        const labelElement = $event.currentTarget;
        const labelElementRect = labelElement.getBoundingClientRect();
        const leftFreeSpace = labelElementRect.left;
        const topFreeSpace = labelElementRect.top;
        const rightFreeSpace = clientWidth - labelElementRect.right;
        const bottomFreeSpace = clientHeight - labelElementRect.bottom;

        return {
            bottomFreeSpace,
            clientHeight,
            clientWidth,
            leftFreeSpace,
            rightFreeSpace,
            labelElementWidth: labelElementRect.width,
            labelElementHeight: labelElementRect.height,
            topFreeSpace,
        };
    }

    function getModalSizes(modalSizes, sizes) {
        const modalHeight = Math.min(modalSizes.height, sizes.clientHeight - MENU_EDGE_MARGIN * 2);
        const modalWidth = Math.min(modalSizes.width, sizes.clientWidth - MENU_EDGE_MARGIN * 2);
        const modalHeightExtra = modalHeight + labelModalDimensions.extraMargin;
        const modalWidthExtra = modalWidth + labelModalDimensions.extraMargin;

        return {
            availablePositions: {
                left: sizes.leftFreeSpace >= modalWidthExtra,
                top: sizes.topFreeSpace >= modalHeightExtra,
                right: sizes.rightFreeSpace >= modalWidthExtra,
                bottom: sizes.bottomFreeSpace >= modalHeightExtra,
            },
            modalHeight,
            modalHeightExtra,
            modalWidth,
            modalWidthExtra,
        };
    }

    function setMdOffset(modalSizes, sizes) {
        let x = 0;
        let y = 0;

        // handles x position in the right/left side
        if (modalSizes.availablePositions.left || modalSizes.availablePositions.right) {
            x = Math.ceil(modalSizes.availablePositions.right
                ? sizes.labelElementWidth + labelModalDimensions.extraMargin
                : -(modalSizes.modalWidth + labelModalDimensions.extraMargin));
            // in this case the desired Y position is overlapping
            y = getYOverlapOffset(sizes, modalSizes.modalHeight);
        } else { // handles overlap position
            const desiredXOffset = -((modalSizes.modalWidth / 2) - (sizes.labelElementWidth / 2));
            const minPossibleXOffset = -sizes.leftFreeSpace;
            const maxPossibleXOffset = sizes.rightFreeSpace + sizes.labelElementWidth - modalSizes.modalWidth;

            x = Math.ceil(Math.min(Math.max(desiredXOffset, minPossibleXOffset), maxPossibleXOffset));
            if (modalSizes.availablePositions.top || modalSizes.availablePositions.bottom) {
                y = Math.ceil(modalSizes.availablePositions.top
                    ? -(modalSizes.modalHeight + labelModalDimensions.extraMargin)
                    : sizes.labelElementHeight + labelModalDimensions.extraMargin
                );
            } else {
                y = getYOverlapOffset(sizes, modalSizes.modalHeight);
            }
        }

        vm.mdOffset = { x, y };
    }

    function getYOverlapOffset(dimensions, modalHeight) {
        const desiredYOffset = -(Math.ceil(modalHeight / 2) - Math.ceil(dimensions.labelElementHeight / 2));
        const maxPossibleYOffset = -dimensions.topFreeSpace;
        let minPossibleYOffset = -(modalHeight - dimensions.bottomFreeSpace);

        minPossibleYOffset = minPossibleYOffset < maxPossibleYOffset ? maxPossibleYOffset : minPossibleYOffset;

        return Math.ceil(Math.min(Math.max(desiredYOffset, maxPossibleYOffset), minPossibleYOffset));
    }

    function getTestURL(type, value) {
        const splitValue = value.split('-');

        switch (type) {
            case 'TESTRAIL_URL':
                return `${vm.testrailIntegration.url}/index.php?/cases/view/${splitValue.pop()}`;
            case 'QTEST_URL':
                return `${vm.qtestIntegration.url}/p/${splitValue[0]}/portal/project#tab=testdesign&object=1&id=${splitValue.pop()}`;
            case 'XRAY_URL':
                return `${vm.jiraIntegration.url}/browse/${value}`;
        }
    }

    function getTestRailTestCase() {
        vm.loadingTestRailData = true;
        vm.loadingTestRailStatus = 'pending';
        const testCaseId = (vm.testLabel.value || '')
            .split('-')
            .pop();

        return TestRunService.getTestRailTestCase(testCaseId)
            .then(({ success, data:testRailData = {}, message}) => {
                if (success) {
                    vm.testRailData = testRailData;
                    vm.testRailData.preconditions = vm.testRailData.preconditions || '';
                    if (vm.testRailData.preconditions.length > 100) {
                        vm.testRailData.shortPreconditions = vm.testRailData.preconditions.slice(0, 100);
                    }
                    vm.loadingTestRailStatus = 'completed';
                } else {
                    if (message) {
                        messageService.error(message);
                    }
                    vm.loadingTestRailStatus = 'error';
                }
            })
            .finally(() => {
                vm.loadingTestRailData = false;
            });
    }

    function fixModalPosition($event) {
        $timeout(() => {
            requestAnimationFrame(() => {
                const el = $event.currentTarget;
                const modalId = el.getAttribute('aria-owns');

                const modalElem = document.getElementById(modalId);

                if (!modalElem) { return; }

                const modalElemSizes = modalElem.getBoundingClientRect();

                initMenuOffsets($event, modalElemSizes);

                $timeout(() => {
                    // it fires repositioning
                    window.dispatchEvent(new Event('resize'));
                }, 0);
            });
        }, 0);
    }
}

'use strict';

import template from './test-label.html';
import controller from './test-label.controller';

import './test-label.scss';

const testLabelComponent = {
    template,
    controller,
    bindings: {
        qtestIntegration: '<',
        testLabel: '<',
        testrailIntegration: '<',
        jiraIntegration: '<',
    },
};

export default testLabelComponent;

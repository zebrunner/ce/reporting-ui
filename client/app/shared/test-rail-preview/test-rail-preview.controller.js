'use strict';

const testRailPreview = () => {
    'ngInject';

    const vm = {
        testLabel: null,
        testUrl: null,
        testRailData: null,
        loadingData: true,
        loadingStatus: 'pending',
        showMoreInfo: false,

        checkIsTable,
    };

    return vm;

    function checkIsTable() {
        return Array.isArray(vm.testRailData?.separatedSteps);
    }
}

export default testRailPreview;

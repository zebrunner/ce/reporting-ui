'use strict';

import template from './test-rail-preview.html';
import controller from './test-rail-preview.controller';

import './test-rail-preview.scss';

const TestRailPreview = {
    template,
    controller,
    bindings: {
        loadingData: '<',
        loadingStatus: '<',
        testRailData: '<',
        testLabel: '<',
        testUrl: '<',
    },
};

export default TestRailPreview;

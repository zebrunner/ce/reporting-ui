import template from './info-popup.html';
import controller from './info-popup.controller';
import './info-popup.scss';

const infoPopupComponent = {
    template,
    controller,
    bindings: {
        infoObj: '<',
        infoStr: '<',
    },
};

export default infoPopupComponent;

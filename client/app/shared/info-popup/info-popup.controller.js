const infoPopupController = function infoPopupController() {
    'ngInject';

    return {
        getFormattedValue,
    }

    function getFormattedValue(value) {
        if (Array.isArray(value)) {
            return value.join(', ');
        }
        return value;
    }
}

export default infoPopupController;

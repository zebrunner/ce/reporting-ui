import template from './zeb-page-header.html';
import './zeb-page-header.scss';

const zebPageHeaderComponent = {
    template,
    bindings: {
        noBreadcrumbs: '<?',
        breadcrumbCustomActions: '<?',
        pageTitle: '<',
    },
    transclude: true,
};

export default zebPageHeaderComponent;

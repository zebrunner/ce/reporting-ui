export const ProfilePhotoController = function ProfilePhotoController(
    $httpMock,
    $scope,
) {
    'ngInject';

    let ngModelWatcherUnsubscriber = angular.noop;
    const vm = {
        autoResize: true,
        chip: false,
        formattedNgModel: '',
        icon: '',
        iconVisible: true,
        label: '',
        rotateHorizontal: false,
        size: 120,
        squared: false,
        src: '',

        $onDestroy,
        $onInit,

        get formattedIcon() {
            if (!vm.icon && !vm.src) {
                if (vm.squared && !vm.chip) {
                    return 'person';
                }
                return 'account_circle';
            }

            return vm.icon || '';
        },
        get mainStyles() {
            return {
                width: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                height: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                borderRadius: angular.isDefined(vm.squared) ? `${ vm.squared }px` : null,
            };
        },
        get customIconStyles() {
            return {
                width: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                height: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                color: '#FFFFFF',
            };
        },
        get chipIconStyles() {
            return {
                fontSize: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                verticalAlign: 'middle',
                color: '#777777',
            };
        },
        get iconStyles() {
            return {
                fontSize: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
                verticalAlign: 'middle',
                color: '#011627',
                backgroundColor: '#d2d2d778',
                borderRadius: '8px',
            };
        },
        get imageStyles() {
            return {
                height: angular.isDefined(vm.imageSize) ? `${ vm.imageSize }px` : null,
            };
        },
        get imageSize() { return vm.autoResize ? vm.size - 4 : vm.size; }
    };

    return vm;

    function $onInit() {
        bindListeners();
    }

    function $onDestroy() {
        unbindListeners();
    }

    function bindListeners() {
        ngModelWatcherUnsubscriber = $scope.$watch(() => vm.ngModel, handleNgModelChange);
    }

    function unbindListeners() {
        ngModelWatcherUnsubscriber();
    }

    function handleNgModelChange(newValue = '') {
        if (newValue) {
            // handle relative link
            if (!newValue.startsWith('http')) {
                // if missed slash
                if (newValue[0] !== '/') {
                    newValue = `/${newValue}`;
                }

                // make it absolute to fix cases if apiHost has different host
                newValue = `${$httpMock.apiHost}${newValue}`;
            }
        }

        vm.formattedNgModel = newValue;
    }
};

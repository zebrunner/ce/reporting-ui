import { ProfilePhotoController as controller } from './profile-photo.controller';
import template from './profile-photo.directive.html';

import './profile-photo.scss';

export const ProfilePhotoDirective = function ProfilePhotoDirective(

) {
    'ngInject';

    return {
        bindToController: true,
        controllerAs: '$ctrl',
        controller,
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            ngModel: '=',
            size: '=?',
            autoResize: '=?',
            icon: '@',
            iconVisible: '=?',
            label: '@',
            rotateHorizontal: '=?',
            src: '@',
            squared: '=?',
            chip: '=?',
        },
        template,
        transclude: true,
    };
};

const warningModalController = (
    $mdDialog,
    actionText,
    message,
    title,
    warningMessage,
) => {
    'ngInject';

    const vm = {
        title,
        message,
        warningMessage,
        actionText,

        $onInit,
        closeModal,
        hideModal,
    };

    return vm;

    function $onInit() {
        vm.actionText = vm.actionText || 'OK';
    }

    function closeModal() {
        $mdDialog.cancel();
    }

    function hideModal() {
        $mdDialog.hide();
    }
};

export default warningModalController;

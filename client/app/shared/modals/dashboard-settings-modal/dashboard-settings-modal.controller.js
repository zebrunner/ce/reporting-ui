const dashboardModalController = function dashboardModalController(
    $scope,
    $mdDialog,
    authService,
    DashboardsService,
    dashboard,
    status,
    defaultDataForRequest,
    messageService,
) {
    'ngInject';

    $scope.isNew = !dashboard.id;
    $scope.dashboard = angular.copy(dashboard);
    $scope.status = status;
    $scope.newAttribute = {};
    $scope.userHasAnyPermission = authService.userHasAnyPermission;

    if ($scope.isNew) {
        $scope.dashboard.hidden = false;
    }

    $scope.createDashboard = function(dashboard, form) {
        $scope.status = status;
        checkDuplicateDashboardTitle(dashboard)
            .then((res) => {
                if (res.success) {
                    form.$setValidity('duplicateKey', false);
                } else if (res.error.status === 404) {
                    DashboardsService.CreateDashboard(dashboard)
                        .then((rs) => {
                            if (rs.success) {
                                messageService.success('Dashboard created');
                                $scope.hide(rs.data, 'CREATE');
                            } else {
                                messageService.error(rs.message);
                            }
                        });
                } else {
                    messageService.error(res.message);
                }
            });
    };

    $scope.updateDashboard = function(dashboard, form) {
        if (!dashboard.editable) {
            $scope.hide();
            return;
        }
        dashboard.widgets = null;
        checkDuplicateDashboardTitle(dashboard)
            .then((res) => {
                if (res.success) {
                    form.$setValidity('duplicateKey', false);
                } else if (res.error.status === 404) {
                    DashboardsService.UpdateDashboard(dashboard)
                        .then((rs) => {
                            if (rs.success) {
                                messageService.success('Dashboard updated');
                                $scope.hide(rs.data, 'UPDATE');
                            } else {
                                messageService.error(rs.message);
                            }
                        });
                } else {
                    messageService.error(res.message);
                }
            });
    };

    $scope.deleteDashboard = function(dashboard){
        DashboardsService.DeleteDashboard(dashboard.id)
            .then((rs) => {
                if (rs.success) {
                    messageService.success('Dashboard deleted');
                } else {
                    messageService.error(rs.message);
                }
            });
        $scope.hide();
    };

    $scope.hide = function (result, action) {
        if (result) {
            result.action = action;
        }
        $mdDialog.hide(result);
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.checkDashboardTitle = function(title, form) {
        form.$setValidity('duplicateKey', true);
        if (title?.length) {
            form.$setValidity('length', !(title.length > 100 || title.length <=1));
        }
    };

    function checkDuplicateDashboardTitle(dashboard) {
        return DashboardsService.CheckDuplicateDashboardTitle(dashboard);
    }

    (function initController(dashboard) {})();
};

export default dashboardModalController;

'use strict';

const testRunCardController = function testRunCardController(
    $mdDialog,
    $mdMedia,
    $rootScope,
    $scope,
    $state,
    $stateParams,
    $timeout,
    authService,
    messageService,
    notificationService,
    TestRunService,
    toolsService,
    UserService,
    UtilService,
) {
        'ngInject';

        const local = {
            currentUser: UserService.currentUser,
        };
        let testRunCardConfigWatcher;
        let testRunCardArtifactsWatcher;
        const vm = {
            testRun: null,
            singleMode: false,
            singleWholeInfo: false,
            showNotificationOption: false,
            showBuildNowOption: false,
            isNotificationAvailable: false,
            isInfoVisible: false,
            specialLabel: null,
            artifactsLimitLetters: 30,
            startCharts: 25,
            isApiFileVisible: false,

            $onInit,
            $onDestroy,
            addToSelectedTestRuns,
            showDetails: showDetails,
            openMenu: openMenu,
            openTestRun: openTestRun,
            copyLink: copyLink,
            markAsReviewed: markAsReviewed,
            showCommentsDialog: showCommentsDialog,
            sendAsEmail: sendAsEmail,
            exportTestRun: exportTestRun,
            sendNotification,
            buildNow: buildNow,
            abort: abort,
            rerun: rerun,
            onTestRunDelete: onTestRunDelete,
            userHasAnyPermission: authService.userHasAnyPermission,
            isButtonVisible,
            copyTestRunTitle,
            prepareAppVersion,
            prepareArtifacts,

            get isMobile() { return $mdMedia('xs'); },
            get isTablet() { return !$mdMedia('gt-md'); },
            get currentOffset() { return $rootScope.currentOffset; },
            get formattedModel() {
                let jsonModel = JSON.parse(vm.testRun.model);
                let formattedModel = '';

                for (let key in jsonModel) {
                    if (jsonModel[key]) {
                        formattedModel += key + ':' + jsonModel[key] + ' / ';
                    }
                }

                return formattedModel.slice(0, -2);
            },
            get goToTestRunParams() {
                const encodedPrevParams = UtilService.base64_encode(JSON.stringify({
                    ...$stateParams,
                    activeTestRunId: vm.testRun.id
                }));

                return {
                    testRunId: vm.testRun.id,
                    runsState: encodedPrevParams,
                };
            },
            get backToRunsParams() {
                const decodedPrevParams = JSON.parse((UtilService.base64_decode($stateParams.runsState))  || '{}');

                return {
                    activeTestRunId: vm.testRun.id,
                    ...decodedPrevParams,
                };
            }
        };

        return vm;

        function $onInit() {
            initSpecialLabels();
            if (vm.isTablet) {
                vm.artifactsLimitLetters = 20;
                vm.startCharts = 15;
            }
            bindListeners();
        }

        function $onDestroy() {
            unbindListeners();
        }

        function bindListeners() {
            testRunCardConfigWatcher = $scope.$watch(() => vm.testRun?.config, (config) => {
                if (config?.appVersion) {
                    prepareAppVersion();
                }
            });
            testRunCardArtifactsWatcher = $scope.$watch(() => vm.testRun?.artifacts, (artifacts) => {
                if (artifacts?.length) {
                    artifacts.forEach((artifact) => {
                        if (!artifact.shortName) {
                            prepareArtifacts();
                        }
                    });
                }
            });
        }

        function unbindListeners() {
            if (testRunCardConfigWatcher) { testRunCardConfigWatcher(); }
            if (testRunCardArtifactsWatcher) { testRunCardArtifactsWatcher(); }
        }

        function prepareArtifacts() {
            return vm.testRun.artifacts.map((artifact) => {
                const shortName = TestRunService.getShortName(artifact.name, vm.startCharts, vm.artifactsLimitLetters);

                if (shortName !== artifact.name) {
                    artifact.shortName = shortName;
                    artifact.isVisible = false;
                }

                return artifact;
            });
        }

        function prepareAppVersion() {
            if (vm.testRun.config.appVersion.length > vm.artifactsLimitLetters) {
                vm.testRun.config.appVersionShort = vm.testRun.config.appVersion.slice(0, vm.startCharts) + '...' + vm.testRun.config.appVersion.slice(vm.testRun.config.appVersion.length - 5);
            } else {
                vm.testRun.config.appVersionShort = vm.testRun.config.appVersion;
            }
        }

        function initSpecialLabels() {
            vm.specialLabel = getSpecialLabelTitle(vm.testRun.labels);
        }

        function addToSelectedTestRuns() {
            vm.onSelect && vm.onSelect(vm.testRun);
        }

        function showDetails() {
            vm.singleWholeInfo = !vm.singleWholeInfo;
        }

        function initMenuRights() {
            vm.showNotificationOption = vm.isNotificationAvailable && vm.testRun.reviewed;
        }

        function openMenu($event, $msMenuCtrl) {
            initMenuRights();
            UtilService.setOffset($event);
            $timeout(function() {
                $msMenuCtrl.open($event);
            });
        }

        function openTestRun() {
            const url = $state.href('tests.runDetails', vm.goToTestRunParams);

            window.open(url,'_blank');
        }

        function copyLink() {
            const url = $state.href('tests.runDetails', vm.goToTestRunParams, {absolute : true});

            url.copyToClipboard();
        }

        function markAsReviewed() {
            showCommentsDialog();
        }

        function sendAsEmail(event) {
            showEmailDialog([vm.testRun], event);
        }

        function showCommentsDialog(event) {
            $mdDialog.show({
                controller: 'CommentsController',
                template: require('../../components/modals/comments/comments.html'),
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    testRun: vm.testRun,
                    isNotificationAvailable: vm.isNotificationAvailable,
                }
            }).then(function(answer) {
                vm.testRun.reviewed = answer.reviewed;
                vm.testRun.comments = answer.comments;
            });
        }

        function showEmailDialog(testRuns, event) {
            $mdDialog.show({
                controller: 'EmailController',
                template: require('../../components/modals/email/email.html'),
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false,
                controllerAs: '$ctrl',
                locals: {
                    testRuns: testRuns
                }
            });
        }

        function exportTestRun() {
            TestRunService.exportTestRunResultsHTML(vm.testRun.id).then(function(rs) {
                if (rs.success && rs.data) {
                    downloadFromByteArray(vm.testRun.testSuite.name.split(' ').join('_') + '.html', rs, 'html');
                } else {
                    if (!rs.data) {
                        rs.message = "Unable to get test run results HTML";
                    }
                    messageService.error(rs.message);
                }
            });
        }

        function downloadFromByteArray(filename, array, contentType) {
            const blob = new Blob([array.data], {type: contentType ? contentType : array.headers('Content-Type')});
            const link = document.createElement('a');

            link.style = 'display: none';
            document.body.appendChild(link);
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;
            link.click();

            //remove link after 10sec
            $timeout(() => {
                link && document.body.removeChild(link);
            }, 10000);
        }

        function sendNotification() {
            notificationService.sendReviewNotification(vm.testRun.id);
        }

        function buildNow(event) {
            showBuildNowDialog(event);
        }

        function rerun(event) {
            showRerunDialog(event);
        }

        function showRerunDialog(event) {
            $mdDialog.show({
                controller: 'TestRunRerunController',
                template: require('../../components/modals/rerun/rerun.html'),
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: false,
                locals: {
                    testRun: vm.testRun,
                }
            });
        }

        function showBuildNowDialog(event) {
            $mdDialog.show({
                controller: 'BuildNowController',
                template: require('../../components/modals/build-now/build-now.html'),
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    testRun: vm.testRun
                }
            });
        }

        function abort() {
            // TODO: refactor this after jenkins integration finished
            // if (vm.isToolConnected('JENKINS')) {
            TestRunService.abortCIJob(vm.testRun.id, vm.testRun.ciRunId)
                .then((rs) => {
                    if (!rs.success){
                        messageService.error(rs.message);
                    }
                    const abortCause = {};

                    abortCause.comment = 'Aborted by ' + local.currentUser.username;
                    TestRunService.abortTestRun(vm.testRun.id, vm.testRun.ciRunId, abortCause)
                        .then((rs) => {
                            if (rs.success){
                                vm.testRun.status = 'ABORTED';
                                messageService.success('Testrun ' + vm.testRun.testSuite.name + ' is aborted');
                            } else {
                                messageService.error(rs.message);
                            }
                        });
                });
            // } else {
            //     messageService.error('Unable connect to jenkins');
            // }
        }

        function onTestRunDelete() {
            if (vm.singleMode) {
                deleteTestRun();
            } else {
                vm.onDelete && vm.onDelete(vm.testRun);
            }
        }

        function deleteTestRun() {
            const confirmation = confirm('Do you really want to delete "' + vm.testRun.testSuite.name + '" test run?');

            if (confirmation) {
                const id = vm.testRun.id;
                TestRunService.deleteTestRun(id).then(function(rs) {
                    const messageData = rs.success ? {success: rs.success, id: id, message: 'Test run{0} {1} removed'} : {id: id, message: 'Unable to delete test run{0} {1}'};

                    UtilService.showDeleteMessage(messageData, [id], [], []);
                    if (rs.success) {
                        $timeout(function() {
                            $state.go('tests.runs');
                        }, 1000);
                    }
                });
            }
        }

        function isButtonVisible(name) {
            // TODO: refactor this after jenkins integration finished
            // const defaultRight = vm.userHasAnyPermission(['reporting:test-runs:read']) && vm.isToolConnected('JENKINS') && vm.testRun?.job?.name !== 'local';
            const defaultRight = vm.userHasAnyPermission(['reporting:test-runs:read']) && vm.testRun?.job?.name !== 'local';

            switch(name) {
                case 'buildNow':
                    return defaultRight;
                case 'abort':
                    return defaultRight && vm.testRun.status === 'IN_PROGRESS';
                case 'rebuild':
                    return defaultRight && vm.testRun.status !== 'IN_PROGRESS';
                default:
                    return false;
            }
        }

        function getSpecialLabelTitle(arr = []) {
            if (!arr.length) {
                return null;
            }

            const branch = arr.find(el => el.key === 'com.zebrunner.app/scm.branch')?.value;
            const url = arr.find(el => el.key === 'com.zebrunner.app/scm.git.repo-url')?.value;

            if (!branch && !url) {
                return null;
            }

            const title = {};

            if (branch) {
                title.branch = branch;
            }
            if (url) {
                title.url = url;
            }

            return title;
        }

        function copyTestRunTitle() {
            if (vm.testRun?.testSuite?.name) {
                vm.testRun.testSuite.name.copyToClipboard();
                messageService.success('The title was copied to the clipboard.');
            }
        }
    };

export default testRunCardController;

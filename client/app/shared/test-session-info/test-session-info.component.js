import template from './test-session-info.html';
import controller from './test-session-info.controller';
import './test-session-info.scss';

const testSessionInfoComponent = {
    template,
    controller,
    bindings: {
        logsView: '<',
        isVnc: '<',
        session: '<',
        testId: '<',
    },
};

export default testSessionInfoComponent;

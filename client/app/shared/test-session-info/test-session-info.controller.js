const testSessionInfoController = function testSessionInfoController(
    $httpMock,
    $mdMedia,
    $sce,
    $scope,
    $stateParams,
    $timeout,
    fullScreenService,
    testsSessionsService,
) {
    'ngInject';

    let sessionWatcher;
    const vm = {
        session: null,
        testId: null,
        videoArtifact: null,

        $onDestroy() {
            unbindListeners();
        },
        $onInit() {
            bindListeners();
        },
        getStatusClass,
        goToTestParams,
        toggleVNCFullScreen,

        get isMobile() { return $mdMedia('xs'); },
    };

    return vm;

    function bindListeners() {
        sessionWatcher = $scope.$watch(() => vm.session, (session) => {
            if (session) {
                formatValues();
                initArtifacts();
                filterRelatedTests();
            }
        });
    }

    function unbindListeners() {
        if (sessionWatcher) { sessionWatcher(); }
    }

    function initArtifacts() {
        if (vm.session?.artifactReferences?.length) {
            vm.session.artifacts = [];
            vm.session.artifactReferences.forEach((artifact) => {
                const artifactName = artifact.name.toLowerCase();

                artifact.url = $sce.trustAsResourceUrl(getArtifactUrl(artifact.value));

                if (artifactName === 'video' || artifactName.includes('live video')) {
                    artifact.type = artifactName.includes('live video') ? 'vnc' : 'video';

                    if (artifact.type === 'vnc') {
                        artifact.isActive = true;
                    }
                    vm.videoArtifact = artifact;
                } else {
                    if (artifactName === 'log') { artifact.name = 'Session logs'; }
                    if (artifactName === 'metadata') { artifact.name = 'Metadata json'; }
                    vm.session.artifacts = [artifact, ...vm.session.artifacts];
                }
            });
        }
    }

    function formatValues() {
        if (!vm.session.formattedValues) {
            testsSessionsService.formatSessionValues(vm.session);
        }
    }

    // do not show link to the current test
    function filterRelatedTests() {
        if (vm.session.tests?.length && vm.testId) {
            vm.session.tests = vm.session.tests.filter((test) => test.id !== vm.testId);
        }
    }

    function getArtifactUrl(path) {
        if (path.startsWith('http')) {
            return path;
        }
        if (path[0] !== '/') {
            path = `/${path}`;
        }

        return `${$httpMock.apiHost}${path}`;
    }

    function getStatusClass(test) {
        return `_${test.status.toLowerCase()}`;
    }

    function goToTestParams(testId) {
        return {
            testId,
            testRunId: $stateParams.testRunId,
        };
    }

    function toggleVNCFullScreen($event) {
        const vncElem = $event.target.closest('.vnc-player');

        if (vncElem && fullScreenService.isFullscreenEnabled()) {
            if (!fullScreenService.isFullscreenActive()) {
                fullScreenService.requestFullscreen(vncElem);
            } else {
                fullScreenService.exitFullscreen();
            }
        }
    }
}

export default testSessionInfoController;

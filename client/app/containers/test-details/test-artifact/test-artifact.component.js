import template from './test-artifact.html';
import controller from './test-artifact.controller';
import './test-artifact.scss';

const testArtifactComponent = {
    template,
    controller,
    bindings: {
        artifact: '<',
        onImageOpen: '&',
    },
};

export default testArtifactComponent;

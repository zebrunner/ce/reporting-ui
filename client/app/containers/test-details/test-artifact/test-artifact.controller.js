const testArtifactController = function testArtifactController(
    ArtifactService,
) {
    'ngInject';

    const vm = {
        artifact: null,

        getArtifactIconId: ArtifactService.getArtifactIconId,
        openImagesViewerModal,
    };

    return vm;

    function openImagesViewerModal($event) {
        vm.onImageOpen({ $event, $artifact: vm.artifact });
    }
}

export default testArtifactController;

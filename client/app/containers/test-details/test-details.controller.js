'use strict';

import ImagesViewerController from '../../components/modals/images-viewer/images-viewer.controller';
import ImagesViewerTemplate from '../../components/modals/images-viewer/images-viewer.html';
import IssuesModalController from '../../components/modals/issues/issues.controller';
import BulkIssuesModalController from '../../components/modals/issues/bulk-issues.controller';
import IssuesModalTemplate from '../../components/modals/issues/issues.html';
import testsFilterModalController from './tests-filter-modal/tests-filter-modal.controller';
import testsFilterModalTemplate from './tests-filter-modal/tests-filter-modal.html';

const testDetailsController = function testDetailsController(
    $httpMock,
    $mdDialog,
    $mdMedia,
    $mdSidenav,
    $q,
    $scope,
    $state,
    $stateParams,
    $timeout,
    $transitions,
    ArtifactService,
    authService,
    messageService,
    modalsService,
    moment,
    pageTitleService,
    integrationsService,
    testsRunsService,
    TestService,
    toolsService,
    UtilService,
) {
    'ngInject';

    const initialCountToDisplay = 50;
    const defaultLimitOptions = [initialCountToDisplay, initialCountToDisplay * 2];
    const fileTypes = ['pdf', 'apk', 'exe', 'doc', 'xls', 'txt', 'png'];
    let firstIndex;
    let lastIndex;
    const scrollableParentElement = document.querySelector('.page-wrapper');
    let jiraIntegration = {};
    let testrailIntegration = {};
    let qtestIntegration = {};
    const defaultStatusFilter = {
        field: 'status',
        values: [],
    };
    let _at = [];
    let scrollTickingTimeout = null;
    const fileExtensionPattern = /\.([0-9a-z]+)(?:[?#]|$)/i;
    const vm = {
        scrollTicking: false,
        countPerPage: initialCountToDisplay,
        currentPage: 1,
        testsViewMode: 'plain',
        filters: {
            status: { ...defaultStatusFilter },
            grouping: null,
        },
        sortOptions: [
            {
                name: 'Creation time (default)',
                field: 'createdAt',
                reverse: false,
                isDefault: true,
            },
            {
                name: 'Failure',
                field: 'message',
                reverse: false,
            },
            {
                name: 'Test name',
                field: 'name',
                reverse: false,
            },
            {
                name: 'Duration (long - short)',
                field: 'elapsed',
                reverse: true,
            },
            {
                name: 'Duration (short - long)',
                field: 'elapsed',
                reverse: false,
            },
        ],
        groupingOptions: [],
        groupingFilters: {
            'plain': {
                name: 'Default (none)',
                weight: 0,
                isEmptyGroupData: true,
                groupData: {
                    'All tests': {
                        name: 'All tests',
                    },
                },
                isDefault: true,
            },
            'class': {
                name: 'Class',
                field: 'testClass',
                weight: 1,
                isEmptyGroupData: true,
                groupData: {},
            },
            'package': {
                name: 'Package',
                field: 'notNullTestGroup',
                weight: 2,
                isEmptyGroupData: true,
                groupData: {},
            },
            'labels': {
                name: 'Label',
                field: 'labels',
                weight: 3,
                isEmptyGroupData: true,
                groupData: {},
            },
            'failure': {
                name: 'Failure',
                field: 'message',
                weight: 4,
                isEmptyGroupData: true,
                groupData: {},
            },
            'sessions': {
                name: 'Sessions',
                field: 'relatedSessions',
                weight: 5,
                isEmptyGroupData: true,
                groupData: {},
            }
        },
        activeGroupingFilter: null,
        testRun: null,
        testsLoading: true,
        subscriptions: {},
        zafiraWebsocket: null,
        testId: null,
        configSnapshot: null,
        bulkChangeInProgress: false,
        batchButtons: [
            {
                text: 'Mark as Passed',
                altText: 'Passed',
                onClick: bulkChangeStatus,
                completed: false,
                class: '_green-icon',
                action: 'PASSED',
                mobileIconClass: 'fa-check-circle',
                get disabled() { return !ableToMarkSelectedTests('PASSED'); },
            },
            {
                text: 'Mark as Failed',
                altText: 'Failed',
                onClick: bulkChangeStatus,
                completed: false,
                class: '_red-icon',
                action: 'FAILED',
                mobileIconClass: 'fa-times-circle',
                get disabled() { return !ableToMarkSelectedTests('FAILED'); },
            },
            {
                text: 'Link issue',
                altText: 'Link issue',
                onClick: bulkLinkIssue,
                completed: false,
                class: '_svg-icon',
                mdIcon: 'bug_report',
                action: 'LINK',
                mobileIconClass: 'fa-bug',
                get disabled() { return !vm.isBulkLinkIssuePossible; },
            },
        ],
        isNotificationAvailable: false,
        isBulkLinkIssuePossible: false,
        isIntegrationInitialized: false,
        searchCriteria: '',
        selectedOrder: null,
        tenant: authService.tenant || 'zafira',
        sessionsInfoActiveTest: null,
        sessionsData: [],
        isTestLoading: true,
        breadcrumbsCustomActions: null,

        get backToRunsParams() {
            const decodedPrevParams = JSON.parse((UtilService.base64_decode($stateParams.runsState)) || '{}');

            return {
                activeTestRunId: vm.testRun.id,
                ...decodedPrevParams,
            };
        },
        get isMobile() { return $mdMedia('xs'); },
        get isTablet() { return !$mdMedia('gt-md'); },
        get activeTests() { return _at || []; },
        set activeTests(data) { _at = data; return _at; },
        get testsToDisplay() {
            return this.activeTests.slice(firstIndex, lastIndex);
        },
        get limitOptions() { return !$mdMedia('xs') ? defaultLimitOptions : false; },
        get empty() { return !this.testRun?.tests?.length; },
        get jiraIntegration() { return jiraIntegration; },
        get testrailIntegration() { return testrailIntegration; },
        get qtestIntegration() { return qtestIntegration; },
        get isMobileSearchActive() { return this.isMobile && this.searchCriteria; },
        get isStatusFilteringActive() { return isStatusFilteringActive(); },
        get isSortingActive() { return isSortingActive(); },
        get currentTitle() { return pageTitleService.pageTitle; },
        get selectedTestsCount() { return getSelectedTestsCount(); },
        get hasSelectedTests() { return hasSelectedTests(); }, // TODO: refactor to use cached result
        get hasUnselectedTests() { return hasUnselectedTests(); }, // TODO: refactor to use cached result
        get hasSelectedGroups() { return hasSelectedGroups(); },
        get testsCount() { return (vm.testRun?.tests ?? []).length; },
        get expandAllLabelsFlag() { return TestService.expandAllLabels; },
        get canManageTestRuns() { return vm.userHasAnyPermission(['reporting:tests:update']); },

        $onInit,
        ableToLinkIssueToTest,
        ableToMarkSelectedTests,
        ableToMarkTestAsFailed,
        ableToMarkTestAsPassed,
        changeTestStatus,
        changeViewMode,
        clearTestsSelection,
        closeSessionsSidenav,
        filterByStatus,
        getArtifactIconId: ArtifactService.getArtifactIconId,
        getEmptyTestsMessage,
        getSelectedGroupText,
        getSelectedOrderText,
        getTestURL,
        goToTestDetails,
        goToTestRuns,
        groupHasSelectedTests,
        groupHasUnselectedTests,
        isExcludedLabel,
        onBulkClose,
        onAllTestsSelect,
        onGroupSelect,
        onOrderChange,
        onPageChange,
        onSearch,
        onTestSelect,
        openSessionsSidenav,
        onViewModeSelect,
        openImagesViewerModal,
        onSidenavSwipeRight,
        resetStatusFilterAndOrdering,
        showDetailsDialog,
        showFilterDialog,
        toggleGroupingFilter,
        handleAllLabelsExpand,
        isTestsLabelsCanExpand,
        userHasAnyPermission: authService.userHasAnyPermission,
    };

    return vm;

    function $onInit() {
        if (!vm.testRun) { return; }

        pageTitleService.setTitle(vm.testRun.name);
        vm.selectedOrder = getDefaultOrderOption();
        initGroupingOptions();
        initIntegrations();
        initFirstLastIndexes();
        initWebsocket();
        initTests()
            .finally(() => {
                vm.isTestLoading = false;
            });
        initJobMetadata();
        getNotificationAvailability();
        bindEvents();
        vm.testRun.downloadArtifacts = downloadArtifacts;
        vm.breadcrumbsCustomActions = {
            'Test runs': {
                action: vm.goToTestRuns,
            },
        };
    }

    function goToTestRuns() {
        $state.go('tests.runs', vm.backToRunsParams);
    }

    function getSessionsDataForTest(test) {
        return {
            ...test,
            relatedSessions: vm.sessionsData
                .filter((session) => session.tests.find(({ id }) => id === test.id))
                .map((sessionItem) => ({
                    ...sessionItem,
                    name: `${sessionItem.name} (${sessionItem.sessionId})`,
                    value: sessionItem.sessionId,
                })),
        };
    }

    function handleAllLabelsExpand(value) {
        if (TestService.expandAllLabels === value || vm.showLabelsControls <= 5) { return; }

        TestService.expandAllLabels = value;
    }

    function isTestsLabelsCanExpand() {
        return vm.testRun?.tests.reduce((acc, item) => {
            if(acc < item.labels?.length) {
                acc = item.labels.length;
            }
            return acc;
        }, 0);
    }

    function downloadArtifacts() {
        const options = {
            data: Object.values(vm.testRun.tests),
            field: 'artifactsToShow',
            name: vm.testRun.testSuite.name,
        };

        ArtifactService.downloadArtifacts(options);
    }

    function initTests() {
        testsRunsService.preparePlatformsData(vm.testRun);

        return loadTests(vm.testRun.id);
    }

    function loadTests(testRunId) {
        const params = {
            page: 1,
            pageSize: 100000,
            testRunId,
        };

        return TestService.searchTests(params)
            .then((rs) => {
                if (rs.success) {
                    vm.testRun.tests = rs.data?.results ?? [];
                    prepareTestsData();
                    vm.showLabelsControls = isTestsLabelsCanExpand();
                } else {
                    console.error(rs.message);
                }
            })
            .then(() => {
                vm.testId = getSelectedTestId();
                vm.testRun.tests = vm.testRun.tests.map((test) => {
                    if (test.testSessionsCount) {
                        return getSessionsDataForTest(test);
                    }

                    return test;
                });
                onInitTests();
            })
            .finally(() => {
                vm.testsLoading = false;
            });
    }

    /**
     * Registers event handlers on controller init event
     * Also registers unsubscribers on controller destroy event
     */
    function bindEvents() {
        $scope.$on('$destroy', function () {
            if (vm.zafiraWebsocket && vm.zafiraWebsocket.connected) {
                for (let key in vm.subscriptions) {
                    vm.subscriptions[key].unsubscribe();
                }
                $timeout(function () {
                    vm.zafiraWebsocket.disconnect();
                }, 0, false);
                UtilService.websocketConnected('zafira');
            }

            if (vm.isMobile) {
                angular.element(scrollableParentElement).off('scroll.hideFilterButton', onScroll);
            }
        });

        const onTransStartSubscription = $transitions.onStart({}, function (trans) {
            const toState = trans.to();

            if (toState.name !== 'tests.runInfo') {
                TestService.clearDataCache();
            }

            onTransStartSubscription();
        });

        if (vm.isMobile) {
            angular.element(scrollableParentElement).on('scroll.hideFilterButton', onScroll);
        }
    }

    /* ------------------------------- UI handlers ------------------------------ */
    // TODO: refactor to use as URL query params
    function goToTestDetails(testId) {
        $state.go('tests.runInfo', {
            testRunId: vm.testRun.id,
            testId,
            configSnapshot: getConfigSnapshot(),
            runsState: $stateParams.runsState,
        });
    }

    function onSearch() {
        onFilterChange(true);
    }

    function filterDataBySearchCriteria(data = vm.activeTests) {
        return data.filter((item) => {
            const searchRegExp = new RegExp(vm.searchCriteria, 'ig');
            const titleMatch = searchRegExp.test(item.name);
            const ownerMatch = searchRegExp.test(item.owner);
            const messageMatch = searchRegExp.test(item.message);
            const deviceMatch = searchRegExp.test(item.testConfig?.device);

            return titleMatch || ownerMatch || messageMatch || deviceMatch;
        });
    }

    function filterByStatus(statuses = []) {
        onStatusFilterChange({ ...defaultStatusFilter, values: statuses });
    }

    function resetStatusFilterAndOrdering() {
        vm.searchCriteria = '';
        vm.filters.status = { ...defaultStatusFilter };
        vm.selectedOrder = getDefaultOrderOption();
        vm.testsViewMode = 'plain';
        onFilterChange(true);
        onViewModeSelect();
    }

    function changeViewMode(mode) {
        if (vm.filters.grouping?.values) {
            vm.groupingFilters[vm.testsViewMode].cachedValues = vm.filters.grouping.values;
        }
        switch (mode) {
            case 'plain':
                onPlainViewModeActivate();
                break;
            default:
                onFilteredViewModeActivate();
        }
    }

    function onViewModeSelect() {
        $timeout(() => {
            if (vm.testsViewMode === 'Default (none)') {
                vm.testsViewMode = 'plain';
            }
            changeViewMode(vm.testsViewMode);
        }, 0);
    }

    function onGroupSelect(selectedGroup) {
        let selectedTests = getTestsByGroups([selectedGroup]);
        const selectionValue = !selectedTests.some(({ selected}) => selected);

        selectedTests.forEach((test) => test.selected = selectionValue);
        updateBulkLinkingPossibility();
        updateGroupsSelectionStatuses();
    }

    function getTestsByGroups(groups) {
        let selectedGroupFilter;

        if (vm.filters.grouping && groups?.length) {
            selectedGroupFilter = {
                ...vm.filters.grouping,
                values: groups.map(({ name }) => name),
            };
        }

        const filtersToApply = [vm.filters.status, selectedGroupFilter].filter(Boolean);

        return getFilteredTests(filtersToApply);
    }

    function toggleGroupingFilter(filterGroup) {
        const filter = vm.groupingFilters[vm.testsViewMode];

        if (vm.activeGroupingFilter === filterGroup) {
            vm.activeGroupingFilter = null;
            vm.filters.grouping = { ...vm.filters.grouping, values: [''] };
        } else {
            vm.activeGroupingFilter = filterGroup;
            vm.filters.grouping = { ...vm.filters.grouping, values: [filterGroup.name] };
        }
        onFilterChange(true, true);
    }

    function onPageChange() {
        updateFirstLastIndexes();
    }

    function changeTestStatus({ test, status } = {}) {
        const currentTest = vm.testRun.tests.find(item => item.id === test.id);

        if (currentTest.status !== status && confirm(`Do you really want mark test as ${status}?`)) {
            const copy = {...currentTest};

            copy.status = status;
            TestService.updateTest(copy)
                .then(rs => {
                    if (rs.success) {
                        currentTest.status = status;
                        currentTest.issueReferences = [];
                        onFilterChange(false, true);

                        messageService.success(`Test was marked as ${currentTest.status}`);
                    } else {
                        console.error(rs.message);
                    }
                });
        }
    }

    function bulkChangeStatus(event, btn) {
        if (vm.bulkChangeInProgress) { return; }

        const selectedTests = vm.testsToDisplay.filter(test => test.selected);
        const ids = selectedTests.map(({ id }) => id);
        const params = {
            ids,
            operation: 'STATUS_UPDATE',
            value: btn.action,
        };

        vm.bulkChangeInProgress = true;
        TestService.updateTestsStatus(vm.testRun.id, params)
            .then(res => {
                if (res.success) {
                    const patchedTests = res.data || [];
                    const selectedTestsObj = selectedTests.reduce((accum, test) => {
                        accum[test.id] = test;

                        return accum;
                    }, {});

                    patchedTests.forEach(patchedTest => {
                        selectedTestsObj[patchedTest.id].status = patchedTest.status;
                    });
                    // display alt text for a while (1sec)
                    btn.completed = true;
                    $timeout(() => {
                        btn.completed = false;
                        vm.bulkChangeInProgress = false;
                    }, 1000);

                    messageService.success('Tests were marked as ' + btn.action);
                    clearTestsSelection();
                } else {
                    messageService.error(res.message);
                    vm.bulkChangeInProgress = false;
                }
            });
    }

    function bulkLinkIssue(event) {
        const tests = getSelectedTests(vm.testRun.tests);

        modalsService
            .openModal({
                controller: BulkIssuesModalController,
                template: IssuesModalTemplate,
                parent: angular.element(document.body),
                targetEvent: event,
                controllerAs: '$ctrl',
                fullscreen: false,
                locals: {
                    testRunId: vm.testRun.id,
                    tests,
                },
            })
            .catch(() => {});
    }

    /* ------------------------------ Work with DOM ----------------------------- */
    function onScroll() {
        if (!vm.scrollTicking) {
            vm.scrollTicking = true;
            $scope.$apply();
            scrollTickingTimeout = $timeout(() => {
                vm.scrollTicking = false;
            }, 300);
        } else {
            $timeout.cancel(scrollTickingTimeout);
            scrollTickingTimeout = $timeout(() => {
                vm.scrollTicking = false;
            }, 300);
        }
    }

    function getTestURL(type, value) {
        value = value.split('-');

        switch (type) {
            case 'TESTRAIL_URL':
                return `${testrailIntegration.url}/index.php?/cases/view/${value.pop()}`;
            case 'QTEST_URL':
                return `${qtestIntegration.ul}/p/${value[0]}/portal/project#tab=testdesign&object=1&id=${value.pop()}`;
        }
    }

    /* ------------------------------ Other Helpers ----------------------------- */
    function initIntegrations() {
        $q.all([
            getJiraIntegration(),
            getTestrailIntegration(),
            getQtestIntegration(),
        ])
            .finally(() => vm.isIntegrationInitialized = true);
    }

    function getJiraIntegration() {
        return getIntegration('jira')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                jiraIntegration = integration;

                return jiraIntegration;
            });
    }

    function getTestrailIntegration() {
        return getIntegration('testrail')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                testrailIntegration = integration;

                return testrailIntegration;
            });
    }

    function getQtestIntegration() {
        return getIntegration('qtest')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                qtestIntegration = integration;

                return qtestIntegration;
            });
    }

    function getIntegration(path) {
        return integrationsService.getIntegration(path)
            .then(({ data = {} }) => data);
    }

    function formatIntegrationUrl(url) {
        if (url) {
            url = url.replace(/\/$/, '');
        }

        return url;
    }

    // TODO: add logic to handle case when we return from internal page
    // and don't have configSnapshot: we can calculate page, firstIndex and lastIndex for default viewMode
    function initFirstLastIndexes() {
        firstIndex = 0;
        lastIndex = initialCountToDisplay;
    }

    function resetPagination() {
        vm.currentPage = 1;
        initFirstLastIndexes();
    }

    function getConfigSnapshot() {
        return {
            countPerPage: vm.countPerPage,
            currentPage: vm.currentPage,
            testsViewMode: vm.testsViewMode,
            filters: vm.filters,
            selectedOrder: vm.selectedOrder,
            searchCriteria: vm.searchCriteria,
        };
    }

    function applyConfigSnapshot() {
        vm.configSnapshot.selectedOrder = vm.sortOptions.find(({ name }) => name === vm.configSnapshot.selectedOrder.name);
        Object.assign(vm, vm.configSnapshot);

        if (vm.configSnapshot.testsViewMode !== 'plain' && vm.configSnapshot.filters?.grouping?.values) {
            const groupName = vm.configSnapshot.filters.grouping.values[0];
            const selectedFilterGroup = vm.groupingFilters[vm.testsViewMode].groupData[groupName];

            if (selectedFilterGroup) {
                toggleGroupingFilter(selectedFilterGroup);
            }
        }

        vm.configSnapshot = null;
    }

    function getSelectedTestId() {
        let successOldUrl = TestService.getPreviousUrl();
        let selectedId;

        if (successOldUrl) {
            TestService.clearPreviousUrl();
            TestService.unsubscribeFromLocationChangeStart();
        }

        if (successOldUrl && successOldUrl.includes('/tests/')) {
            const parsedId = parseInt(successOldUrl.split('/').pop(), 10);

            if (!isNaN(parsedId)) {
                selectedId = parsedId;
            }
        }

        return selectedId;
    }

    function updateFirstLastIndexes() {
        const newFirstIndex = (vm.currentPage - 1) * vm.countPerPage;
        let newLastIndex = newFirstIndex + vm.countPerPage;

        firstIndex = newFirstIndex;
        lastIndex = newLastIndex;
        scrollableParentElement.scrollTop = 0;
    }

    function initJobMetadata() {
        if (vm.testRun.job && vm.testRun.job.jobURL) {
            const buildNumber = vm.testRun.buildNumber ? `/${vm.testRun.buildNumber}` : '';
            !vm.testRun.jenkinsURL && (vm.testRun.jenkinsURL = `${vm.testRun.job.jobURL}${buildNumber}`);
            !vm.testRun.UID && (vm.testRun.UID = `${vm.testRun.testSuite.name} ${vm.testRun.jenkinsURL}`);
        }
    }

    function getEventFromMessage(message) {
        return JSON.parse(message.replace(/&quot;/g, '"').replace(/&lt;/g, '<').replace(/&gt;/g, '>'));
    }

    function isCurrentTestRunStatistics(event) {
        return vm.testRun.id === +event.testRunStatistics.testRunId;
    }

    function isStatusFilteringActive() {
        return vm.filters.status && vm.filters.status.values && vm.filters.status.values.length;
    }

    function isSortingActive() {
        return vm.selectedOrder && !vm.selectedOrder.isDefault;
    }

    function getTestsIndexByID(id) {
        if (!vm.testRun.tests) { return -1; }

        return vm.testRun.tests.findIndex(test => test.id === id);
    }

    function getTestById(id) {
        return vm.testRun.tests.find(test => test.id === id);
    }

    function getEmptyTestsMessage(group) {
        let message = '';

        if (vm.empty && vm.testRun.status !== 'IN_PROGRESS') {
            message = 'No tests';
        }
        if (!vm.empty && !vm.activeTests.length && (vm.isStatusFilteringActive || vm.searchCriteria) && (vm.testsViewMode === 'plain' || vm.activeGroupingFilter === group)) {
            message = 'No tests matching selected criteria';
        }
        if (vm.testRun.status === 'IN_PROGRESS' && vm.empty && (vm.testsViewMode === 'plain' || vm.activeGroupingFilter === group)) {
            message = 'No tests yet';
        }

        return message;
    }

    function updateBulkLinkingPossibility() {
        const selectedTests = vm.testRun.tests.filter((test) => test.selected);
        const hasSelectedTests = !!selectedTests.length;
        const onlyLinkableTests = !selectedTests.some((test) => !ableToLinkIssueToTest(test));

        vm.isBulkLinkIssuePossible = hasSelectedTests && onlyLinkableTests;
    }

    function updateGroupsSelectionStatuses() {
        if (vm.testsViewMode === 'plain') { return; }

        const activeFilter = vm.groupingFilters[vm.testsViewMode];

        Object.values(activeFilter.groupData)
            .forEach((group) => {
                group.selected = isAllGroupItemsSelected(group);
            });
    }

    function updateGroupsTestsCounts() {
        if (vm.testsViewMode === 'plain') { return; }

        Object.values(vm.groupingFilters[vm.testsViewMode].groupData)
            .forEach((group) => {
                group.count = getTestsByGroups([group]).length;
            });
    }

    function isAllGroupItemsSelected(group) {
        const allGroupTests = getTestsByGroups([group]);
        const selectedTests = getSelectedTests(allGroupTests);

        return allGroupTests.length && allGroupTests.length === selectedTests.length;
    }

    /* --------------------- Filtering and ordering helpers --------------------- */

    function onStatusFilterChange(newFilter) {
        vm.filters.status = newFilter;
        onFilterChange(true);
    }

    function isFitsByFilter(itemValue, filterValues) {
        if (!filterValues?.length) {
            return true;
        } else if (!itemValue || Array.isArray(itemValue) && !itemValue.length) {
            return false;
        } else if (Array.isArray(itemValue)) {
            return itemValue.find(({ value }) => (filterValues.findIndex((fValue) => {
                fValue = typeof fValue === 'string' ? fValue.toLowerCase() : fValue;
                value = typeof value === 'string' ? value.toLowerCase() : value;

                return fValue && value && fValue === value;
            }) > -1));
        }

        return (filterValues.findIndex(fValue => {
            fValue = typeof fValue === 'string' ? fValue.toLowerCase() : fValue;
            itemValue = typeof itemValue === 'string' ? itemValue.toLowerCase() : itemValue;

            return fValue && itemValue && (fValue === itemValue || fValue === (typeof itemValue === 'string' && itemValue.substring(0, 60)));
        }) > -1);
    }

    function onPlainViewModeActivate() {
        vm.activeGroupingFilter = null;
        vm.filters.grouping = null;
        onFilterChange(true);
        // resetStatusFilterAndOrdering(); // Use this if you need to reset status filters and ordering on view mode change
    }

    function onFilteredViewModeActivate() {
        const values = vm.groupingFilters[vm.testsViewMode].cachedValues || [''];

        vm.filters.grouping = { field: vm.groupingFilters[vm.testsViewMode].field, values };
        onFilterChange(true);
        // resetStatusFilterAndOrdering(); // Use this if you need to reset status filters and ordering on view mode change
    }

    /* -------------- Work with data (filtering, ordering and etc.) ------------- */

    /**
     * Init tests for view by filtering and sorting with default values
     */
    function onInitTests() {
        initGroupingData();
        if (vm.configSnapshot) {
            applyConfigSnapshot();
            onPageChange();
            onFilterChange();
        } else {
            onFilterChange(true);
        }
    }

    function initGroupingData() {
        vm.testRun.tests.forEach(updateGroupDataFromTest);
        updateGroupDataStates();
    }

    function updateGroupDataFromTest(test) {
        const {
            class: classData,
            package: packageData,
            labels: labelsData,
            failure: failureData,
            sessions: sessionsData,
        } = vm.groupingFilters;

        if (test[classData.field]) {
            handleGroupData(classData.groupData, test[classData.field]);
        }
        if (test[packageData.field]) {
            handleGroupData(packageData.groupData, test[packageData.field]);
        }
        if (Array.isArray(test[labelsData.field])) {
            test[labelsData.field]
                //skip Testrail and Qtest labels (see ZEB-486 ticket)
                .filter((label) => !isExcludedLabel(label))
                .forEach((label) => handleGroupData(labelsData.groupData, label.value, `${label.key}: ${label.value}`));
        }
        if (test[failureData.field]) {
            handleGroupData(failureData.groupData, test[failureData.field].substring(0, 60));
        }
        if (Array.isArray(test[sessionsData.field])) {
            test[sessionsData.field]
                .forEach((session) => handleGroupData(sessionsData.groupData, session.sessionId, session.name));
        }
    }

    function updateGroupDataStates() {
        Object.keys(vm.groupingFilters)
            .forEach((groupName) => {
                vm.groupingFilters[groupName].isEmptyGroupData = !Object.keys(vm.groupingFilters[groupName].groupData || {}).length;
            });
    }

    function handleGroupData(groupData, key, label) {
        if (!groupData.hasOwnProperty(key)) {
            groupData[key] = {
                label: label || key,
                name: key,
                selected: false,
                count: 0,
            };
        }
    }

    /**
     * Sorts array of tests by specified field
     *
     * @param {[]} [data=vm.activeTests]
     * @returns {[]} sorted array of tests
     */
    // so looks like them shouldn't be sorted by duration and should be placed in the end of the list
    function getOrderedTests(data = vm.activeTests) {
        const { field, reverse } = vm.selectedOrder;

        if (field === 'message') {
            return sortByFailures(data);
        } else if (field === 'createdAt') {
            return [...data.sort((a, b) => moment(a.createdAt) - moment(b.createdAt))];
        }

        return UtilService.sortArrayByField(data, field, reverse);
    }

    function sortByFailures(data) {
        const statusWeights = {
            'FAILED': 0,
            'SKIPPED': 1,
            'ABORTED': 2,
            'PASSED': 3,
            'IN_PROGRESS': 4
        };
        const statusesToSortByDate = ['PASSED', 'IN_PROGRESS'];
        const groupedData = data.reduce((accum, item) => ({
            ...accum,
            [item.status]: [...(accum[item.status] || []), item],
        }), {});

        return Object.keys(groupedData)
            .sort((a, b) => statusWeights[a] - statusWeights[b])
            .flatMap((key) => {
                const sortingField = statusesToSortByDate.includes(key) ? 'startTime' : 'message';

                return groupedData[key].sort((a, b) => {
                    if (a[sortingField] < b[sortingField]) { return -1; }
                    if (a[sortingField] > b[sortingField]) { return 1; }

                    return 0;
                });
            });
    }

    /**
     * Sorts current array of tests
     */
    function onOrderChange() {
        vm.activeTests = getOrderedTests();
    }

    function onAddingNewTest(test) {
        updateGroupDataFromTest(test);
        onFilterChange(false, true);
    }

    function onUpdatingTest(test) {
        //recollect testsToDisplay, if old test was in current displaying scope
        if (vm.testsToDisplay.find(({ id }) => id === test.id)) {
            onFilterChange(false, true);
        } else {
            updateGroupsTestsCounts();
        }
    }

    function onFilterChange(shouldResetPagination, skipSelectionReset) {
        const filters = [vm.filters.status, vm.filters.grouping].filter(Boolean);
        let filteredData = getFilteredTests(filters);

        //reset tests selection
        if (!skipSelectionReset) {
            clearTestsSelection();
        }

        if (shouldResetPagination) {
            resetPagination();
        }

        vm.activeTests = getOrderedTests(filteredData);
        updateGroupsTestsCounts();
    }

    function getFilteredTests(filters) {
        let filteredData = vm.testRun.tests.filter((test) => {
            return filters.every(filter => {
                return isFitsByFilter(test[filter.field], filter.values);
            });
        });

        if (vm.searchCriteria) {
            filteredData = filterDataBySearchCriteria(filteredData);
        }

        return filteredData;
    }

    function prepareArtifacts(test) {
        test.artifactsToShow = test.artifacts.reduce(function (formatted, artifact) {
            const name = artifact.name.toLowerCase();

            if (!name.includes('live video') && artifact.value && !artifact.value.includes('artifacts/test-sessions/')) {
                const links = artifact.value.split(' ');
                let link = links[0];
                const extensionMatch = link.match(fileExtensionPattern);

                // if link is relative
                if (!link.startsWith('http')) {
                    if (link[0] !== '/') {
                        link = `/${link}`;
                    }

                    artifact.value = `${$httpMock.apiHost}${link}`;
                } else if (!link.startsWith(window.location.origin) && $httpMock.apiHost && !link.startsWith($httpMock.apiHost)) {
                    artifact.isExternalLink = true;
                }
                if (!artifact.isExternalLink && !vm.testRun.hasArtifacts) {
                    vm.testRun.hasArtifacts = true;
                }
                if (extensionMatch) {
                    artifact.extension = extensionMatch[1];
                }

                formatted.push(artifact);
            }

            return formatted;
        }, []);
    }

    function prepareTestsData() {
        vm.testRun.tests.forEach(prepareTestData);
    }

    function addNewTest(test) {
        prepareTestData(test);
        vm.testRun.tests = [...vm.testRun.tests, test];
        onAddingNewTest(test);
    }

    function updateTest(test, index) {
        prepareTestData(test);
        vm.testRun.tests[index] = {...vm.testRun.tests[index], ...test};
        onUpdatingTest(vm.testRun.tests[index]);
    }

    function prepareTestData(test) {
        test.elapsed = test.finishTime ? (test.finishTime - test.startTime) : Number.MAX_VALUE;
        prepareArtifacts(test);
        test.labels = sortLabels(prepareLabels(test.labels));
    }

    function prepareLabels(labels = []) {
        return labels
            .map((label) => {
                if (isExcludedLabel(label)) {
                    return {
                        ...label,
                        normalizedValue: label.value.split('-').pop(),
                    };
                }

                return label;
            });
    }

    function sortLabels(labels) {
        const qTestArr = [];
        const testrailArr = [];
        const otherArr = [];

        labels.forEach(el => {
            if (el.key === 'com.zebrunner.app/tcm.testrail.testcase-id') {
                testrailArr.push(el);
            } else if (el.key === 'com.zebrunner.app/tcm.qtest.testcase-id') {
                qTestArr.push(el);
            } else {
                otherArr.push(el);
            }
        });

        return [...sortLabelsById(testrailArr), ...sortLabelsById(qTestArr), ...otherArr.sort((a, b) => a.key.localeCompare(b.key))];
    }

    function sortLabelsById(arr) {
        return arr.sort((a, b) => parseInt(a.normalizedValue, 10) - parseInt(b.normalizedValue, 10));
    }

    function isExcludedLabel(label) {
        return label.key === 'com.zebrunner.app/tcm.testrail.testcase-id' || label.key === 'com.zebrunner.app/tcm.qtest.testcase-id';
    }

    /* ----------------------------- Dialog openers ----------------------------- */
    function showDetailsDialog({ test, event } = {}) {
        modalsService
            .openModal({
                controller: IssuesModalController,
                template: IssuesModalTemplate,
                parent: angular.element(document.body),
                targetEvent: event,
                controllerAs: '$ctrl',
                fullscreen: false,
                locals: {
                    test,
                },
            })
            .catch(() => {});
    }

    function showFilterDialog(event) {
        $mdDialog.show({
            controller: testsFilterModalController,
            template: testsFilterModalTemplate,
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            fullscreen: false,
            bindToController: true,
            controllerAs: '$ctrl',
            onComplete: () => {
                $(window).on('resize.filterDialog', () => {
                    if (!vm.isMobile) {
                        $mdDialog.hide();
                    }
                });
            },
            onRemoving: () => {
                $(window).off('resize.filterDialog');
            },
            locals: {
                statusInitValues: vm.filters.status.values,
                searchCriteria: vm.searchCriteria,
                defaultValues: {
                    status: defaultStatusFilter.values,
                },
                filterByStatus: (filters, searchCriteria) => {
                    if (vm.isMobile) {
                        vm.searchCriteria = searchCriteria;
                    }

                    filterByStatus(filters);
                },
                reset: () => {
                    if (vm.isMobile) {
                        vm.searchCriteria = '';
                    }

                    resetStatusFilterAndOrdering();
                },
            },
        });
    }

    function openImagesViewerModal({ event, artifact, test } = {}) {
        ArtifactService.extractImageArtifacts([test]);

        $mdDialog.show({
            controller: ImagesViewerController,
            template: ImagesViewerTemplate,
            controllerAs: '$ctrl',
            bindToController: true,
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            fullscreen: false,
            escapeToClose: false,
            locals: {
                test,
                activeArtifactValue: artifact.value,
            },
        });
    }

    /* --------------------------- Work with Websocket -------------------------- */
    function initWebsocket() {
        const wsName = 'zafira';

        vm.zafiraWebsocket = Stomp.over(new SockJS(`${$httpMock.apiHost}/api/reporting/api/websockets`));
        vm.zafiraWebsocket.debug = null;
        vm.zafiraWebsocket.ws.close = function () { };
        vm.zafiraWebsocket.connect({ withCredentials: false }, function () {
            vm.subscriptions.statistics = subscribeStatisticsTopic();
            vm.subscriptions.testRun = subscribeTestRunsTopic();
            vm.subscriptions[vm.testRun.id] = subscribeTestsTopic(vm.testRun.id);
            vm.subscriptions.launchedTestRuns = subscribeLaunchedTestRuns();
            UtilService.websocketConnected(wsName);
        }, function () {
            UtilService.reconnectWebsocket(wsName, initWebsocket);
        });
    }

    /**
     * Subscribes to launchers soket to store new ones in session storage
     *
     * @returns function to unsubscribe from socket
     */
    function subscribeLaunchedTestRuns() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.launcherRuns`, function (data) {
            const event = getEventFromMessage(data.body);
            const launcher = event.launcher;

            const indexOfLauncher = testsRunsService.getCachedLaunchers()
                .findIndex((cachedLauncher) => cachedLauncher.ciRunId === event.ciRunId);

            if (indexOfLauncher === -1) {
                launcher.status = 'LAUNCHING';
                launcher.ciRunId = event.ciRunId;
                launcher.testSuite = { name: launcher.name };
                testsRunsService.addNewLauncher(launcher);
            }
        });
    }

    function subscribeStatisticsTopic() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.statistics`, function (data) {
            const event = getEventFromMessage(data.body);

            if (!isCurrentTestRunStatistics(event)) {
                return;
            }

            Object.assign(vm.testRun, event.testRunStatistics);
            $scope.$apply();
        });
    }

    function subscribeTestRunsTopic() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.testRuns`, function (data) {
            const event = getEventFromMessage(data.body);
            const testRun = angular.copy(event.testRun);

            if (vm.testRun.id !== +testRun.id) { return; }

            vm.testRun.status = testRun.status;
            vm.testRun.reviewed = testRun.reviewed;
            vm.testRun.elapsed = testRun.elapsed;
            vm.testRun.config = testRun.config;
            vm.testRun.comments = testRun.comments;
            vm.testRun.reviewed = testRun.reviewed;
            $scope.$apply();
        });
    }

    function subscribeTestsTopic() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.testRuns.${vm.testRun.id}.tests`, function (data) {
            const { test } = getEventFromMessage(data.body);

            if (test) {
                const index = getTestsIndexByID(test.id);

                if (index !== -1) {
                    updateTest(test, index);
                } else {
                    addNewTest(test);
                }

                $scope.$apply();
            }
        });
    }

    function onTestSelect() {
        updateBulkLinkingPossibility();
        updateGroupsSelectionStatuses();
    }

    function onBulkClose() {
        clearTestsSelection();
    }

    function onAllTestsSelect() {
        const tests = getActiveViewTests();
        const selectionValue = !tests.some(({ selected}) => selected);

        tests.forEach(test => test.selected = selectionValue);
        onTestSelect();
    }

    function clearTestsSelection() {
        vm.testRun.tests.forEach(test => test.selected = false);
        onTestSelect();
    }

    function getNotificationAvailability() {
        return toolsService.getIfAnyNotificationToolConnected()
            .then((isAvailableAnyNotificationTool) => vm.isNotificationAvailable = isAvailableAnyNotificationTool);
    }

    function getSelectedOrderText() {
        return vm.selectedOrder.isDefault ? '<span class="default-option-placeholder">Sort by</span>' : vm.selectedOrder.name;
    }

    function getDefaultOrderOption() {
        return vm.sortOptions.find(({ isDefault }) => isDefault);
    }

    function initGroupingOptions() {
        vm.groupingOptions = Object.keys(vm.groupingFilters)
            .map((key) => ({
                name: vm.groupingFilters[key].name,
                value: key,
                isDefault: vm.groupingFilters[key].isDefault,
                weight: vm.groupingFilters[key].weight,
            }))
            .sort((a, b) => a.weight - b.weight);
    }

    function getSelectedGroupText() {
        if (vm.testsViewMode === 'plain') {
            return '<span class="default-option-placeholder">Group by</span>';
        }

        return vm.groupingFilters[vm.testsViewMode].name;
    }

    function getActiveViewTests() {
        if (vm.testsViewMode === 'plain') {
            return vm.activeTests;
        }

        return getTestsByGroups(Object.values(vm.groupingFilters[vm.testsViewMode].groupData));
    }

    function hasSelectedGroups() {
        return Object.values(vm.groupingFilters[vm.testsViewMode].groupData).some(({ selected }) => selected);
    }

    function hasSelectedTests() {
        return isSelectedTestPresents(getActiveViewTests());
    }

    function hasUnselectedTests() {
        return isNotSelectedTestPresents(getActiveViewTests());
    }

    function getSelectedTestsCount() {
        return getSelectedTests(getActiveViewTests()).length;
    }

    function groupHasSelectedTests(filterGroup) {
        return isSelectedTestPresents(getTestsByGroups([filterGroup]));
    }

    function groupHasUnselectedTests(filterGroup) {
        return isNotSelectedTestPresents(getTestsByGroups([filterGroup]));
    }

    function isSelectedTestPresents(tests) {
        return tests.some(({ selected }) => selected);
    }

    function isNotSelectedTestPresents(tests) {
        return tests.some(({ selected }) => !selected);
    }

    function getSelectedTests(tests) {
        return tests.filter(({ selected }) => selected);
    }

    function ableToMarkSelectedTests(newStatus) {
        const selectedTests = getSelectedTests(getActiveViewTests());

        if (newStatus === 'PASSED') {
            return !selectedTests.some((test) => !ableToMarkTestAsPassed(test));
        } else if (newStatus === 'FAILED') {
            return !selectedTests.some((test) => !ableToMarkTestAsFailed(test));
        }
    }

    function ableToMarkTestAsFailed(test) {
        const allowedStatuses = ['PASSED', 'SKIPPED', 'ABORTED'];

        return allowedStatuses.includes(test.status);
    }

    function ableToMarkTestAsPassed(test) {
        return test.status !== 'PASSED';
    }

    function ableToLinkIssueToTest(test) {
        return test.status !== 'PASSED' && test.message;
    }

    function openSessionsSidenav(test) {
        if (vm.sessionsInfoActiveTest === test) { return; }

        const sidenavInstance = $mdSidenav('test-sessions-sidenav');

        if (!sidenavInstance) { return; }

        vm.sessionsInfoActiveTest = test;
        sidenavInstance.onClose(() => {
            vm.sessionsInfoActiveTest = null;
        });
        sidenavInstance.open();
    }

    function closeSessionsSidenav() {
        const sidenavInstance = $mdSidenav('test-sessions-sidenav');

        if (sidenavInstance.isOpen()) {
            sidenavInstance.close();
        }
    }

    function onSidenavSwipeRight($event, $target) {
        if (!UtilService.isTouchDevice()) { return; }

        closeSessionsSidenav();
    }
};

export default testDetailsController;

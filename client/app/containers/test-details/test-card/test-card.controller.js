const testCardController = function testsTableController(
    $filter,
    $mdMedia,
    $scope,
    $timeout,
    authService,
    UtilService,
    messageService,
    TestRunService,
    TestService,
) {
    'ngInject';

    const defaultLimitCount = 5;
    const minimumMessageTitleLength = 25;
    const messageLimit = 255;
    const messageLimitMobile = 55;
    const statusCSSClassesMap = {
        IN_PROGRESS: '_info',
        PASSED: '_success',
        FAILED: '_danger',
        SKIPPED: '_warning',
        ABORTED: '_aborted',
    };
    let dataWatchUnsubscriber = angular.noop;
    const vm = {
        artifactsLimitCount: defaultLimitCount,
        labelsLimitCount: defaultLimitCount,
        test: null,
        isHighlighted: false,
        testrailIntegration: null,
        qtestIntegration: null,
        jiraIntegration: null,
        messageLimit: getLessMessageLimit(),
        artifactsLimitLetters: 30,
        startCharts: 25,

        $onInit() {
            prepareStacktrace();
            highlightTest();
            handleLabelsFlag();
            if (vm.isTablet) {
                vm.artifactsLimitLetters = 20;
                vm.startCharts = 15;
            }
            vm.artifacts?.length && prepareArtifacts();
            dataWatchUnsubscriber = $scope.$watch(
                () => vm.expandAllLabels,
                handleLabelsFlag,
            );
        },
        handleLabelsFlag,
        changeTestStatus,
        goToTestInfo,
        openImagesViewerModal,
        openSessionsSidenav,
        selectTest,
        showDetailsDialog,
        showLess,
        showMore,
        toggleStacktraceExpand,
        userHasAnyPermission: authService.userHasAnyPermission,
        copyTestRunTitle,
        prepareArtifacts,

        get artifacts() { return vm.test?.artifactsToShow ?? []; },
        get cssClasses() { return getCSSClasses(); },
        get defaultLimitCount() { return defaultLimitCount; },
        get labels() { return vm.test?.labels ?? []; },
        get messageTitleLengthLimit() { return getLessMessageLimit(); },
        get testDuration() { return getTestDuration(); },
        get isTablet() { return !$mdMedia('gt-md'); },
        get expandAllLabels() { return TestService.expandAllLabels; },
        get canManageTestRuns() { return vm.userHasAnyPermission(['reporting:tests:update']); },
        $onDestroy,
    };

    return vm;

    function $onDestroy() {
        unbindListeners();
    }

    function unbindListeners() {
        dataWatchUnsubscriber();
    }

    function handleLabelsFlag() {
        vm.expandAllLabels ? vm.showMore('labels') : vm.showLess('labels')
    }

    function prepareArtifacts() {
        return vm.artifacts.map((artifact) => {
            const shortName = TestRunService.getShortName(artifact.name, vm.startCharts, vm.artifactsLimitLetters);

            if (shortName !== artifact.name) {
                artifact.shortName = shortName;
                artifact.isVisible = false;
            }

            return artifact;
        });
    }

    function goToTestInfo(e) {
        if (e) {
            e.preventDefault();
        }

        vm.onTestInfoGo({ $testId: vm.test.id });
    }

    function getCSSClasses() {
        if (!vm.test) { return ''; }

        return [
            vm.test.selected ? '_selected' : '',
            vm.test._highlighting ? '_highlighting' : '',
            statusCSSClassesMap[vm.test.status],
        ].filter(Boolean).join(' ');
    }

    function getTestDuration() {
        if (vm.test?.finishTime) {
            const duration = (vm.test.finishTime - vm.test.startTime) / 1000;

            if (duration > 0) {
                return duration;
            }
        }

        return 0;
    }

    function toggleStacktraceExpand(e) {
        if (e) { e.stopPropagation(); }

        vm.test.isExpanded = !vm.test.isExpanded;
    }

    function getLessMessageLimit() {
        return $mdMedia('xs') ? messageLimitMobile : messageLimit;
    }

    function showDetailsDialog(event) {
        vm.onShowDetailsDialog({
            $data: {
                test: vm.test,
                event,
            },
        });
    }

    function changeTestStatus(status) {
        vm.onChangeTestStatus({
            $data: {
                test: vm.test,
                status,
            },
        });
    }

    function openImagesViewerModal(event, artifact) {
        vm.onOpenImagesViewerModal({
            $data: {
                artifact,
                event,
                test: vm.test,
            },
        });
    }

    function selectTest(test) {
        vm.onTestSelect({
            $test: test,
        });
    }

    function showMore(type) {
        if (type === 'artifacts') {
            vm.artifactsLimitCount = vm.artifacts.length;
        } else if (type === 'labels') {
            vm.labelsLimitCount = vm.labels.length;
        }
    }

    function showLess(type) {
        if (type === 'artifacts') {
            vm.artifactsLimitCount = defaultLimitCount;
        } else if (type === 'labels') {
            vm.labelsLimitCount = defaultLimitCount;
        }
    }

    function prepareStacktrace() {
        if (!vm.test.message?.length) { return; }

        vm.test.messageTitle = vm.test.message.split(/\r\n|\n\r|\n|\r/)[0];
        // handles too short lines
        if (vm.test.messageTitle.length < minimumMessageTitleLength) {
            vm.test.messageHtml = getMessageHtml(vm.test.message);
        }
        if (vm.test.messageTitle.length > vm.messageTitleLengthLimit) {
            vm.test.messageHtml = getMessageHtml(vm.test.message);
            vm.test.messageTitle = $filter('cut')(vm.test.messageTitle, false, vm.messageTitleLengthLimit, '...');
        } else {
            vm.test.messageHtml = getMessageHtml(vm.test.message);
            vm.test.messageTitle += '...';
        }
    }

    function getMessageHtml(message) {
        return message
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/ *(\r?\n|\r)/g, '<br/>')
            .replace(/\s/g, '&nbsp;');
    }

    function highlightTest() {
        if (vm.test.id !== vm.highlightedTestId) { return; }

        requestAnimationFrame(() => {
            const el = document.getElementById('test_' + vm.test.id);

            if (el && !UtilService.isElementInViewport(el)) {
                const scrollableParentElement = document.querySelector('.page-wrapper');
                const appHeader = document.querySelector('.app-header').offsetHeight;
                const testRunHeader = document.querySelector('.p-tests-run-details__sticky-header').offsetHeight;
                const headerOffset = testRunHeader + appHeader;
                const elOffsetTop = $(el).offset().top;

                $(scrollableParentElement).animate(
                    { scrollTop: elOffsetTop - headerOffset },
                    'fast',
                    null,
                    () => {
                        $scope.$apply(handleHighlightState);
                    }
                );
            } else {
                handleHighlightState();
            }
        });
    }

    function handleHighlightState() {
        // reset this ID to prevent highlighting on filtering/grouping
        vm.highlightedTestId = null;
        // animating (2sec is the animation duration)
        vm.test._highlighting = true;
        $timeout(() => {
            Reflect.deleteProperty(vm.test, '_highlighting');
        }, 2000);
    }

    function copyTestRunTitle() {
        if (vm.test?.name) {
            vm.test.name.copyToClipboard();
            messageService.success('The title was copied to the clipboard.');
        }
    }

    function openSessionsSidenav() {
        vm.onTestSessionsOpen({ $test: vm.test });
    }
}

export default testCardController;

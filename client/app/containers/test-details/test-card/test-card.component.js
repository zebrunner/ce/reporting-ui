import template from './test-card.html';
import controller from './test-card.controller';
import './test-card.scss';

const testCardComponent = {
    template,
    controller,
    bindings: {
        highlightedTestId: '=',
        jiraIntegration: '<',
        isIntegrationInitialized: '<',
        qtestIntegration: '<',
        onChangeTestStatus: '&',
        onOpenImagesViewerModal: '&',
        onShowDetailsDialog: '&',
        onTestSessionsOpen: '&',
        onTestInfoGo: '&',
        onTestSelect: '&',
        test: '<',
        testrailIntegration: '<',
    },
};

export default testCardComponent;

import template from './saved-searches.html';
import controller from './saved-searches.controller';
import './saved-searches.scss';

const savedSearchesComponent = {
    template,
    controller,
    bindings: {
        onClose: '&',
        onSearchApply: '&',
        onSearchSave: '&',
        onSearchDelete: '&',
        onSearchUpdate: '&',
        searchesList: '<',
        paramsToSave: '=',
    },
};

export default savedSearchesComponent;

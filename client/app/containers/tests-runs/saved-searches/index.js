import savedSearchesComponent from './saved-searches.component';
import SavedSearchesService from './saved-searches.service';

export const SavedFiltersModule = angular.module('savedFilters', [])

    .component('savedSearches', savedSearchesComponent)
    .service({ SavedSearchesService })

    .name;

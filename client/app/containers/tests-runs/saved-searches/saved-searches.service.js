export default function SavedSearchesService(
    $httpMock,
    UtilService,
) {
    'ngInject';

    const arrayTypeSearchParams = [
        'browser',
        'environment',
        'locale',
        'platform',
        'status',
    ];

    return {
        deleteSavedSearch,
        getSavedSearches,
        saveSearch,
        updateSavedSearch,
        formatParamsToSave,
        normalizeSearchParams,
    };

    function getSavedSearches() {
        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/test-run-filters`)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to fetch saved searches'));
    }

    function saveSearch(searchTemplate) {
        return $httpMock.post(`${$httpMock.apiHost}/api/reporting/v1/test-run-filters`, searchTemplate)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to save search'));
    }

    function deleteSavedSearch(savedSearchID) {
        return $httpMock.delete(`${$httpMock.apiHost}/api/reporting/v1/test-run-filters/${savedSearchID}`)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to delete search'));
    }

    function updateSavedSearch(savedSearchID, params) {
        return $httpMock.patch(`${$httpMock.apiHost}/api/reporting/v1/test-run-filters/${savedSearchID}`, params)
            .then(UtilService.handleSuccess, UtilService.handleError('Unable to update search'));
    }

    function formatParamsToSave(searchParams) {
        return Object.entries(searchParams).reduce((accum, [key, value]) => {
            accum[key] = Array.isArray(value) ? value : [value];

            return accum;
        }, {});
    }

    function normalizeSearchParams(savedSearchParams) {
        return Object.entries(savedSearchParams).reduce((accum, [key, value]) => {
            accum[key] = arrayTypeSearchParams.includes(key) ? value : value[0];

            return accum;
        }, {});
    }
};

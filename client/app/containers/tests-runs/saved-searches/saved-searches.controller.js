export default function SavedSearchesController(
    $timeout,
    authService,
    messageService,
    SavedSearchesService,
    UserService,
) {
    'ngInject';

    const emptySearchTemplate = {
        name: '',
        isPrivate: true,
    };

    const vm = {
        isSearchSaving: false,
        paramsToSave: null,
        searchesFilterQuery: '',
        searchesList: [],
        searchTemplate: { ...emptySearchTemplate },

        applySavedSearch,
        canChangePrivacy,
        canDeleteItem,
        changeSearchPrivacy,
        deleteSavedSearch,
        isCurrentUserOwner,
        saveSearch,
        onSaveClear,
        onToggleFavoriteStatus,
        toggleInfoPopup,
        userHasAnyPermission: authService.userHasAnyPermission,
    };

    return vm;

    function saveSearch() {
        const searchTemplate = {
            ...vm.searchTemplate,
            items: SavedSearchesService.formatParamsToSave(vm.paramsToSave),
        };

        vm.isSearchSaving = true;
        SavedSearchesService.saveSearch(searchTemplate)
            .then(({ success, data, message }) => {
                if (success) {
                    setSavedSearchAsActive(data);
                    vm.onSearchSave({
                        $searchTemplate: data,
                    });
                    messageService.success(`"${searchTemplate.name}" filter was saved successfully`);
                    onSaveClear();
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => vm.isSearchSaving = false);
    }

    function onSaveClear() {
        vm.paramsToSave = null;
        vm.searchTemplate = { ...emptySearchTemplate };
    }

    function onToggleFavoriteStatus(searchItem) {
        if (searchItem.isUpdating) { return; }
        const isFavorite = !searchItem.isFavorite;
        const params = [{
            op: 'replace',
            path: '/isFavorite',
            value: isFavorite,
        }];

        searchItem.isUpdating = true;
        SavedSearchesService.updateSavedSearch(searchItem.id, params)
            .then(({ success, message }) => {
                if (success) {
                    searchItem.isFavorite = isFavorite;
                    vm.onSearchUpdate();
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => searchItem.isUpdating = false);
    }

    function deleteSavedSearch(searchItem) {
        if (searchItem.isUpdating) { return; }

        searchItem.isUpdating = true;
        SavedSearchesService.deleteSavedSearch(searchItem.id)
            .then(({ success, message }) => {
                if (success) {
                    vm.onSearchDelete({
                        $searchId: searchItem.id,
                    });
                    messageService.success(`"${searchItem.name}" filter was deleted successfully`);
                } else {
                    if (message) {
                        messageService.error(message);
                    }
                    searchItem.isUpdating = false;
                }
            });
    }

    function changeSearchPrivacy(searchItem) {
        if (searchItem.isUpdating) { return; }
        const isPrivate = !searchItem.isPrivate;
        const params = [{
            op: 'replace',
            path: '/isPrivate',
            value: isPrivate,
        }];

        searchItem.isUpdating = true;
        SavedSearchesService.updateSavedSearch(searchItem.id, params)
            .then(({ success, message }) => {
                if (success) {
                    searchItem.isPrivate = isPrivate;
                } else if (message) {
                    messageService.error(message);
                }
            })
            .finally(() => searchItem.isUpdating = false);
    }

    function isCurrentUserOwner(searchItem) {
        return UserService.currentUser.username === searchItem.owner;
    }

    function canDeleteItem(searchItem) {
        return isCurrentUserOwner(searchItem) || vm.userHasAnyPermission(['reporting:filters:delete-any']);
    }

    function canChangePrivacy(searchItem) {
        return isCurrentUserOwner(searchItem) || vm.userHasAnyPermission(['reporting:filters:create-public']);
    }

    function applySavedSearch(savedSearch) {
        if (savedSearch.isActive) { return; }

        setSavedSearchAsActive(savedSearch);
        vm.onSearchApply({
            $searchTemplate: savedSearch,
        });
    }

    function setSavedSearchAsActive(savedSearch) {
        vm.searchesList.forEach((searchItem) => searchItem.isActive = false);
        savedSearch.isActive = true;
    }

    function toggleInfoPopup(event, savedSearch, value) {
        savedSearch.isInfoVisible = value;
    }
}

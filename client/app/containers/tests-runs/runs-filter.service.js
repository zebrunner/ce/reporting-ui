export default function RunsFilterService(
    $q,
    messageService,
    TestRunService,
) {
    'ngInject';

    const local = {
        controlsOptions: {
            status: ['PASSED', 'FAILED', 'SKIPPED', 'ABORTED', 'IN_PROGRESS', 'UNKNOWN'],
        },
    }
    const paramsForSaveSkip = ['page', 'pageSize', 'filterID'];
    const searchControlsTemplate = [
        {
            id: 'searchBrowser',
            isDefault: true,
            isVisible: true,
            options: [],
            key: 'browser',
            label: 'Browser',
            type: 'select',
        },
        {
            id: 'searchPlatform',
            isDefault: true,
            isVisible: true,
            options: [],
            key: 'platform',
            label: 'Platform',
            type: 'select',
        },
        {
            id: 'searchStatus',
            isDefault: false,
            isVisible: false,
            options: [],
            key: 'status',
            label: 'Status',
            type: 'select',
        },
        {
            id: 'searchEnv',
            isDefault: false,
            isVisible: false,
            options: [],
            key: 'environment',
            label: 'Environment',
            type: 'select',
        },
        {
            id: 'searchLocales',
            isDefault: false,
            isVisible: false,
            options: [],
            key: 'locale',
            label: 'Locale',
            type: 'select',
        },
        {
            id: 'searchCalendar',
            isDefault: false,
            isVisible: false,
            key: 'datePicker',
            label: 'Date',
            type: 'datePicker',
        },
        {
            id: 'searchReviewed',
            isDefault: false,
            isVisible: false,
            key: 'reviewed',
            label: 'Reviewed',
            type: 'button',
            onClick: angular.noop,
        },
    ];

    const service =  {
        isInitialized: false,

        getControlOptions,
        initServiceData,
        isSearchActive,
        setControlOptions,

        get ignoredParams() { return paramsForSaveSkip; },
        get searchControlsTemplate() { return searchControlsTemplate; },
    };

    return service;

    function getControlOptions(key) {
        return local.controlsOptions[key];
    }

    function setControlOptions(key, options) {
        return local.controlsOptions[key] = options;
    }

    function isSearchActive(searchParams) {
        return searchParams && !!Object.keys(searchParams)
            .filter((key) => !service.ignoredParams.includes(key))
            .filter((key) => searchParams[key] && (!Array.isArray(searchParams[key]) || searchParams[key].length))
            .length;
    }

    function loadEnvironments() {
        return TestRunService.getEnvironments()
            .then(handleResponse)
            .then((options) => service.setControlOptions('environment', options));
    }

    function loadPlatforms() {
        return TestRunService.getPlatforms()
            .then(handleResponse)
            .then((options) => service.setControlOptions('platform', options));
    }

    function loadBrowsers() {
        return TestRunService.getBrowsers()
            .then(handleResponse)
            .then((options) => service.setControlOptions('browser', options));
    }

    function loadLocales() {
        return TestRunService.getLocales()
            .then(handleResponse)
            .then((options) => service.setControlOptions('locale', options));
    }

    function initServiceData() {
        return $q
            .all([
                loadBrowsers(),
                loadEnvironments(),
                loadLocales(),
                loadPlatforms(),
            ])
            .finally(() => service.isInitialized = true);
    }

    function handleResponse({ data = [], message }) {
        if (message) {
            messageService.error(message);
        }

        // TODO: remove filtering when BE get rid of nullish values from DB
        return data.filter(Boolean);
    }
};

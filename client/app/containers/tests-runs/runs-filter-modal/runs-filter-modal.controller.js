export default function RunsFilterModalController(
    $mdDateRangePicker,
    $mdDialog,
    $timeout,
    activeSearchParams,
    authService,
    DEFAULT_SC,
    onSearch,
    RunsFilterService,
    saveSearch,
    UtilService,
) {
    'ngInject';

    const dateSearchKeys = ['date', 'fromDate', 'toDate'];
    const cleanDateRangeData = {
        selectedTemplate: null,
        selectedTemplateName: null,
        dateStart: null,
        dateEnd: null,
        showTemplate: false,
        fullscreen: false,
    };

    const vm = {
        activeSearchParams: { ...DEFAULT_SC },
        debounceDelay: 700,
        isSearchActive: false,
        isSearchChanged: false,
        searchControls: [],
        searchParams: {},
        selectedRange: { ...cleanDateRangeData },

        get hidingSearchControls() { return vm.searchControls.filter(({ isDefault }) => !isDefault); },
        get isDateSearchActive() { return vm.selectedRange?.selectedTemplateName; },
        get visibleSearchControls() { return vm.searchControls.filter(({ isDefault, isVisible }) => isDefault || isVisible); },

        $onInit() {
            vm.activeSearchParams = { ...activeSearchParams };
            initControls();
            initSearchParamsState();
        },
        cancel,
        clearAllControls,
        clearControl,
        getSelectControlText,
        hideControl,
        hide,
        isControlActive,
        onSearch,
        onFilterChange,
        onSearchSave,
        onSearchApply,
        resetSearch,
        saveSearch,
        showDatePickerDialog,
        userHasAnyPermission: authService.userHasAnyPermission,
    };

    return vm;

    function initControls() {
        vm.searchControls = RunsFilterService.searchControlsTemplate.map((controlTemplate) => {
            const control = { ...controlTemplate };

            if (control.type === 'select') {
                control.options = RunsFilterService.getControlOptions(control.key) || [];
            } else if (control.type === 'button' && control.id === 'searchReviewed') {
                control.onClick = () => {
                    vm.searchParams[control.key] = !vm.searchParams[control.key];
                    onFilterChange();
                };
            }

            return control;
        });
    }

    //apply activeSearchParams to local ones
    function initSearchParamsState() {
        const searchParams = {};
        let hasIrrelevantValues = false;

        if (vm.activeSearchParams) {
            let hasDateParams = false;

            Object.keys(vm.activeSearchParams)
                .forEach((key) => {
                    // ignore false values
                    if (vm.activeSearchParams[key]) {
                        const control = geSearchControlByKey(key);
                        // handle Array values, ignore empty ones
                        if (Array.isArray(vm.activeSearchParams[key])) {
                            if (vm.activeSearchParams[key].length && control) {
                                // collect only relevant values
                                const values = vm.activeSearchParams[key]
                                    .filter((value) => control.options.find((optionValue) => optionValue === value));

                                hasIrrelevantValues = hasIrrelevantValues || values.length !== vm.activeSearchParams[key].length;
                                searchParams[key] = values;
                                //update control's visibility
                                if (!control.isDefault) {
                                    control.isVisible = true;
                                }
                            }
                        } else {
                            if (dateSearchKeys.includes(key)) {
                                !hasDateParams && (hasDateParams = true);
                                fillDateInitialParams({ [key]: vm.activeSearchParams[key] });
                            }
                            searchParams[key] = vm.activeSearchParams[key];
                            if (control && !control.isDefault) {
                                control.isVisible = true;
                            }
                        }
                    }
                });
            // updates selectedTemplateName
            if (hasDateParams) {
                const control = geSearchControlByKey('datePicker');

                control.isVisible = true;
                vm.selectedRange = UtilService.handleDateFilter(vm.selectedRange).selectedRange;
            }
        }

        vm.searchParams = searchParams;
        //force update search after clearing irrelevant values
        if (hasIrrelevantValues) {
            onChangeSearchCriteria(true);
        } else {
            updateSearchState();
        }
    }

    function fillDateInitialParams({  date, fromDate, toDate }) {
        date && (fromDate = toDate = date);
        fromDate && (vm.selectedRange.dateStart = new Date(fromDate));
        toDate && (vm.selectedRange.dateEnd = new Date(toDate));
    }

    function geSearchControlByKey(key) {
        return vm.searchControls.find((control) => control.key === key);
    }

    function getSelectControlText(control) {
        if (!vm.searchParams[control.key]?.length) {
            return control.label;
        }

        return `${control.label}: ${getSelectionString(vm.searchParams[control.key])}`;
    }

    function getSelectionString(values) {
        return values.map((value) => `<strong>${value}</strong>`).join(', ');
    }

    function clearControl(control) {
        if (!control)  { return; }

        if (control.key === 'datePicker') {
            vm.selectedRange = { ...cleanDateRangeData };
            dateSearchKeys.forEach((key) => {
                vm.searchParams[key] = null;
            });

            return;
        }

        vm.searchParams[control.key] = null;
    }

    function hideControl(control, skipSearchUpdate) {
        clearControl(control);
        control.isVisible = control.isDefault;

        if (!skipSearchUpdate) {
            onChangeSearchCriteria();
        }
    }

    function clearAllControls() {
        vm.hidingSearchControls.forEach((control) => hideControl(control, true));
        onChangeSearchCriteria();
    }

    function isControlActive(control) {
        switch (control.type) {
            case 'select':
                return vm.searchParams[control.key]?.length;
            case 'datePicker':
                return vm.selectedRange.selectedTemplateName;
            case 'button':
                return vm.searchParams[control.key];
        }
    }

    function onChangeSearchCriteria(skipPageReset) {
        const filteredParams = getFilteredSearchParams(vm.searchParams);
        const formattedParams = getFormattedSearchParams(filteredParams);

        // check if changed before trying to send event
        if (!isParamsChanged(vm.activeSearchParams, formattedParams)) { return; }

        // reset page on search
        if (!skipPageReset) {
            formattedParams.page = 1;
        }

        updateSearchState();
        vm.onSearch(formattedParams);
    }

    // filters nullish values and empty arrays
    function getFilteredSearchParams(searchParams) {
        return Object.keys(searchParams)
            .reduce((accum, key) => {
                if (searchParams[key] && !(Array.isArray(searchParams[key]) && !searchParams[key].length)) {
                    accum[key] = searchParams[key];
                }

                return accum;
            }, {});
    }

    // returns formatted values if need
    function getFormattedSearchParams(searchParams) {
        return Object.keys(searchParams)
            .reduce((accum, key) => {
                if (dateSearchKeys.includes(key) && typeof searchParams[key] !== 'string') {
                    // format Dates into ISO strings
                    accum[key] = searchParams[key].toISOString();
                } else {
                    accum[key] = searchParams[key];
                }

                return accum;
            }, {});
    }

    //compare applied activeSearchParams with local searchParams
    function isParamsChanged(params1, params2) {
        return !angular.equals(params1, params2);
    }

    function updateSearchState() {
        const filteredParams = getFilteredSearchParams(vm.searchParams);
        const formattedParams = getFormattedSearchParams(filteredParams);

        vm.isSearchActive = RunsFilterService.isSearchActive(vm.searchParams);
        vm.isSearchChanged = isParamsChanged(vm.activeSearchParams, formattedParams);
    }

    function resetSearch() {
        vm.searchControls.forEach((control) => hideControl(control, true));
        vm.searchParams = { ...DEFAULT_SC };
        updateSearchState()
    }

    function onSearchSave() {
        onChangeSearchCriteria();
        hide();
        $timeout(() => {
            saveSearch();
        }, 0);
    }

    function onSearchApply() {
        onChangeSearchCriteria();
        hide();
    }

    function onFilterChange() {
        updateSearchState();
    }

    function showDatePickerDialog($event, showTemplate) {
        vm.selectedRange.showTemplate = showTemplate;

        $mdDateRangePicker
            .show({
                targetEvent: $event,
                model: vm.selectedRange,
                multiple: true,
            })
            .then((result) => {
                if (result) {
                    const res = UtilService.handleDateFilter(result);
                    const newSearchParams = { ...vm.searchParams, ...res.searchParams };

                    if ((!res.selectedRange.selectedTemplateName && vm.selectedRange.selectedTemplateName) || (res.selectedRange.selectedTemplateName && !angular.equals(vm.searchParams, newSearchParams))) {
                        vm.searchParams = newSearchParams;
                    }

                    onFilterChange();
                    vm.selectedRange = res.selectedRange;
                }
            })
            .catch(() => null);
    }

    function hide() {
        $mdDialog.hide();
    }

    function cancel() {
        $mdDialog.cancel();
    }
}

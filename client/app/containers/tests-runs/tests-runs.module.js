'use strict';

import testsRunsComponent from './tests-runs.component';
import { RunsFilterModule } from './runs-filter';
import { RunsFilterMobileModule } from './runs-filter-mobile';
import { SavedFiltersModule } from './saved-searches'
import RunsFilterService from './runs-filter.service';

import playArrow from '../../../assets/images/play_arrow-black.svg';

import './test-runs.scss';
import './runs-filter-modal/runs-filter-modal.scss';

export const testsRunsModule = angular.module('app.testsRuns', [
    RunsFilterModule,
    RunsFilterMobileModule,
    SavedFiltersModule,
    'ui.ace',
])
    .service({ RunsFilterService })
    .component({ testsRunsComponent })
    .config(function ($mdIconProvider) {
        'ngInject';

        $mdIconProvider
            .icon('play_arrow-outline', playArrow);
    });

import runsFilterMobileComponent from './runs-filter-mobile.component';

export const RunsFilterMobileModule = angular.module('runsFilterMobile', [])

    .component('runsFilterMobile', runsFilterMobileComponent)

    .name;

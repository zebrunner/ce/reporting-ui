export default function RunsFilterMobileController(
    DEFAULT_SC,
    RunsFilterService,
) {
    'ngInject';

    const vm = {
        searchesList: [],

        get isSearchActive() {
            return RunsFilterService.isSearchActive(vm.activeSearchParams);
        },

        toggleSavedSearch,
        toggleSavedSearchesSidenav,
    };

    return vm;

    function toggleSavedSearch(savedSearch) {
        if (savedSearch.isActive) {
            vm.onSearch({ $params: { ...DEFAULT_SC } });
            vm.onSearchReset();
        } else {
            setSavedSearchAsActive(savedSearch);
            vm.onSearchApply({
                $searchTemplate: savedSearch,
            });
        }
    }

    function setSavedSearchAsActive(savedSearch) {
        vm.searchesList.forEach((searchItem) => searchItem.isActive = false);
        savedSearch.isActive = true;
    }

    function toggleSavedSearchesSidenav() {
        vm.onSavedSearchesClick();
    }
}

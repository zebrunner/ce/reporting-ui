import template from './runs-filter-mobile.html';
import controller from './runs-filter-mobile.controller';
import './runs-filter-mobile.scss';

const runsFilterMobileComponent = {
    template,
    controller,
    transclude: true,
    bindings: {
        activeSearchParams: '<',
        onFilterCreate: '&',
        onSavedSearchesClick: '&',
        onSearch: '&',
        onSearchReset: '&',
        onSearchApply: '&',
        searchesList: '<',
    },
};

export default runsFilterMobileComponent;

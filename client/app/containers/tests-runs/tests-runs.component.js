import template from './tests-runs.html';
import controller from './tests-runs.controller';

const testsRunsComponent = {
    template,
    controller,
    bindings: {
        resolvedData: '<',
        activeTestRunId: '<',
    },
};

export default testsRunsComponent;

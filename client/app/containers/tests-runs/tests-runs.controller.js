import runsFilterModalController from './runs-filter-modal/runs-filter-modal.controller';
import runsFilterModalTemplate from './runs-filter-modal/runs-filter-modal.html';

const testsRunsController = function testsRunsController(
    $httpMock,
    $mdDialog,
    $mdMedia,
    $mdSidenav,
    $q,
    $scope,
    $state,
    $stateParams,
    $timeout,
    TestRunService,
    UtilService,
    UserService,
    testsRunsService,
    toolsService,
    messageService,
    pageTitleService,
    authService,
    SavedSearchesService,
    DEFAULT_SC,
    RunsFilterService,
) {
    'ngInject';

    let scrollTickingTimeout = null;
    const scrollableParentElement = document.querySelector('.page-wrapper');

    const vm = {
        savedSearches: [],
        savingSearch: false,
        searchParams: {},
        searchParamsToSave: null,
        testRuns: [],
        launchers: [],
        totalResults: 0,
        pageSize: 0,
        currentPage: 0,
        selectedTestRuns: [],
        zafiraWebsocket: null,
        subscriptions: {},
        activeTestRunId: null,
        scrollTicking: false,
        switcherState: 'runs',
        isUserParamSaving: false,
        isNotificationAvailable: false,
        tenant: authService.tenant || 'zafira',
        isFetchingTestsRuns: false,

        $onInit,
        clearTestRunsSelection,
        showCompareDialog,
        batchRerun,
        batchDelete,
        abortSelectedTestRuns,
        batchEmail,
        deleteSingleTestRun,
        selectAllTestRuns,
        selectTestRun,
        showMobileFiltersDialog,
        onPageChange,
        onSearchParamsChange,
        saveSearch,
        showSavedSearches,
        hideSavedSearches,
        onSavedSearchAdd,
        onSavedSearchApply,
        onSavedSearchDelete,
        onSavedSearchUpdate,
        userHasAnyPermission: authService.userHasAnyPermission,

        get isAbleAbortTestRuns() { return isAbleAbortTestRuns(); },
        get isAbleCompareTestRuns() { return isAbleCompareTestRuns(); },
        get isAbleRebuildTestRuns() { return isAbleRebuildTestRuns(); },
        get currentTitle() { return pageTitleService.pageTitle; },
        get isMobile() { return $mdMedia('xs'); },
        get isTablet() { return !$mdMedia('gt-md'); },
        get isSearchActive() { return RunsFilterService.isSearchActive(vm.searchParams); },
        get isTestRunsEmpty() { return !vm.testRuns?.length;},
    };

    return vm;

    function $onInit() {
        vm.testRuns = vm.resolvedData.results || [];
        vm.testRuns.forEach(testsRunsService.preparePlatformsData);
        initSearchParams();
        vm.totalResults = vm.resolvedData.totalResults || 0;
        vm.pageSize = vm.searchParams.pageSize;
        vm.currentPage = vm.searchParams.page;
        vm.activeTestRunId = $stateParams.activeTestRunId;
        vm.fromWizard = $stateParams.fromWizard;

        initLaunchers();
        initSavedSearches();
        getNotificationAvailability();
        setTimersOnDestroyingLaunchers();
        initWebsocket();
        bindEvents();
        vm.activeTestRunId && highlightTestRun();
        bindEventListeners();
    }

    function initSearchParams() {
        vm.searchParams = {
            ...testsRunsService.defaultSearchParams,
            ...testsRunsService.prepareSearchParams($stateParams),
        }
    }

    function initLaunchers() {
        const launchers = testsRunsService.getCachedLaunchers();

        if (launchers && launchers.length) {
            vm.launchers = launchers.reduce((filteredLaunchers, launcher) => {
                if (vm.testRuns.find(testRun => testRun.ciRunId === launcher.ciRunId)) {
                    filteredLaunchers = testsRunsService.deleteLauncherFromCache(launcher.ciRunId);
                }

                return filteredLaunchers;
            }, launchers);
        }
    }

    function initSavedSearches() {
        SavedSearchesService.getSavedSearches()
            .then(({ success, data }) => {
                if (success) {
                    vm.savedSearches = data || [];
                    updateActiveSavedSearch();
                }
            });
    }

    function addOnSidenavClose(sidenavInstance) {
        sidenavInstance.onClose(() => {
            if (vm.searchParamsToSave) {
                vm.searchParamsToSave = null;
            }
        });
    }

    function saveSearch() {
        const sidenavInstance = $mdSidenav('saved-searches-sidenav');

        if (!sidenavInstance) { return; }

        const paramsToSave = { ...vm.searchParams };

        RunsFilterService.ignoredParams.forEach((key) => Reflect.deleteProperty(paramsToSave, key));
        vm.searchParamsToSave = paramsToSave;
        addOnSidenavClose(sidenavInstance);
        sidenavInstance
            .open()
            .then(() => {
                $timeout(() => {
                    const inputElem = document.querySelector('.saved-searches__item-input');

                    if (inputElem) {
                        inputElem.focus();
                    }
                });
            });
    }

    // reset active saved search
    function resetActiveSavedSearch() {
        const activeSavedSearch = vm.savedSearches.find(({ isActive }) => isActive);

        if (activeSavedSearch) {
            activeSavedSearch.isActive = false;
        }
    }

    function showSavedSearches() {
        const sidenavInstance = $mdSidenav('saved-searches-sidenav');

        if (!sidenavInstance) { return; }

        addOnSidenavClose(sidenavInstance);
        sidenavInstance.open();
    }

    function hideSavedSearches() {
        const sidenavInstance = $mdSidenav('saved-searches-sidenav');

        if (!sidenavInstance) { return; }

        sidenavInstance.close();
    }

    function onSavedSearchAdd(savedSearch) {
        const newSavedSearches = [
            savedSearch,
            ...vm.savedSearches,
        ];
        const searchParams = {
            ...vm.searchParams,
            filterID: savedSearch.id,
            ...SavedSearchesService.normalizeSearchParams(savedSearch.items),
        };

        vm.savedSearches = getSortedSavedSearches(newSavedSearches);
        onSearchParamsChange(searchParams);
    }

    function onSavedSearchApply(savedSearch) {
        const searchParams = {
            filterID: savedSearch.id,
            ...SavedSearchesService.normalizeSearchParams(savedSearch.items),
            ...DEFAULT_SC,
        };

        hideSavedSearches();
        onSearchParamsChange(searchParams);
    }

    function onSavedSearchDelete(savedSearchID) {
        vm.savedSearches = vm.savedSearches.filter(({ id }) => id !== savedSearchID);

        if ($stateParams.filterID && $stateParams.filterID === savedSearchID) {
            const searchParams = { ...vm.searchParams };

            Reflect.deleteProperty(searchParams, 'filterID');
            onSearchParamsChange(searchParams);
        }
    }

    function onSavedSearchUpdate() {
        vm.savedSearches = getSortedSavedSearches(vm.savedSearches);
    }

    function getSortedSavedSearches(savedSearches) {
        const favoriteSavedSearches = savedSearches.filter(({ isFavorite }) => isFavorite)
            .sort(savedSearchesSort);
        const restSavedSearches = savedSearches.filter(({ isFavorite }) => !isFavorite)
            .sort(savedSearchesSort);

        return [ ...favoriteSavedSearches, ...restSavedSearches ];
    }

    function savedSearchesSort(a, b) {
        return b.createdAt - a.createdAt;
    }

    function updateActiveSavedSearch(filterID) {
        if (!filterID && $stateParams.filterID) {
            filterID = $stateParams.filterID;
        }
        if (filterID) {
            const activeSavedSearch = vm.savedSearches.find(({ id }) => id === filterID);

            if (activeSavedSearch && isActiveSavedSearchApplied(activeSavedSearch)) {
                activeSavedSearch.isActive = true;
            } else {
                const searchParams = { ...vm.searchParams, page: vm.currentPage };

                if (activeSavedSearch) {
                    activeSavedSearch.isActive = false;
                }
                Reflect.deleteProperty(searchParams, 'filterID');
                onSearchParamsChange(searchParams);
            }
        } else {
            resetActiveSavedSearch();
        }
    }

    function isActiveSavedSearchApplied(savedSearch) {
        const currentParams = { ...vm.searchParams };

        RunsFilterService.ignoredParams.forEach((key) => Reflect.deleteProperty(currentParams, key));

        return angular.equals(SavedSearchesService.normalizeSearchParams(savedSearch.items), currentParams)
    }

    function showMobileFiltersDialog(event) {
        $mdDialog.show({
            controller: runsFilterModalController,
            controllerAs: '$ctrl',
            template: runsFilterModalTemplate,
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            multiple: true,
            locals: {
                activeSearchParams: vm.searchParams,
                saveSearch: vm.saveSearch,
                onSavedSearchesClick: vm.showSavedSearches,
                onSearch: (searchParams) => {
                    vm.onSearchParamsChange(searchParams);
                },
            },
        });
    }

    function setTimersOnDestroyingLaunchers() {
        vm.launchers.forEach((launcher) => {
            setTimerOnDestroingLauncher(launcher);
        })
    }

    function setTimerOnDestroingLauncher(launcher) {
        let dateNow = new Date();
        let timeDiff = launcher.shouldBeDestroyedAt - dateNow.getTime();

        if (timeDiff > 0) {
            launcher.timeout = setTimeout(function() {
                vm.launchers = testsRunsService.deleteLauncherFromCache(launcher.ciRunId);
                $scope.$apply();
            }, timeDiff)
        }
    }

    //TODO: move to the card (see test details)?
    function highlightTestRun() {
        const activeTestRun = getTestRunById(vm.activeTestRunId);

        if (activeTestRun) {
            $timeout(function() {
                requestAnimationFrame(() => {
                    const el = document.getElementById(`testRun_${vm.activeTestRunId}`);

                    if (el && !UtilService.isElementInViewport(el)) {
                        const appHeaderHeight = document.querySelector('.app-header').offsetHeight;
                        const mobileFilterHeight = vm.isMobile && document.querySelector('.runs-filter-mobile').offsetHeight;
                        const headerOffset = appHeaderHeight + mobileFilterHeight;
                        const elOffsetTop = $(el).offset().top;

                        $(scrollableParentElement).animate(
                            { scrollTop: elOffsetTop - headerOffset },
                            'fast',
                            null,
                            () => {
                                $scope.$apply(() => activeTestRun.highlighting = true);
                            }
                        );
                    } else {
                        activeTestRun.highlighting = true;
                    }

                    $timeout(function() {
                        delete activeTestRun.highlighting;
                    }, 4000); //4000 - highlighting animation duration in CSS
                });
            }, 500); // wait for content is rendered (maybe need to be increased if scroll position is incorrect)
        }
    }

    function onPageChange() {
        onSearchParamsChange({ ...vm.searchParams, page: vm.currentPage });
    }

    function searchTestRuns() {
        vm.selectedAll = false;
        vm.isFetchingTestsRuns = true;

        return testsRunsService.getTestRuns(vm.searchParams)
            .then((response) => {
                vm.totalResults = response.totalResults;
                vm.testRuns = response.results || [];
                vm.testRuns.forEach(testsRunsService.preparePlatformsData);
                vm.launchers = testsRunsService.getCachedLaunchers();
                $timeout(() => {
                    scrollableParentElement.scrollTop = 0;
                }, 0, false);

                return $q.resolve(vm.testRuns);
            })
            .catch(() => {
                vm.totalResults = 0;
                vm.testRuns = [];
            })
            .finally(() => {
                vm.isFetchingTestsRuns = false;
            });
    }

    function areTestRunsFromOneSuite() {
        let firstItem = vm.selectedTestRuns[0];

        return vm.selectedTestRuns.every(testRun => testRun.testSuite.id === firstItem.testSuite.id);
    }

    function showCompareDialog(event) {
        $mdDialog.show({
            controller: 'CompareController',
            template: require('../../components/modals/compare/compare.html'),
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose:true,
            fullscreen: true,
            locals: {
                selectedTestRuns: vm.selectedTestRuns
            }
        });
    }

    function batchRerun() {
        const rerunFailures = confirm('Would you like to rerun only failures, otherwise all the tests will be restarted?');
        const resultsCounter = {success: 0, fail: 0};
        const promises = vm.testRuns
            .filter(testRun => testRun.selected)
            .map(function(testRun) {
                return rebuild(testRun, rerunFailures, resultsCounter);
            });

        $q.all(promises)
            .finally(() => {
                showBulkOperationMessages({
                    action: 'rerun',
                    succeeded: resultsCounter.success,
                    failed: resultsCounter.fail,
                    total: vm.selectedTestRuns.length,
                });
                postBulkActionHandler(resultsCounter);
            });
    }

    function rebuild(testRun, rerunFailures, resultsCounter) {
        // TODO: refactor this after jenkins integration finished
        // if (vm.isToolConnected('JENKINS')) {
            if (typeof rerunFailures === 'undefined') {
                rerunFailures = confirm('Would you like to rerun only failures, otherwise all the tests will be restarted?');
            }

            return TestRunService.rerunTestRun(testRun.id, rerunFailures).then(function(rs) {
                if (rs.success) {
                    testRun.status = 'IN_PROGRESS';
                    if (resultsCounter) {
                        testRun.selected && (testRun.selected = false);
                        resultsCounter.success += 1;
                    } else {
                        messageService.success('Rebuild triggered in CI service');
                    }
                } else {
                    if (resultsCounter) {
                        resultsCounter.fail += 1;
                    } else {
                        messageService.error(rs.message);
                    }
                }
            });
        // } else {
        //     if (testRun.jenkinsURL) {
        //         resultsCounter && (resultsCounter.success += 1);
        //         window.open(testRun.jenkinsURL + '/rebuild/parameterized', '_blank');
        //
        //         return $q.resolve();
        //     } else {
        //         resultsCounter && (resultsCounter.fail += 1);
        //
        //         return $q.resolve();
        //     }
        // }
    }

    function batchDelete() {//TODO: why we don't use confirmation in this case?
        const selectedCount = vm.selectedTestRuns.length;
        const resultsCounter = {success: 0, fail: 0};
        const promises = vm.selectedTestRuns.map(testRun => deleteTestRunFromQueue(testRun, resultsCounter));

        $q.all(promises)
            .finally(function() {
                const isAllTestsWasSelected = selectedCount === vm.testRuns.length;
                const isLastPage = vm.currentPage === Math.ceil(vm.totalResults / vm.pageSize);
                const isFirstPage = vm.currentPage === 1;

                showBulkOperationMessages({
                    action: 'deleted',
                    succeeded: resultsCounter.success,
                    failed: resultsCounter.fail,
                    total: selectedCount,
                });
                postBulkActionHandler(resultsCounter);
                // load previous page if:
                // 1) was selected all tests on page
                // 2) it was a last page
                // 3) it wasn't a single page
                // 4) no failed operations
                if (isAllTestsWasSelected  && isLastPage && !isFirstPage && !resultsCounter.fail) {
                    vm.currentPage -= 1;
                    onPageChange();
                } else if (resultsCounter.success) {
                    searchTestRuns()
                        .then((testRuns) => {
                            // update new data statuses and reinitialize selectedTestRuns if not all selected test runs was removed
                            if (resultsCounter.fail) {
                                vm.selectedTestRuns = testRuns.reduce((newSelectedTestRuns, testRun) => {
                                    if (vm.selectedTestRuns.length) {
                                        const index = vm.selectedTestRuns.findIndex(({ id }) => id === testRun.id);

                                        if (index !== -1) {
                                            testRun.selected = true;
                                            newSelectedTestRuns.push(testRun);
                                            vm.selectedTestRuns.splice(index, 1);
                                        }
                                    }

                                    return newSelectedTestRuns;
                                }, []);
                                // enable selectedAll if on the page left only previously selected test runs
                                if (vm.selectedTestRuns.length === testRuns.length) { vm.selectedAll = true; }
                            }
                        });
                }
            });
    }

    function abortSelectedTestRuns() {
        // if (vm.isToolConnected('JENKINS')) {
            const resultsCounter = { success: 0, fail: 0 };
            let promises = [];

            vm.selectedTestRuns = vm.selectedTestRuns.reduce((newSelection, testRun) => {
                if (testRun.status === 'IN_PROGRESS') {
                    newSelection.push(testRun);
                } else {
                    testRun.selected = false;
                }

                return newSelection;
            }, []);
            updateSelectedTestRuns();
            promises = vm.selectedTestRuns.map(testRun => abort(testRun, resultsCounter));

            $q.all(promises)
                .finally(() => {
                    showBulkOperationMessages({
                        action: 'aborted',
                        succeeded: resultsCounter.success,
                        failed: resultsCounter.fail,
                        total: vm.selectedTestRuns.length,
                    });
                    postBulkActionHandler(resultsCounter);
                });
        // } else {
        //     messageService.error('Unable connect to jenkins');
        // }
    }

    function abort(testRun, resultsCounter) {
        return TestRunService.abortCIJob(testRun.id, testRun.ciRunId)
            .then((rs) => {
                if (!rs.success && rs.message) {
                    if (resultsCounter) {
                        resultsCounter.fail += 1;
                    } else {
                        messageService.error(rs.message);
                    }
                    return;
                }
                const abortCause = {};
                const currentUser = UserService.currentUser;

                abortCause.comment = 'Aborted by ' + currentUser.username;

                return TestRunService.abortTestRun(testRun.id, testRun.ciRunId, abortCause)
                    .then((rs) => {
                        if (rs.success) {
                            testRun.status = 'ABORTED';
                            testRun.selected = false;
                            if (resultsCounter) {
                                resultsCounter.success += 1;
                            } else {
                                messageService.success('Testrun ' + testRun.testSuite.name + ' is aborted' );
                            }
                        } else {
                            if (resultsCounter) {
                                resultsCounter.fail += 1;
                            } else if (rs.message) {
                                messageService.error(rs.message);
                            }
                        }
                    });
            });
    }

    function batchEmail(event) {
        showEmailDialog(vm.selectedTestRuns, event)
            .then((resultsCounter) => {
                showBulkOperationMessages({
                    action: 'send by email',
                    succeeded: resultsCounter.success,
                    failed: resultsCounter.fail,
                    total: resultsCounter.total,
                });
                postBulkActionHandler(resultsCounter);
            })
            .catch(() => null);
    }

    function showEmailDialog(testRuns, event) {
        return $mdDialog
            .show({
                controller: 'EmailController',
                template: require('../../components/modals/email/email.html'),
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false,
                controllerAs: '$ctrl',
                locals: {
                    testRuns: testRuns,
                },
            });
    }

    function postBulkActionHandler(resultsCounter) {
        // reset selectAll if enabled and we have success responses
        if (vm.selectedAll && resultsCounter.success) {
            vm.selectedAll = false;
        }
        // update or reset selectedTestRuns object
        if (!resultsCounter.fail) {
            vm.selectedTestRuns = [];
        } else {
            updateSelectedTestRuns();
        }
    }

    function isAbleAbortTestRun(testRun) {
        const notIsLocal = testRun.job?.name !== 'local';
        const isInProgress = testRun.status === 'IN_PROGRESS';

        return notIsLocal && isInProgress;
    }

    function isAbleRebuildTestRun(testRun) {
        const notIsLocal = testRun.job?.name !== 'local';
        const isInProgress = testRun.status !== 'IN_PROGRESS';

        return notIsLocal && isInProgress;
    }

    function isAbleAbortTestRuns() {
        if (!vm.selectedTestRuns?.length) { return false; }

        return vm.selectedTestRuns.every(isAbleAbortTestRun);
    }

    function isAbleRebuildTestRuns() {
        if (!vm.selectedTestRuns?.length) { return false; }

        return vm.selectedTestRuns.every(isAbleRebuildTestRun);
    }

    function isAbleCompareTestRuns() {
        if (!vm.selectedTestRuns?.length || vm.selectedTestRuns.length === 1) { return false; }

        return areTestRunsFromOneSuite();
    }

    function selectTestRun(testRun) {
        if (testRun.selected) {
            if (vm.testRuns.every(testRun => testRun.selected)) {
                vm.selectedAll = true;
            }
        } else if (!testRun.selected && vm.selectedAll) {
            vm.selectedAll = false;
        }

        updateSelectedTestRuns();
    }

    function selectAllTestRuns() {
        vm.testRuns.forEach(testRun => testRun.selected = vm.selectedAll);
        updateSelectedTestRuns();
    }

    function clearTestRunsSelection() {
        vm.selectedAll = false;
        vm.testRuns.forEach(testRun => testRun.selected = vm.selectedAll);
        updateSelectedTestRuns();
    }

    function updateSelectedTestRuns() {
        vm.selectedTestRuns = vm.testRuns.filter(testRun => testRun.selected);
    }

    function deleteSingleTestRun(testRun) {
        const confirmation = confirm('Do you really want to delete "' + testRun.testSuite.name + '" test run?');

        if (confirmation) {
            const id = testRun.id;
            TestRunService.deleteTestRun(id).then(function(rs) {
                const messageData = rs.success ? {success: rs.success, id: id, message: 'Test run{0} {1} removed'} : {id: id, message: 'Unable to delete test run{0} {1}'};

                UtilService.showDeleteMessage(messageData, [id], [], []);
                if (rs.success) {
                    vm.selectedAll = false;
                    //if it was last item on the page try to load previous page
                    if (vm.testRuns.length === 1 && vm.currentPage !== 1) {
                        vm.currentPage -= 1;
                        onPageChange();
                    } else {
                        searchTestRuns();
                    }
                }
            });
        }
    }

    function deleteTestRunFromQueue(testRun, resultsCounter) {
        return TestRunService.deleteTestRun(testRun.id)
            .then(function(rs) {
                if (rs.success) {
                    testRun.selected = false;
                    resultsCounter.success += 1;
                } else {
                    resultsCounter.fail += 1;
                }
        });
    }

    function getEventFromMessage(message) {
        return JSON.parse(message.replace(/&quot;/g, '"').replace(/&lt;/g, '<').replace(/&gt;/g, '>'));
    }

    function initWebsocket() {
        const wsName = 'zafira';

        vm.zafiraWebsocket = Stomp.over(new SockJS(`${$httpMock.apiHost}/api/reporting/api/websockets`));
        vm.zafiraWebsocket.debug = null;
        vm.zafiraWebsocket.ws.close = function() {};
        vm.zafiraWebsocket.connect({withCredentials: false}, function () {
            vm.subscriptions.statistics = subscribeStatisticsTopic();
            vm.subscriptions.testRuns = subscribeTestRunsTopic();
            vm.subscriptions.launchedTestRuns = subscribeLaunchedTestRuns();
            UtilService.websocketConnected(wsName);
        }, function () {
            UtilService.reconnectWebsocket(wsName, initWebsocket);
        });
    }

    function subscribeLaunchedTestRuns() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.launcherRuns`, function (data) {
            const event = getEventFromMessage(data.body);
            const launcher = event.launcher;

            launcher.status = 'LAUNCHING';
            launcher.ciRunId = event.ciRunId;
            launcher.testSuite = { name: launcher.name };
            const indexOfLauncher = vm.launchers.findIndex((res) => { res.ciRunId === launcher.ciRunId });

            if (indexOfLauncher === -1) {
                vm.launchers = testsRunsService.addNewLauncher(launcher);
                setTimerOnDestroingLauncher(launcher);
                $scope.$apply();
            }
        });
    }

    function subscribeTestRunsTopic() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.testRuns`, function (data) {
            const event = getEventFromMessage(data.body);
            const testRun = angular.copy(event.testRun);
            const index = getTestRunIndexById(+testRun.id);

            testsRunsService.preparePlatformsData(testRun);
            if (vm.launchers) {
                const indexOfLauncher = vm.launchers.findIndex((launcher) => launcher.ciRunId === testRun.ciRunId);

                if (indexOfLauncher !== -1) {
                    clearTimeout(vm.launchers[indexOfLauncher].timeout);
                    $timeout(() => {
                        vm.launchers = testsRunsService.deleteLauncherFromCache(testRun.ciRunId);
                    }, 0);
                }
            }

            //add new testRun to the top of the list or update fields if it is already in the list
            if (index === -1) {
                // do not add new Test run if Search is active
                if (vm.currentPage === 1) {
                    testsRunsService.initTestRunData(testRun);
                    testsRunsService.getTestRuns(vm.searchParams)
                        .then((res) => {
                            vm.testRuns = res.results || [];
                            vm.testRuns.forEach(testsRunsService.preparePlatformsData);
                            vm.totalResults = res?.totalResults || 0;
                        })
                        .catch((error) => {
                            messageService.error(error.message);
                        });
                }
            } else {
                const data = {
                    status: testRun.status,
                    reviewed: testRun.reviewed,
                    elapsed: testRun.elapsed,
                    config: testRun.config,
                    comments: testRun.comments,
                };

                vm.testRuns = updateTestRun(index, data);
            }
            $scope.$apply();
        });
    }

    function subscribeStatisticsTopic() {
        return vm.zafiraWebsocket.subscribe(`/topic/${vm.tenant}.statistics`, function (data) {
            const event = getEventFromMessage(data.body);
            const index = getTestRunIndexById(+event.testRunStatistics.testRunId);

            if (index !== -1) {
                const data = {
                    inProgress: event.testRunStatistics.inProgress,
                    passed: event.testRunStatistics.passed,
                    failed: event.testRunStatistics.failed,
                    failedAsKnown: event.testRunStatistics.failedAsKnown,
                    skipped: event.testRunStatistics.skipped,
                    reviewed: event.testRunStatistics.reviewed,
                    aborted: event.testRunStatistics.aborted,
                };

                vm.testRuns = updateTestRun(index, data);
                $scope.$apply();
            }
        });
    }

    function updateTestRun(index, data = {}) {
        const testRun = vm.testRuns[index];

        if (testRun) {
            Object.keys(data).forEach((key) => testRun[key] = data[key]);
        }

        if (data.config) {
            testsRunsService.preparePlatformsData(testRun);
        }

        return [ ...vm.testRuns ];
    }

    function bindEvents() {
        $scope.$on('$destroy', function () {
            if (vm.zafiraWebsocket && vm.zafiraWebsocket.connected) {
                vm.subscriptions.statistics && vm.subscriptions.statistics.unsubscribe();
                vm.subscriptions.testRuns && vm.subscriptions.testRuns.unsubscribe();
                vm.subscriptions.launchedTestRuns && vm.subscriptions.launchedTestRuns.unsubscribe();
                $timeout(function () {
                    vm.zafiraWebsocket.disconnect();
                }, 0, false);
                UtilService.websocketConnected('zafira');
            }
        });
    }

    function getTestRunIndexById(id) {
        return vm.testRuns.findIndex((testRun) => testRun.id === id);
    }

    function getTestRunById(id) {
        let testRun;
        const index = getTestRunIndexById(id);

        index !== -1 && (testRun = vm.testRuns[index]);

        return testRun;
    }

    function showBulkOperationMessages({ action, succeeded, failed, total }) {
        let delay = 0;

        switch (true) {
            case !!succeeded:
                delay = 2500;
                showSuccessBulkOperationMessage({
                    action,
                    count: succeeded,
                    total,
                    options: {hideDelay: delay},
                });
                if (!failed) {
                    break;
                }
            case !!failed:
                $timeout(() => {
                    showFailBulkOperationMessage({
                        action,
                        count: failed,
                        total,
                    });
                }, delay);
        }
    }

    function showSuccessBulkOperationMessage({ action, count, total, options = {} }) {
        messageService.success(`${count} out of ${total} test runs have been successfully ${action}.`, options);
    }

    function showFailBulkOperationMessage({ action, count, total, options = {} }) {
        messageService.error(`${count} out of ${total} test runs have failed to be ${action}. Please, try again.`, options);
    }

    function bindEventListeners() {
        vm.$onDestroy = () => {
            if (vm.isMobile) {
                angular.element(scrollableParentElement).off('scroll.hideFabButton', onScroll);
            }
        }
        if (vm.isMobile) {
            angular.element(scrollableParentElement).on('scroll.hideFabButton', onScroll);
        }
    }

    function onScroll() {
        if (!vm.scrollTicking) {
            vm.scrollTicking = true;
            $scope.$apply();
            scrollTickingTimeout = $timeout(() => {
                vm.scrollTicking = false;
            }, 300);
        } else {
            $timeout.cancel(scrollTickingTimeout);
            scrollTickingTimeout = $timeout(() => {
                vm.scrollTicking = false;
            }, 300);
        }
    }

    function getNotificationAvailability() {
        return toolsService.getIfAnyNotificationToolConnected()
            .then((isAvailableAnyNotificationTool) => vm.isNotificationAvailable = isAvailableAnyNotificationTool);
    }

    function onSearchParamsChange(searchParams = {}) {
        vm.searchParams = applyNewSearchParams(searchParams);
        const stateParams = testsRunsService.prepareQueryParams(vm.searchParams);

        $state.go($state.current, stateParams, { inherit: false })
            .then(() => updateActiveSavedSearch(stateParams.filterID));
        searchTestRuns();
    }

    function applyNewSearchParams(searchParams) {
        if (vm.currentPage !== searchParams.page) {
            vm.currentPage = searchParams.page;
        }

        const newSearchParams =  {
            ...searchParams,
            pageSize: vm.pageSize,
        }

        return newSearchParams;
    }
};

export default testsRunsController;

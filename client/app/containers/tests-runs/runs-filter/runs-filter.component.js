import template from './runs-filter.html';
import './runs-filter.scss';

const runsFilterComponent = {
    template,
    controller: 'runsFilterController',
    transclude: true,
    bindings: {
        activeSearchParams: '=?',
        onSavedSearchesClick: '&',
        onSearch: '&',
        onSearchSave: '&',
        searchesList: '<',
        selectedTestsRunsCount: '<',
    },
};

export default runsFilterComponent;

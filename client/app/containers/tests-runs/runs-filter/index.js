import runsFilterComponent from './runs-filter.component';
import runsFilterController from './runs-filter.controller'

export const RunsFilterModule = angular.module('runsFilter', [])

    .controller('runsFilterController', runsFilterController)
    .component('runsFilter', runsFilterComponent)

    .name;

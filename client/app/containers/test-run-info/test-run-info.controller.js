'use strict';

import { Subject, from as rxFrom, timer, of, combineLatest } from 'rxjs';
import { switchMap, takeUntil, map, catchError } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

import ImagesViewerController from '../../components/modals/images-viewer/images-viewer.controller';
import ImagesViewerTemplate from '../../components/modals/images-viewer/images-viewer.html';
import IssuesModalController from '../../components/modals/issues/issues.controller';
import IssuesModalTemplate from '../../components/modals/issues/issues.html';

const testRunInfoController = function testRunInfoController(
    $httpMock,
    $location,
    $mdDialog,
    $mdMedia,
    $q,
    $scope,
    $state,
    $stateParams,
    $timeout,
    $transitions,
    $window,
    UtilService,
    ArtifactService,
    testArtifactsService,
    TestExecutionHistoryService,
    TestRunService,
    testsRunsService,
    TestService,
    testsSessionsService,
    pageTitleService,
    integrationsService,
    authService,
    messageService,
    logLevelService,
    modalsService,
    moment,
) {
    'ngInject';

    let onTransStartSubscription = null;
    let jiraIntegration = {};
    let testrailIntegration = {};
    let qtestIntegration = {};
    let wsSubscription = null;
    let stompClient = null;
    let logsRequestsCanceler = $q.defer();
    const testsWebsocketName = 'tests';
    const logsGettingDestroy$ = new Subject();
    const liveLogsGetInterval = 5000;
    const fileExtensionPattern = /\.([0-9a-z]+)(?:[?#]|$)/i;
    const searchParamsDefaults = {
        logs: {
            page: 1,
            fetchedCount: 0,
            totalResults: 0,
            pageSize: 10000,
        },
        screenshots: {
            page: 1,
            fetchedCount: 0,
            totalResults: 0,
            pageSize: 10000,
        },
    };
    const searchParams = {
        logs: { ...searchParamsDefaults.logs },
        screenshots: { ...searchParamsDefaults.screenshots },
        get logsParams() {
            return {
                page: this.logs.page,
                pageSize: this.logs.pageSize,
            };
        },
        get screenshotsParams() {
            return {
                page: this.screenshots.page,
                pageSize: this.screenshots.pageSize,
            };
        },
    };
    const defaultLimitCount = 5;
    let testStatus;
    const linkIssueTestStatuses = ['failed', 'aborted', 'skipped'];
    const noSettingsTestStatuses = ['in_progress', 'unknow'];
    const vm = {
        activeDriverIndex: 0,
        activeMode: null,
        activeTestId: null,
        configSnapshot: null,
        drivers: [],
        executionHistory: [],
        logs: [],
        parentTestId: null,
        test: null,
        testRun: null,
        logLevels: logLevelService.logLevels,
        filteredLogs: [],
        selectedLevel: logLevelService.initialLevel,
        selectedLogRow: -1,
        isControllerRefreshing: false,
        isLogsLoading: true,
        isIntegrationInitialized: false,
        artifactsLimitCount: defaultLimitCount,
        labelsLimitCount: defaultLimitCount,
        sessions: [],
        breadcrumbsCustomActions: null,

        $onInit: controllerInit,
        $onDestroy,
        changeTestStatus,
        copyLogLine,
        copyLogPermalink,
        downloadAllArtifacts,
        filterResults,
        getFullLogMessage,
        goToTests,
        goToTestRuns,
        isLiveVideoArtifact,
        onDriverChange,
        onHistoryElemClick,
        openImagesViewerModal,
        selectLogRow,
        showDetailsDialog,
        switchMoreLess,
        userHasAnyPermission: authService.userHasAnyPermission,
        showMore,
        showLess,
        getArtifactName,
        disabledTestRunsSettings,
        displayLinkIssue,

        get canManageTestRuns() { return vm.userHasAnyPermission(['reporting:tests:update']); },
        get currentTitle() { return pageTitleService.pageTitle; },
        get defaultLimitCount() { return defaultLimitCount; },
        get isMobile() { return $mdMedia('xs'); },
        get jiraIntegration() { return jiraIntegration; },
        get labels() { return vm.test?.labels ?? []; },
        get qtestIntegration() { return qtestIntegration; },
        get testrailIntegration() { return testrailIntegration; },
        get backToRunsParams() {
            const decodedPrevParams = JSON.parse((UtilService.base64_decode($stateParams.runsState)) || '{}');

            return {
                activeTestRunId: vm.testRun.id,
                ...decodedPrevParams,
            };
        },
    };


    let agent = null;
    const MODES = {
        live: {
            name: 'live',
            initFunc: initLiveMode,
        },
        record: {
            name: 'record',
            initFunc: initRecordMode,
            screenshotsAttempts: 0,
            screenshotsAttemptsLimit: 3,
            screenshotsAttemptDelay: 10000,
        },
    };

    return vm;

    /* Controller methods and helpers */
    function goToTests() {
        const parentTest = vm.executionHistory.find(({ testId }) => testId === vm.parentTestId);

        $state.go('tests.runDetails', {
            testRunId: parentTest ? parentTest.testRunId : $stateParams.testRunId,
            configSnapshot: vm.configSnapshot,
            runsState: $stateParams.runsState,
        });
    }

    function goToTestRuns() {
        $state.go('tests.runs', vm.backToRunsParams);
    }

    function filterResults(index) {
        if (vm.selectedLevel === logLevelService.logLevels[index]) {
            return;
        }

        vm.selectedLevel = logLevelService.logLevels[index];
        vm.filteredLogs = logLevelService.filterLogs(vm.logs, vm.selectedLevel);
    }

    function downloadAllArtifacts() {
        ArtifactService.downloadArtifacts({
            data: [vm.test],
            field: 'artifactsToDownload',
        });
    }

    function changeTestStatus(test, status = '') {
        status = status.toUpperCase();
        if (!test || !status || test.status === status.toUpperCase()) { return; }

        const testCopy = { ...test };

        testCopy.status = status.toUpperCase();

        return TestService.updateTest(testCopy)
            .then((response) => {
                if (response.success) {
                    messageService.success(`Test was marked as ${status}`);
                    vm.test = response.data;
                    testStatus = vm.test.status.toLowerCase();
                    updateExecutionHistoryItem(vm.test);
                } else {
                    const message = response.message ? response.message : 'Unable to change test status';

                    messageService.error(message);
                }
            });
    }

    function setTestParams() {
        if (vm.test) {
            setMode(vm.test.status === 'IN_PROGRESS' ? 'live' : 'record');
            vm.activeMode.initFunc();
        }
    }

    function setMode(modeName) {
        if (vm.activeMode?.name === modeName) { return; }

        vm.activeMode = { ...MODES[modeName] };
    }

    function openImagesViewerModal(event, url) {
        const activeArtifact = vm.test.imageArtifacts.find(({ value }) => value === url);

        if (activeArtifact) {
            $mdDialog.show({
                controller: ImagesViewerController,
                template: ImagesViewerTemplate,
                controllerAs: '$ctrl',
                bindToController: true,
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: false,
                escapeToClose: false,
                locals: {
                    test: vm.test,
                    activeArtifactValue: activeArtifact.value,
                },
            });
        }
    }

    function switchMoreLess(e, log) {
        e.preventDefault();
        e.stopPropagation();
        const rowElem = e.target.closest('.testrun-info__tab-table-col._action')
            || e.target.closest('.testrun-info__tab-mobile-table-data._action-data');

        log.showMore = !log.showMore;
        if (!log.showMore) {
            if (rowElem.scrollIntoView) {
                $timeout(() => {
                    rowElem.scrollIntoView(true);
                }, 0, false);
            }
        }
    }

    function getFullLogMessage(log) {
        return log.message
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/ *(\r?\n|\r)/g, '<br/>')
            .replace(/\s/g, '&nbsp;');
    }

    function prepareArtifacts() {
        // extract image artifacts from logs
        const imageArtifacts = vm.logs
            .filter((log) => log.type === 'screenshot')
            .reduce((formatted, artifact) => {
                const urls = artifact.urls;

                if (urls.image?.path) {
                    const path = urls.image.path;
                    const extensionMatch = path.match(fileExtensionPattern);
                    let newArtifact = {
                        name: urls.image.name || 'screenshot',
                        value: path,
                    };

                    if (extensionMatch) {
                        newArtifact.extension = extensionMatch[1];
                    }
                    if (urls.thumb?.path) {
                        newArtifact.thumb = urls.thumb.path;
                    }
                    formatted.push(newArtifact);
                }

                return formatted;
            }, []);
        // extract artifacts from test
        const artifactsToDownload = vm.test.artifacts.reduce((formatted, artifact) => {
            const name = artifact.name.toLowerCase();

            if (!name.includes('live') && !name.includes('video') && artifact.value) {
                const links = artifact.value.split(' ');
                let link = links[0];
                const extensionMatch = link.match(fileExtensionPattern);

                // if link is relative
                if (!link.startsWith('http')) {
                    if (link[0] !== '/') {
                        link = `/${link}`;
                    }
                    artifact.value = `${$httpMock.apiHost}${link}`;
                }
                if (extensionMatch) {
                    artifact.extension = extensionMatch[1];
                }
                formatted.push(artifact);
            }

            return formatted;
        }, []);

        vm.test.imageArtifacts = imageArtifacts;
        vm.test.artifactsToDownload = [...artifactsToDownload, ...imageArtifacts];
        vm.test.atifactsToShow = vm.test.artifacts.filter((artifact) => {
            const name = artifact.name.toLowerCase();

            return !name.includes('live video') && artifact.value && !artifact.value.includes('artifacts/test-sessions/');
        });
    }

    function selectLogRow(ev, index) {
        const hash = ev.currentTarget.attributes.id.value;
        const stateParams = $state.params;

        stateParams['#'] = hash;

        const newUrl = $state.href('tests.runInfo', stateParams, {absolute: true});

        vm.selectedLogRow = index;
        // we don't want reload page
        $window.history.pushState(null, null, newUrl);
    }

    function copyLogLine(log) {
        const formattedTime = moment(log.timestamp).format('HH:mm:ss');
        const message = `${formattedTime} [${log.threadName}] [${log.level}] ${log.message}`;

        message.copyToClipboard();
    }

    function copyLogPermalink() {
        $location.absUrl().copyToClipboard();
    }

    function initSelectedLog() {
        const hash = $location.hash();

        if (hash) {
            const logIndex = parseInt(hash.replace('log-', ''), 10);

            if (!isNaN(logIndex)) {
                vm.selectedLogRow = logIndex;
            }
        }
    }

    // TODO: refactor this magic :)
    function bindEvents() {
        TestService.subscribeOnLocationChangeStart();
        onTransStartSubscription = $transitions.onStart({}, function (trans) {
            const toState = trans.to();

            if (toState.name !== 'tests.runDetails') {
                TestService.clearDataCache();
                TestService.clearPreviousUrl();
                TestService.unsubscribeFromLocationChangeStart();
            }
            onTransStartSubscription();
        });
    }

    function resetInitialValues(testRun, test) {
        onTransStartSubscription = null;
        jiraIntegration = {};
        agent = null;
        searchParams.logs = { ...searchParamsDefaults.logs };
        searchParams.screenshots = { ...searchParamsDefaults.screenshots };

        vm.isLogsLoading = true;
        vm.selectedLogRow = -1;
        vm.logs = [];
        vm.activeDriverIndex = 0;
        vm.activeMode = {};
        vm.drivers = [];
        vm.filteredLogs = [];
        vm.testRun = testRun;
        vm.test = test;
    }

    function unbindEvents() {
        logsGettingDestroy$.next();
        logsRequestsCanceler.resolve();
        wsSubscription = wsSubscription && wsSubscription.unsubscribe();
        closeTestsWebsocket();
        if (typeof onTransStartSubscription === 'function') {
            onTransStartSubscription();
        }
    }

    function initIntegrations() {
        $q.all([
            getJiraIntegration(),
            getTestrailIntegration(),
            getQtestIntegration(),
        ])
            .finally(() => vm.isIntegrationInitialized = true);
    }

    function getJiraIntegration() {
        return getIntegration('jira')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                jiraIntegration = integration;

                return jiraIntegration;
            });
    }

    function getTestrailIntegration() {
        return getIntegration('testrail')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                testrailIntegration = integration;

                return testrailIntegration;
            });
    }

    function getQtestIntegration() {
        return getIntegration('qtest')
            .then((integration) => {
                integration.url = formatIntegrationUrl(integration.url);
                qtestIntegration = integration;

                return qtestIntegration;
            });
    }

    function getIntegration(path) {
        return integrationsService.getIntegration(path)
            .then(({ data = {} }) => data);
    }

    function formatIntegrationUrl(url) {
        if (url) {
            url = url.replace(/\/$/, '');
        }

        return url;
    }

    function showDetailsDialog(test, event) {
        modalsService
            .openModal({
                controller: IssuesModalController,
                template: IssuesModalTemplate,
                parent: angular.element(document.body),
                targetEvent: event,
                controllerAs: '$ctrl',
                fullscreen: false,
                locals: {
                    test,
                }
            })
            .catch(() => {});
    }

    function initLiveMode() {
        startLiveTestLogsGetter$()
            .pipe(
                takeUntil(logsGettingDestroy$),
            )
            .subscribe({
                complete() { vm.isLogsLoading = false; },
            });
    }

    function initRecordMode() {
        getStoredLogsAndScreenshots$()
            .pipe(
                takeUntil(logsGettingDestroy$),
            )
            .subscribe({
                complete(res) { vm.isLogsLoading = false; },
            });
    }

    function controllerInit(skipHistoryUpdate) {
        if (!vm.testRun || !vm.test) { return; }

        testStatus = vm.test.status.toLowerCase();
        vm.test.labels = sortLabels(vm.test.labels);
        pageTitleService.setTitle(vm.test.name);
        initSelectedLog();
        initTestsWebSocket();
        vm.testRun.normalizedPlatformData = testsRunsService.normalizeTestPlatformData(vm.testRun.config);
        initTestExecutionData(skipHistoryUpdate);
        initIntegrations();
        bindEvents();
        setTestParams();
        vm.breadcrumbsCustomActions = {
            'TestRun': {
                action: vm.goToTests,
                customItemTitle: vm.testRun.name,
            },
            'Test Runs': {
                action: vm.goToTestRuns,
            },
        };
    }

    function $onDestroy() {
        unbindEvents();
    }

    /* END Controller methods and helpers */

    /* Work with drivers */
    function onDriverChange({ activeDriverIndex }) {
        vm.activeDriverIndex = activeDriverIndex;
    }

    function isVideoArtifact(artifact) {
        return artifact.name.toLowerCase().includes('video') && !artifact.name.toLowerCase().includes('live');
    }

    function isLiveVideoArtifact(artifact) {
        return artifact.name.toLowerCase().includes('video') && artifact.name.toLowerCase().includes('live');
    }

    function addSessions(sessions, resetSessions) {
        if (resetSessions) {
            vm.sessions = [];
        }

        let needToChangeActiveSession = false;
        const reducedSessions = sessions.reduce((accum, session) => {
            const foundSession = vm.sessions.find(({ id }) => id === session.id);

            if (foundSession) {
                if (!equalSessionsArtifacts(session.artifactReferences, foundSession.artifactReferences)) {
                    session.isActive = foundSession.isActive;
                    accum.sessionsToUpdate = [session, ...accum.sessionsToUpdate];
                }
            } else {
                needToChangeActiveSession = true;
                accum.sessionsToAdd = [session, ...accum.sessionsToAdd];
            }

            return accum;
        }, { sessionsToAdd: [], sessionsToUpdate: [] });

        let restSessions = vm.sessions;

        if (reducedSessions.sessionsToUpdate.length) {
            restSessions = vm.sessions.filter(({ id }) => !reducedSessions.sessionsToUpdate.find((session) => session.id === id));
        }

        reducedSessions.sessionsToAdd.forEach(addArtifactsTypes);
        reducedSessions.sessionsToUpdate.forEach(addArtifactsTypes);

        const resultSessions = [...reducedSessions.sessionsToAdd, ...reducedSessions.sessionsToUpdate, ...restSessions]
            .sort((a, b) => moment(b.startedAt) - moment(a.startedAt));

        if (needToChangeActiveSession) {
            resultSessions.forEach((session) => session.isActive = false);
            resultSessions[0].isActive = true;
        }

        vm.sessions = resultSessions;
    }

    function addStoredSessions(sessions, resetSessions) {
        if (resetSessions) {
            vm.sessions = [];
        }

        if (!sessions?.length) {
            return;
        }

        sessions.forEach(addArtifactsTypes);
        sessions.sort((a, b) => moment(b.startedAt) - moment(a.startedAt));
        sessions[0].isActive = true;
        vm.sessions = sessions;
    }

    function equalSessionsArtifacts(a, b) {
        if ((!a && b) || (!b && a)) { return false; }
        if (a.length !== b.length) { return false; }

        return a.every((artifact) => b.find(({ name, value }) => artifact.name === name && artifact.value === value));
    }

    function addArtifactsTypes(session) {
        if (session.artifacts?.length) {
            session.artifacts.forEach((artifact) => {
                if (isVideoArtifact(artifact)) {
                    artifact.type = 'video';
                } else if (isLiveVideoArtifact(artifact)) {
                    artifact.type = 'vnc';
                } else {
                    artifact.type = 'attachment';
                }
            });
        }
    }
    /* END Work with drivers */

    /* Work with WebSocket */
    function initTestsWebSocket() {
        const ws = new SockJS(`${$httpMock.apiHost}/api/reporting/api/websockets`);

        stompClient = Stomp.over(ws);
        stompClient.debug = null;
        stompClient.ws.close = () => {};
        stompClient.connect({ withCredentials: false }, stompConnectHandler, stompErrorHandler);

        UtilService.websocketConnected(testsWebsocketName);
    }

    function stompConnectHandler() {
        if (stompClient.connected) {
            const destination = `/topic/${authService.tenant}.testRuns.${vm.testRun.id}.tests`;

            wsSubscription = stompClient.subscribe(destination, onTestsWSMessage);
        }
    }

    function stompErrorHandler() {
        UtilService.reconnectWebsocket(testsWebsocketName, initTestsWebSocket);
    }

    function onTestsWSMessage(wsEvent) {
        if (!wsEvent.body) { return; }

        const test = getEventFromMessage(wsEvent.body).test;

        if (test && vm.test && test.id === vm.test.id) {
            // keep logs getting work for 30secs delay before stop it
            if (test.status !== 'IN_PROGRESS' && vm.test.status === 'IN_PROGRESS') {
                $timeout(() => logsGettingDestroy$.next(), 30000);
            }
            vm.test = test;

            $scope.$apply();
        }
        updateExecutionHistoryItem(test);
    }

    function getEventFromMessage(message) {
        let parsedMessage = {};

        try {
            parsedMessage = JSON.parse(message.replace(/&quot;/g, '"').replace(/&lt;/g, '<').replace(/&gt;/g, '>'));
        } catch (err) {
            // TODO: ?log error
        }

        return parsedMessage;
    }

    function closeTestsWebsocket() {
        if (stompClient && stompClient.connected) {
            stompClient.disconnect();
            UtilService.websocketConnected(testsWebsocketName);
        }
    }
    /* END Work with WebSocket */

    /* Work with screenshots */
    function presentsUncompletedScreenshotLogs() {
        return vm.logs.some(({ urls }) => urls && !(urls.image && urls.thumb));
    }
    /* END work with screenshots */

    /* Work with execution history */
    function initTestExecutionData(skipHistoryUpdate) {
        vm.activeTestId = vm.test.id;
        vm.parentTestId = $stateParams.parentTestId ? parseInt($stateParams.parentTestId, 10) : vm.activeTestId;

        if (!skipHistoryUpdate) {
            TestExecutionHistoryService.getTestExecutionHistory(vm.parentTestId)
                .then((response) => {
                    if (response.success) {
                        const sortedData = (response.data || [])
                            .sort((a, b) => {
                                // handles cases when tests started at the same time. Sort them by ID
                                if (a.startTime === b.startTime) {
                                    return a.testId - b.testId;
                                }

                                return a.startTime - b.startTime;
                            });

                        vm.executionHistory = TestExecutionHistoryService.addTimeDiffs(sortedData);
                    }
                });
        }
    }

    function onHistoryElemClick(historyItem) {
        if (vm.isControllerRefreshing) { return; }
        vm.isControllerRefreshing = true;
        getPageData(historyItem.testRunId, historyItem.testId)
            .then(([testRun, test]) => {
                pushNewState(historyItem);
                unbindEvents();
                logsRequestsCanceler = $q.defer();
                resetInitialValues(testRun, test);
                updateExecutionHistoryItem(test);
                $timeout(() => {
                    controllerInit(true);
                });
            })
            .catch((err) => {
                if (err.message) {
                    messageService.error(err.message);
                }
            })
            .finally(() => {
                $timeout(() => {
                    vm.isControllerRefreshing = false;
                }, 0, false);
            });
    }

    function pushNewState(historyItem) {
        const stateParams = $state.params;

        stateParams.testId = historyItem.testId;
        stateParams.testRunId = historyItem.testRunId;
        if (historyItem.testId === vm.parentTestId) {
            Reflect.deleteProperty(stateParams, 'parentTestId');
        } else {
            stateParams.parentTestId = vm.parentTestId;
        }
        Reflect.deleteProperty(stateParams, '#');

        const newUrl = $state.href('tests.runInfo', stateParams, {absolute: true});

        $window.history.pushState(null, null, newUrl);
    }

    function getPageData(testRunId, testId) {
        const testParams = {
            'page': 1,
            'pageSize': 100000,
            testRunId,
        };

        const testRequest = TestService.searchTests(testParams)
            .then((rs) => {
                if (rs.success) {
                    TestService.tests = rs.data.results || [];

                    return TestService.getTest(testId);
                } else {
                    return $q.reject({message: `Can't fetch tests for test run with ID=${testRunId}` });
                }
            });

        const testRunRequest = TestRunService.searchTestRuns({ id: testRunId })
            .then((response) => {
                if (response.success && response.data.results && response.data.results[0]) {
                    return response.data.results[0];
                } else {
                    return $q.reject({message: 'Can\'t get test run with ID=' + testRunId});
                }
            });

        return $q.all([testRunRequest, testRequest]);
    }

    function updateExecutionHistoryItem(test) {
        if (!test) { return; }

        const updatingTestIndex = vm.executionHistory.findIndex(({ testId }) => testId === test.id);

        if (updatingTestIndex !== -1) {
            const status = test.status;
            const issueReferences = (test.issueReferences || []);
            const elapsed = test.finishTime ? test.finishTime - test.startTime : null;
            const testToUpdate = vm.executionHistory[updatingTestIndex];

            $timeout(() => {
                vm.executionHistory[updatingTestIndex] = {
                    ...testToUpdate,
                    status,
                    elapsed,
                    issueReferences,
                };
                vm.executionHistory = TestExecutionHistoryService.addTimeDiffs(vm.executionHistory);
            }, 0);
        }
    }
    /* END Work with execution history */

    /* Work with logs */
    function startLiveTestLogsGetter$() {
        vm.logs = [];
        vm.sessions = [];

        return getLiveTestLogsAndScreenshots$();
    }

    function getLiveTestLogsAndScreenshots$() {
        return combineLatest([getLiveTestLogs$(), getLiveTestScreenshots$(), getLiveSessions$()]);
    }

    function getLiveTestLogs$() {
        return getTestLogs$()
            .pipe(
                switchMap((hasSecondPage) => hasSecondPage ? getLiveTestLogs$() : reGetLiveTestLogs$()),
                catchError(logsErrorHandler$),
            );
    }

    function reGetLiveTestLogs$() {
        return timer(liveLogsGetInterval)
            .pipe(
                switchMap(() => getLiveTestLogs$()),
            );
    }

    function getLiveTestScreenshots$() {
        return getTestScreenshots$()
            .pipe(
                switchMap((hasSecondPage) => hasSecondPage ? getLiveTestScreenshots$() : reGetLiveTestScreenshots$()),
                catchError(() => logsErrorHandler$({ message: 'Unable to fetch screenshots' })),
            );
    }

    function reGetLiveTestScreenshots$() {
        return timer(liveLogsGetInterval)
            .pipe(
                switchMap(() => getLiveTestScreenshots$()),
            );
    }

    function getLiveSessions$() {
        return getSessions$()
            .pipe(
                switchMap(() => reGetLiveSessions$()),
                catchError(() => logsErrorHandler$({ message: 'Unable to fetch sessions' })),
            );
    }

    function reGetLiveSessions$() {
        return timer(liveLogsGetInterval)
            .pipe(
                switchMap(() => getLiveSessions$()),
            );
    }

    function logsErrorHandler$(error) {
        messageService.error(error.message || 'Unable to fetch logs');

        return of(true);
    }

    function getStoredLogsAndScreenshots$() {
        vm.logs = [];
        vm.sessions = [];

        return combineLatest([getStoredTestLogs$(), getStoredTestScreenshots$(), getStoredTestSessions$()]);
    }

    function getStoredTestScreenshots$() {
        const activeParams = searchParams.screenshots;

        return getTestScreenshots$()
            .pipe(
                switchMap((needReFetch) => {
                    if (needReFetch) {
                        return getStoredTestScreenshots$();
                    } else if (vm.activeMode.screenshotsAttempts < vm.activeMode.screenshotsAttemptsLimit) {
                        vm.activeMode.screenshotsAttempts += 1;

                        return reGetStoredTestScreenshots$();
                    }

                    return of(null);
                }),
                catchError(() => logsErrorHandler$({ message: 'Unable to fetch screenshots' })),
            );
    }

    function reGetStoredTestScreenshots$() {
        return timer(vm.activeMode.screenshotsAttemptDelay)
            .pipe(
                switchMap(() => getStoredTestScreenshots$()),
            );
    }

    function getStoredTestLogs$() {
        return getTestLogs$()
            .pipe(
                switchMap((hasSecondPage) => hasSecondPage ? getStoredTestLogs$() : of(null)),
                catchError(logsErrorHandler$),
            );
    }

    function getStoredTestSessions$() {
        return getSessions$(true)
            .pipe(
                catchError(logsErrorHandler$),
            );
    }

    function getTestLogs$() {
        const activeParams = searchParams.logs;

        return fetchTestLogs$(searchParams.logsParams)
            .pipe(
                map((response) => {
                    const hasSecondPage = response.totalResults > activeParams.pageSize;
                    const logs = response.results.slice(activeParams.fetchedCount);

                    if (logs.length) {
                        logs.forEach((log) => {
                            log.type = 'log';
                            log.uuidInternal = uuidv4();
                        });
                        handleLogs(logs);
                    }
                    activeParams.fetchedCount = response.results.length;
                    if (hasSecondPage) {
                        activeParams.pageSize = response.totalResults;
                    }

                    return hasSecondPage;
                }),
            );
    }

    function fetchTestLogs$(params) {
        const config = {
            params,
            timeout: logsRequestsCanceler.promise,
        };

        return rxFrom(testArtifactsService.getTestLogs(vm.testRun.id, vm.test.id, config))
            .pipe(
                map(({ data }) => data),
            );
    }

    function getTestScreenshots$() {
        const activeParams = searchParams.screenshots;

        return fetchTestScreenshots(searchParams.screenshotsParams)
            .pipe(
                map((response) => {
                    const hasSecondPage = response.totalResults > activeParams.pageSize;
                    const screenshots = response.results.slice(activeParams.fetchedCount);

                    if (screenshots.length) {
                        screenshots.forEach(prepareScreenshotLog);
                        handleLogs(screenshots);
                    }
                    activeParams.fetchedCount = response.results.length;
                    if (hasSecondPage) {
                        activeParams.pageSize = response.totalResults;
                    }

                    return hasSecondPage;
                }),
            );
    }

    function fetchTestScreenshots(params) {
        const config = {
            params,
            timeout: logsRequestsCanceler.promise,
        };

        return rxFrom(testArtifactsService.getTestScreenshots(vm.testRun.id, vm.test.id, config))
            .pipe(
                map(({ data }) => data),
            );
    }

    function getSessions$(isStored) {
        return fetchSessions$()
            .pipe(
                map((sessions) => {
                    if (sessions.length) {
                        handleSessions(sessions, isStored);
                    }

                    return sessions;
                }),
            );
    }

    function fetchSessions$() {
        const config = {
            timeout: logsRequestsCanceler.promise,
        };

        return rxFrom(testsSessionsService.getSessions(vm.testRun.id, vm.test.id, config))
            .pipe(
                map(({ data }) => data.items || []),
            );
    }

    function handleLogs(logs) {
        vm.logs = [ ...vm.logs, ...logs ]
            .sort((a, b) => a.timestamp - b.timestamp);
        vm.filteredLogs = logLevelService.filterLogs(vm.logs, vm.selectedLevel);

        $scope.$applyAsync();
        prepareArtifacts();
    }

    function handleSessions(sessions, isStored) {
        $scope.$applyAsync(() => isStored ? addStoredSessions(sessions) : addSessions(sessions));
    }

    function prepareScreenshotLog(log) {
        log.level = 'INFO';
        log.type = 'screenshot';
        log.message = log.message || 'Screenshot is captured';
        log.uuidInternal = uuidv4();
        log.urls = {};

        if (log.src) {
            let link = log.src;
            let thumbLink = '';

            // if link is relative
            if (!link.startsWith('http')) {
                if (link[0] !== '/') {
                    link = `/${link}`;
                }

                link = `${$httpMock.apiHost}${link}`;
            }
            thumbLink = link.replace(fileExtensionPattern, '_thumbnail.$1');

            log.urls.image = { path: link, name: 'screenshot' };
            log.urls.thumb = { path: thumbLink };
        }
    }
    /* END Work with logs */

    function showMore(type) {
        if (type === 'artifacts') {
            vm.artifactsLimitCount = vm.test.artifacts.length;
        } else if (type === 'labels') {
            vm.labelsLimitCount = vm.labels.length;
        }
    }

    function showLess(type) {
        if (type === 'artifacts') {
            vm.artifactsLimitCount = defaultLimitCount;
        } else if (type === 'labels') {
            vm.labelsLimitCount = defaultLimitCount;
        }
    }

    function sortLabels(labels) {
        const qTestArr = [];
        const testrailArr = [];
        const otherArr = [];

        labels.forEach(el => {
            if (el.key === 'com.zebrunner.app/tcm.testrail.testcase-id') {
                testrailArr.push(el);
            } else if (el.key === 'com.zebrunner.app/tcm.qtest.testcase-id') {
                qTestArr.push(el);
            } else {
                otherArr.push(el);
            }
        });

        return [...sortLabelsById(testrailArr), ...sortLabelsById(qTestArr), ...otherArr.sort((a, b) => a.key.localeCompare(b.key))];
    }

    function sortLabelsById(arr) {
        return arr.sort((a, b) => parseInt(a.normalizedValue, 10) - parseInt(b.normalizedValue, 10));
    }

    function getArtifactName(artifact) {
        return artifact.name || 'N/A';
    }

    function disabledTestRunsSettings() {
        const isMatchedStatus =  noSettingsTestStatuses.includes(testStatus);

        return !vm.canManageTestRuns || isMatchedStatus;
    }

    function displayLinkIssue() {
        const isMatchedStatus = linkIssueTestStatuses.includes(testStatus);

        return vm.canManageTestRuns && isMatchedStatus && vm.test.message;
    }
};

export default testRunInfoController;

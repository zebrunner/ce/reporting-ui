'use strict';

import testRunInfoComponent from './test-run-info.component';
import testArtifactsService from './test-artifacts.service';
import { testExecutionHistoryModule } from './test-execution-history/test-execution-history.module';

export const testRunInfoModule = angular.module('app.testRunInfo', [
    testExecutionHistoryModule,
])
    .factory({ testArtifactsService })
    .component({ testRunInfoComponent });

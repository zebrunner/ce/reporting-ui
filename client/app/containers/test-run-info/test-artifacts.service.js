'use strict';

const testArtifactsService = function testArtifactsService(
    $httpMock,
) {
    'ngInject';

    return {
        getTestLogs,
        getTestScreenshots,
    };

    function getTestLogs(testRunId, testId, config) {
        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/test-runs/${testRunId}/tests/${testId}/logs`, config);
    }

    function getTestScreenshots(testRunId, testId, config) {
        return $httpMock.get(`${$httpMock.apiHost}/api/reporting/v1/test-runs/${testRunId}/tests/${testId}/screenshots`, config);
    }
};

export default testArtifactsService;

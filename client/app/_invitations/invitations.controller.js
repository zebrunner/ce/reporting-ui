import inviteUserModalTpl from '../shared/modals/invitation-modal/invitation-modal.html';
import inviteUserModalCtrl from '../shared/modals/invitation-modal/invitation-modal.controller';

const InvitationsController = function InvitationsController(
    $location,
    $mdDialog,
    authService,
    GroupService,
    InvitationService,
    messageService,
    pageTitleService,
    $timeout,
) {
    'ngInject';

    let DEFAULT_SC = {
        page: 1,
        pageSize: 20,
        query: null,
        orderBy: null,
        sortOrder: null
    };

    const scrollableParentElement = document.querySelector('.ui-section');

    const vm = {
        showInviteUsersDialog: showInviteUsersDialog,
        takeOff: takeOff,
        retryInvite: retryInvite,
        isFiltered: false,
        sc: angular.copy(DEFAULT_SC),
        sr: [],
        totalResults: 0,
        search: search,
        reset: reset,
        copyLink: copyLink,
        userHasAnyPermission: authService.userHasAnyPermission,
        isInvitationsEmpty,

        get currentTitle() { return pageTitleService.pageTitle; },
        get groups() {return GroupService.groups;},
    };

    vm.$onInit = initController;

    return vm;

    function isInvitationsEmpty() {
        return vm.sr && !vm.sr.length;
    }

    function showInviteUsersDialog(event) {
        $mdDialog
            .show({
                controller: inviteUserModalCtrl,
                template: inviteUserModalTpl,
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: false,
                locals: {
                    groups: GroupService.groups,
                },
            })
            .then(
                function (invitations = []) {
                    vm.sr = [...invitations, ...vm.sr];
                    vm.totalResults += invitations.length;
                },
                function () {},
            );
    }

    function copyLink(invite) {
        buildInvitationUrl(invite.token).copyToClipboard();
        messageService.success('Link copied to clipboard');
    }

    function buildInvitationUrl(token) {
        return `${location.origin}/signup?token=${token}`;
    }

    function checkEmptyPage(data = {}) {    
        if (!data?.results?.length && data.page > 1) {
            vm.sc.page -= 1;
            search();
        }
        $timeout(() => {
            scrollableParentElement.scrollTop = 0;
        }, 0, false);
    }

    function takeOff(invite) {
        InvitationService.deleteInvitation(invite.id)
            .then((rs) => {
                if (rs.success) {
                    vm.sr = vm.sr.filter(({ id }) => id !== invite.id);
                    messageService.success('Invitation was taken off successfully.');
                    search();
                } else {
                    messageService.error(rs.message);
                }
            });
    }

    function retryInvite(invite, index) {
        const {email, group: {id: groupId}, source} = invite;
        const invitation = {
            invitationTypes: [{email, groupId, source}],
        };

        InvitationService.invite(invitation)
            .then((rs) => {
                if (rs.success) {
                    vm.sr[index] = {...vm.sr[index], ...rs.data[0]};
                    messageService.success('Invitation was sent successfully.');
                } else {
                    messageService.error(rs.message);
                }
            });
    }

    function getAllGroups(isPublic) {
        GroupService.getAllGroups(isPublic).then(function (rs) {
            if(rs.success) {
                GroupService.groups = rs.data.results;
            }
        });
    }

    function search() {
        var requestVariables = $location.search();
        if (requestVariables) {
            angular.forEach(requestVariables, function (value, key) {
                if(vm.sc[key] !== undefined) {
                    vm.sc[key] = value;
                }
            });
        }
        InvitationService.search(vm.sc)
            .then((rs) => {
                if (rs.success) {
                    vm.sr = rs.data.results || [];
                    vm.totalResults = rs.data.totalResults || 0;
                    checkEmptyPage(rs.data);
                } else {
                    messageService.error(rs.message);
                }
            });
        vm.isFiltered = true;
    }

    function reset() {
        if(vm.isFiltered) {
            vm.sc = angular.copy(DEFAULT_SC);
            search();
            vm.isFiltered = false;
        }
    }

    function initController() {
        search();
        if (!GroupService.groups.length) {
            getAllGroups(true);
        }
    }

};

export default InvitationsController;

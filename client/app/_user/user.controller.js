import uploadImageModalController from '../shared/modals/upload-image-modal/upload-image-modal.controller';
import uploadImageModalTemplate from '../shared/modals/upload-image-modal/upload-image-modal.html';

const UserProfileController = function UserProfileController(
    $httpMock,
    $mdDialog,
    UserService,
    UtilService,
    authService,
    $q,
    $state,
    messageService,
    $rootScope,
    pageTitleService,
) {
    'ngInject';

    const vm = {
        user: {},
        changePassword: {},
        accessToken: null,

        copyAccessToken,
        showUploadImageDialog,
        updateUserProfile,
        updateUserPassword,
        generateAccessToken,
        validations: UtilService.validations,
        getValidationValue: UtilService.getValidationValue,
        untouchForm: UtilService.untouchForm,
        copyServiceUrl,
        goToState,

        get currentTitle() { return pageTitleService.pageTitle },
        get currentUser() { return UserService.currentUser; },
        get serviceUrl() { return $httpMock.apiHost ? $httpMock.apiHost : location.origin; },
        get appVersions() { return $rootScope.version; },
        get userImage() {
            /// to support old absolute path and new relative
            // if url isn't relative and apiHost is specified add this host
            if (UserService.currentUser?.photoUrl && (!UserService.currentUser.photoUrl.startsWith('http') && $httpMock.apiHost)) {
                return `${$httpMock.apiHost}${UserService.currentUser.photoUrl}`;
            }

            return UserService.currentUser?.photoUrl ?? '';
        },
        set userImage(url) {
            if (UserService.currentUser) {
                UserService.currentUser.photoUrl = url;
            }
        },
    };

    function goToState(state) {
        $state.go(state);
    }

    function updateUserProfile() {
        const changedUserValues = [];

        for (const key in vm.user) {
            if (vm.user[key] !== vm.previousUserData[key] && !angular.isObject(vm.user[key]) && !Array.isArray(vm.user[key])) {
                changedUserValues.push({op: 'replace', path: `/${key}`, value: vm.user[key]});
            }
        }

        UserService.updateUserProfile(vm.user.id, changedUserValues)
            .then(function (rs) {
                if (rs.success) {
                    vm.currentUser.firstName = vm.user.firstName;
                    vm.currentUser.lastName = vm.user.lastName;
                    vm.previousUserData = angular.copy(vm.user);
                    messageService.success('User profile updated');
                } else {
                    messageService.error(rs.message);
                }
            });
    }

    function generateAccessToken() {
        authService.generateAccessToken()
            .then(function (rs) {
                if (rs.success) {
                    vm.accessToken = rs.data.token;
                }
            });
    }

    function copyAccessToken() {
        vm.accessToken.copyToClipboard();
        messageService.success('Access token copied to clipboard');
    }

    function copyServiceUrl() {
        vm.serviceUrl.copyToClipboard();
        messageService.success('Service URL copied to clipboard');
    }

    function updateUserPassword() {
        const data = angular.copy(vm.changePassword);

        UserService.updateUserPassword(vm.user.id, data)
            .then(function (rs) {
                if (rs.success) {
                    vm.changePassword = {};
                    messageService.success('Password changed');
                } else {
                    messageService.error(rs.message);
                }
            });
    }

    function fetchUserProfile() {
        vm.user = {...vm.currentUser};
        vm.previousUserData = angular.copy(vm.currentUser);
        vm.changePassword.userId = vm.user.id;
    }

    function showUploadImageDialog($event) {
        $mdDialog.show({
            controller: uploadImageModalController,
            controllerAs: '$ctrl',
            template: uploadImageModalTemplate,
            parent: angular.element(document.body),
            targetEvent: $event,
            clickOutsideToClose: true,
            locals: {
                urlHandler: (url) => {
                    if (url) {
                        const params = [{'op': 'replace', 'path': '/photoUrl', 'value': url}];

                        return UserService.updateUserProfile(vm.user.id, params)
                            .then((prs) => {
                                if (prs.success) {
                                    vm.currentUser.photoUrl = `${url}?${(new Date()).getTime()}`;
                                    messageService.success('Profile was successfully updated');

                                    return true;
                                } else {
                                    messageService.error(prs.message);

                                    return false;
                                }
                            });
                    }

                    return $q.reject(false);
                },
                fileTypes: 'USER_ASSET',
            }
        });
    }

    function initController() {
        fetchUserProfile();
    }

    vm.$onInit = initController;

    return vm;
};

export default UserProfileController;

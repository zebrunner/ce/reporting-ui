import widgetDialog from './widget-dialog/widget-dialog.html';
import widgetDialogController from './widget-dialog/widget-dialog.controller';
import dashboardEmailModalTemplate from './dashboard-email-modal/dashboard-email-modal.html';
import dashboardEmailModalController from './dashboard-email-modal/dashboard-email-modal.controller';
import widgetWizardController from '../components/modals/widget-wizard/widget-wizard.controller';
import widgetWizardModalTemplate from '../components/modals/widget-wizard/widget_wizard.html';
import dashboardSettingsModalController
    from '../shared/modals/dashboard-settings-modal/dashboard-settings-modal.controller';
import dashboardSettingsModalTemplate from '../shared/modals/dashboard-settings-modal/dashboard-settings-modal.html';

const dashboardController = function dashboardController(
    $scope,
    $q,
    $timeout,
    $interval,
    $location,
    $stateParams,
    $mdDialog,
    DashboardsService,
    UserService,
    $widget,
    messageService,
    authService,
    pageTitleService,
) {
    'ngInject';

    const vm = {
        dashboard: null,
        userHasAnyPermission: authService.userHasAnyPermission,

        get currentTitle() { return pageTitleService.pageTitle; },
    };

    $scope.emptyContent = {
        mobileText: ['No widget added.', 'Go to desktop version for more options.'],
        imageUrl: require('../../assets/images/empty-pages/empty_screen_dashboards.svg'),
        showArrow: vm.userHasAnyPermission(['reporting:widgets:update'])
    };

    const emptyContentText = vm.userHasAnyPermission(['reporting:widgets:update']) ?
        'No widget added. Use button at the top.' :
        'No widget added';
    $scope.emptyContent.text = [emptyContentText];

    $scope.isPageLoading = true;

    $scope.currentUserId = $location.search().userId;

    $scope.ECHART_TYPES = ['echart', 'PIE', 'LINE', 'BAR', 'TABLE', 'OTHER'];

    $scope.pristineWidgets = [];

    $scope.unexistWidgets = [];

    $scope.dashboard = {};

    $scope.gridstackOptions = {
        disableDrag: true,
        disableResize: true,
        verticalMargin: 20,
        resizable: {
            handles: 'se, sw'
        },
        cellHeight: 20
    };

    $scope.isGridStackEvailableToEdit = function() {
        return !angular.element('.grid-stack-one-column-mode').is(':visible');
    };

    $scope.MODE = '';

    $scope.startEditWidgets = function () {
        angular.element('.grid-stack').gridstack($scope.gridstackOptions).data('gridstack').enable();
        $scope.MODE = 'WIDGET_PLACEMENT';
    };

    $scope.updateWidgetsPosition = () => {
        const widgetsLocations = [];

        for (let i = 0; i < $scope.dashboard.widgets.length; i += 1) {
            const currentWidget = $scope.dashboard.widgets[i];
            const widgetData = {};
            angular.copy(currentWidget, widgetData);
            widgetData.location = JSON.stringify(widgetData.location);
            widgetsLocations.push({ widgetId: currentWidget.id, location: widgetData.location });
        }
        DashboardsService.UpdateDashboardWidgets($stateParams.dashboardId, widgetsLocations)
            .then((rs) => {
                if (rs.success) {
                    angular.copy(rs.data, $scope.pristineWidgets);
                    $scope.resetGrid();
                    messageService.success('Widget positions were updated');
                } else {
                    messageService.error(rs.message);
                }
                $scope.MODE = '';
            });
    };

    function loadDashboardData (dashboard, refresh) {
        if (!Array.isArray(dashboard.widgets)) {
            return;
        }
        for (let i = 0; i < dashboard.widgets.length; i++) {
            const currentWidget = dashboard.widgets[i];
            currentWidget.location = jsonSafeParse(currentWidget.location);
            if (!refresh || refresh && currentWidget.refreshable) {
                loadWidget(dashboard, currentWidget, refresh);
            }
        }
        angular.copy(dashboard.widgets, $scope.pristineWidgets);
    }

    function loadWidget(dashboard, widget, refresh) {
        if (widget.widgetTemplate) {
            widget.builder = widget.builder || {};

            if (!widget.builder.paramsConfigObject) {
                widget.builder.paramsConfigObject = $widget.build(widget, dashboard, $scope.currentUserId);
                widget.builder.legendConfigObject = jsonSafeParse(widget.legendConfig);
                applyLegend(widget);
            }

            const sqlAdapter = {
                templateId: widget.widgetTemplate.id,
                paramsConfig: Object.keys(widget.builder.paramsConfigObject)
                    .reduce((params, key) => {
                        params[key] = widget.builder.paramsConfigObject[key].value;

                        return params;
                    }, {}),
            };

            DashboardsService.ExecuteWidgetTemplateSQL(sqlAdapter)
                .then(({ success, data, message }) => {
                    if (success) {
                        if (!refresh) {
                            widget.model = widget.widgetTemplate ? widget.widgetTemplate.chartConfig : widget.model;
                            if ((!widget.widgetTemplate && widget.type !== 'echart') || (widget.widgetTemplate && widget.widgetTemplate.type === 'TABLE')) {
                                widget.model = jsonSafeParse(widget.model);
                            }
                        }
                        widget.data = {};
                        widget.data.dataset = data;
                        if (widget.title.toUpperCase().includes('CRON')) {
                            addOnClickConfirm();
                        }
                    } else if (message) {
                        messageService.error(message);
                    }
                })
                .finally(() => {
                    $scope.isLoading = false;
                });
        }
    }

    function applyLegend(widget) {
        widget.chartActions = widget.chartActions || [];
        angular.forEach(widget.builder.legendConfigObject, function (value, legendName) {
            widget.chartActions.push({type: value ? 'legendSelect' : 'legendUnSelect', name: legendName});
        });
    }

    function getNextEmptyGridArea(defaultLocation) {
        const location = jsonSafeParse(defaultLocation);
        let preparedLocation = angular.copy(location);
        const gridstackElement = angular.element('.grid-stack');
        const gridstack = gridstackElement.gridstack($scope.gridstackOptions).data('gridstack');

        // if at least one widget is present, grid stack element is not undefined
        if (gridstack) {
            const grid = gridstack.grid;
            const nodes = grid.nodes;

            const maxYNodes = getMaxNodes(nodes, 'y');
            const maxXYNodes = getMaxNodes(maxYNodes, 'x');
            if (maxXYNodes.length === 1) {
                const maxNode = maxXYNodes[0];
                preparedLocation.x = maxNode.x + maxNode.width;
                preparedLocation.y = maxNode.y;

                const isEmpty = gridstack.isAreaEmpty(preparedLocation.x, preparedLocation.y, preparedLocation.width, preparedLocation.height);
                const outOfWidth = preparedLocation.x + preparedLocation.width > grid.width;
                if (! isEmpty || outOfWidth) {
                    preparedLocation.x = location.x;
                    preparedLocation.y = maxNode.y + location.height + 1;
                }
            }
        }
        return jsonSafeStringify(preparedLocation);
    }

    function getMaxNodes(nodes, key) {
        let maxNodes = undefined;
        let maxNodeValue = undefined;
        nodes.forEach(function (node) {
            const nodeValue = node[key];
            if(maxNodeValue === undefined || (nodeValue > maxNodeValue[key])) {
                maxNodes = [];
                maxNodes.push(node);
                maxNodeValue = node;
            } else if(maxNodes !== undefined && (nodeValue === maxNodeValue[key])) {
                maxNodes.push(node);
            }
        });
        return maxNodes;
    }

    $scope.showDashboardSettingsModal = function(event, dashboard, status) {
        $mdDialog.show({
            controller: dashboardSettingsModalController,
            template: dashboardSettingsModalTemplate,
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            fullscreen: false,
            autoWrap: false,
            locals: {
                dashboard,
                status,
                defaultDataForRequest: { ...vm.sc },
            },
        })
            .then((rs) => {
                if (rs) {
                    rs.widgets = $scope.dashboard.widgets;
                    $scope.dashboard = angular.copy(rs);
                    delete rs.action;
                }
            }, () => {});
    };

    $scope.addDashboardWidget = function (widget, hideSuccessAlert) {
        widget.location = resolveWidgetLocation(widget);
        const widgetLocation = { location: widget.location };
        const requestParams = { widgetId: widget.id };

        return DashboardsService.AddDashboardWidget($stateParams.dashboardId, widgetLocation, requestParams)
            .then(({ success, message }) => {
                if (success) {
                    $scope.dashboard.widgets.push(widget);
                    loadDashboardData($scope.dashboard, false);
                    if (!hideSuccessAlert) {
                        messageService.success('The widget was successfully added');
                    }
                } else if (message) {
                    messageService.error(message);
                }
            });
    };

    const defaultWidgetLocation = {
        x: 0,
        y: 0,
        width: 4,
        height: 11,
    };

    function resolveWidgetLocation(widget) {
        const {
            width,
            height,
            x,
            y,
        } = defaultWidgetLocation;

        const widgetLocation = {
            width: widget.defaultSize && widget.defaultSize.width || width,
            height: widget.defaultSize && widget.defaultSize.height || height,
            x: widget.location?.x || x,
            y: widget.location?.y || y,
        };
        return getNextEmptyGridArea(jsonSafeStringify(widgetLocation));
    }

    $scope.deleteDashboardWidget = function (widget) {
        var confirmedDelete = confirm('Would you like to delete widget "' + widget.title + '" from dashboard?');
        if (confirmedDelete) {
            DashboardsService.DeleteDashboardWidget($stateParams.dashboardId, widget.id).then(function (rs) {
                if (rs.success) {
                    $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
                    $scope.dashboard.widgets.forEach(function (widget) {
                        widget.location = jsonSafeStringify(widget.location);
                    });
                    loadDashboardData($scope.dashboard, false);
                    messageService.success("Widget deleted");
                }
                else {
                    messageService.error(rs.message);
                }
            });
        }
    };

    function isJSON(jsonStr) {
        try {
            const json = JSON.parse(jsonStr);

            return (typeof json === 'object');
        } catch (e) {
            return false;
        }
    };

    $scope.resetGrid = function () {
        var gridstack = angular.element('.grid-stack').gridstack($scope.gridstackOptions).data('gridstack');
        //gridstack.batchUpdate();
        $scope.pristineWidgets.forEach(function (widget) {
            const currentWidget = $scope.dashboard.widgets.find(function(w) {
                return widget.id === w.id;
            });

            if (currentWidget) {
                widget.location = jsonSafeParse(widget.location);
                currentWidget.location.x = widget.location.x;
                currentWidget.location.y = widget.location.y;
                currentWidget.location.height = widget.location.height;
                currentWidget.location.width = widget.location.width;
                var element = angular.element('#widget-' + currentWidget.id);

                gridstack.update(element, widget.location.x, widget.location.y,
                    widget.location.width, widget.location.height);
            }
        });
        $scope.MODE = '';
        gridstack && gridstack.disable();
        //gridstack.commit();
    };

    $scope.asString = function (value) {
        if (value) {
            value = value.toString();
        }
        return value;
    };

    $scope.isFormatted = function (string) {
        var pattern = /^<.+>.*<\/.+>$/g;
        return pattern.test(string);
    };

    function jsonSafeParse (preparedJson) {
        if (typeof preparedJson === 'string') {
            preparedJson = preparedJson.replace(/^(")|(")$/g, '').replace(/\\/g,'');

            if (isJSON(preparedJson)) {
                return JSON.parse(preparedJson);
            }
        }

        return preparedJson;
    };

    function jsonSafeStringify (preparedJson) {
        if (typeof preparedJson === 'object') {
            return JSON.stringify(preparedJson);
        }

        return preparedJson;
    };

    $scope.sort = {
        column: null,
        descending: false
    };

    $scope.deleteWidget = function($event, widget){
        var confirmedDelete = confirm('Would you like to delete widget "' + widget.title + '" ?');
        if (confirmedDelete) {
            DashboardsService.DeleteWidget(widget.id).then(function (rs) {
                if (rs.success) {
                    if($scope.dashboard.widgets.indexOfId(widget.id) >= 0) {
                        $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOfId(widget.id), 1);
                    }
                    messageService.success("Widget deleted");
                }
                else {
                    messageService.error(rs.message);
                }
            });
        }
    };

    $scope.changeSorting = function(widget, column) {
        var specCharRegexp = /[-[\]{}()*+?.,\\^$|#\s%]/g;

        if (column.search(specCharRegexp) != -1) {
            column.replace("\"\"", "\"");
         }
         if(! widget.sort) {
             widget.sort = {};
             angular.copy($scope.sort, widget.sort);
         }
        if (widget.sort.column == column) {
            widget.sort.descending = !widget.sort.descending;
        } else {
            widget.sort.column = column;
            widget.sort.descending = false;
        }
    };

    /*$scope.deleteDashboard = function(dashboard){
        var confirmedDelete = confirm('Would you like to delete dashboard "' + dashboard.title + '"?');
        if (confirmedDelete) {
            DashboardsService.DeleteDashboard(dashboard.id).then(function (rs) {
                if (rs.success) {
                    messageService.success("Dashboard deleted");
                    var mainDashboard = $location.$$absUrl.substring(0, $location.$$absUrl.lastIndexOf('/'));
                    window.open(mainDashboard, '_self');
                }
                else {
                    messageService.error(rs.message);
                }
            });
        }
        $scope.hide();
    };*/

    $scope.showNeededWidgetModal = function(event, widget, isNew, dashboard) {
        if($scope.ECHART_TYPES.indexOf(widget.type) !== -1 && widget.widgetTemplate) {
            $scope.showWidgetWizardDialog(event, widget, dashboard);
        } else {
            $scope.showWidgetDialog(event, widget, isNew, dashboard);
        }
    };

    $scope.showWidgetWizardDialog = function (event, widget, dashboard) {
        $mdDialog.show({
            controller: widgetWizardController,
            controllerAs: '$ctrl',
            template: widgetWizardModalTemplate,
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            targetEvent: event,
            fullscreen: false,
            autoWrap: false,
            escapeToClose:false,
            locals: {
                widget: widget,
                dashboard: dashboard,
                currentUserId: $scope.currentUserId
            }
        })
        .then(function (rs) {
            switch(rs.action) {
                case 'ADD':
                    $scope.addDashboardWidget(rs.widget, true);
                    break;
                case 'CREATE':
                    $scope.addDashboardWidget(rs.widget, true);
                    break;
                case 'UPDATE':
                    const selectedWidget = $scope.dashboard.widgets.find(({ id }) => id === rs.widget.id);

                    if (selectedWidget) {
                        Object.assign(selectedWidget, rs.widget);
                        selectedWidget.location = jsonSafeParse(selectedWidget.location);
                        selectedWidget.builder = {};
                        loadWidget(dashboard, selectedWidget, false);
                    }
                    break;
                case 'DELETE':
                    break;
                default:
                    break;
            }
        }, function () {});
    };

    $scope.showWidgetDialog = function (event, widget, isNew, dashboard) {
        $mdDialog.show({
            controller: widgetDialogController,
            template: widgetDialog,
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            fullscreen: false,
            locals: {
                widget: widget,
                isNew: isNew,
                dashboard: dashboard,
                currentUserId: $scope.currentUserId
            }
        })
        .then(function (rs) {
            if(rs) {
                switch(rs.action) {
                    case 'CREATE':
                        break;
                    case 'UPDATE':
                        break;
                    default:
                        break;
                }
                delete rs.action;
            }
        }, function () {
        });
    };

    $scope.showEmailDialog = function (event, item) {
        let model = angular.copy(item);

        if (item.widgetTemplate) {
            model.title = $scope.dashboard.title + ' dashboard - ' + item.title + ' widget';
            model.locator = '#widget-container-' + model.id;
        } else {
            model.title = item.title + ' dashboard';
            model.locator = '#dashboard_content';
        }

        $mdDialog.show({
            controller: dashboardEmailModalController,
            template: dashboardEmailModalTemplate,
            controllerAs: '$ctrl',
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            fullscreen: false,
            locals: {
                model,
                dashboardId: vm.dashboard.id,
            }
        })
        .then(function () {}, function () {});
    };

    var toAttributes = function (qParams) {
        var attributes = [];
        for(var param in qParams) {
            var currentAttribute = {};
            currentAttribute.key = param;
            currentAttribute.value = qParams[param];
            attributes.push(currentAttribute);
        }
        return attributes;
    };

    var getQueryAttributes = function () {
        var qParams = $location.search();
        var qParamsLength = Object.keys(qParams).length;
        if(qParamsLength > 0 && $stateParams.dashboardId) {
            return toAttributes(qParams);
        }
    };

    $scope.optimizeWidget = function (widget, index) {
        if (['table', 'TABLE'].indexOf(widget.type) !== -1 && (Object.size(widget.data.dataset) === 0 || Object.size(widget.data.dataset) === index + 1)) {
            $scope.gridstackOptions.disableResize = false;
            const gridstack = angular.element('.grid-stack').gridstack($scope.gridstackOptions).data('gridstack');
            const el = angular.element('#widget-container-body-' + widget.id)[0];
            const gridstackEl = angular.element('#widget-' + widget.id)[0];
            $timeout(function () {
                const height = (Math.ceil(el.offsetHeight / $scope.gridstackOptions.cellHeight / 2)) + 2;
                if(Object.size(widget.data.dataset) === 0) {
                    gridstack.resize(gridstackEl, widget.location.width, height);
                } else {
                    gridstack.resize(gridstackEl, widget.location.width, height);
                }
                $scope.gridstackOptions.disableResize = true;
            }, 0, false);
        }
    };

    var refreshIntervalInterval;

    function refresh() {
        const currentUser = UserService.currentUser;

        if ($scope.dashboard.title && currentUser.refreshInterval) {
            refreshIntervalInterval = $interval(function () {
                loadDashboardData($scope.dashboard, true);
            }, currentUser.refreshInterval);
        }
    };

    $scope.stopRefreshIntervalInterval = function() {
        if (angular.isDefined(refreshIntervalInterval)) {
            $interval.cancel(refreshIntervalInterval);
            refreshIntervalInterval = undefined;
        }
    };

    function getDashboardById(dashboardId) {
        return $q(function (resolve, reject) {
            DashboardsService.GetDashboardById(dashboardId).then(function (rs) {
                if (rs.success) {
                    $scope.dashboard = rs.data;
                    loadDashboardData($scope.dashboard, false);
                    resolve(rs.data);
                    pageTitleService.setTitle($scope.$ctrl.dashboard.title);
                } else {
                    reject(rs.message);
                }
                $scope.isPageLoading = false;
            });
        });
    }

    $scope.$watch(
        function() {
            if ($scope.currentUserId && $location.$$search.userId){
                return $scope.currentUserId !== $location.$$search.userId;
            }
        },
        function() {
            if ($scope.currentUserId && $location.$$search.userId) {
                if ($scope.currentUserId !== $location.$$search.userId) {
                    $scope.currentUserId = $location.search().userId;
                    getDashboardById($stateParams.dashboardId);
                }
            }
        }
    );

    $scope.$on("$event:widgetIsUpdated", function () {
        getDashboardById($stateParams.dashboardId);
    });

    $scope.$on('$destroy', function () {
        $scope.stopRefreshIntervalInterval();
        $scope.resetGrid();
    });

    function addOnClickConfirm() {
        $scope.$watch(function () {
            return angular.element('#cron_rerun').is(':visible')
        }, function () {
            var rerunAllLinks = document.getElementsByClassName("cron_rerun_all");
            Array.prototype.forEach.call(rerunAllLinks, function(link) {
                link.addEventListener("click", function (event) {
                    if (!confirm('Rebuild for all tests in cron job will be started. Continue?')) {
                        event.preventDefault();
                    }
                }, false);
            });
            var rerunFailuresLinks = document.getElementsByClassName("cron_rerun_failures");
            Array.prototype.forEach.call(rerunFailuresLinks, function(link) {
                link.addEventListener("click", function (event) {
                    if (!confirm('Rebuild for failures in cron job will be started. Continue?')) {
                        event.preventDefault();
                    }
                }, false);
            });
        });
    }

    function initController() {
        getDashboardById($stateParams.dashboardId).then(function (rs) {
            $timeout(function () {
                refresh();
            }, 0, false);
        });
    }

    vm.$onInit = initController;

    return vm;
};

export default dashboardController;
